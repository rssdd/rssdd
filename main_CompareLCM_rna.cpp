
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <sys/param.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <malloc.h>


#include <iostream>
#include <fstream>

#include <bitset>
#include <string>
#include <vector>
#include <stack>     /* read from DotBracket */
#include <utility>   /* pair, make_pair */
#include <unordered_map>       /* command */
#include <unordered_set>

#include <initializer_list>
#include <iomanip>   /* print */
#include <sstream>   /* StoN, NtoS */
#include <algorithm> /* sort, min_element, unique */
#include <cmath>     /* log */
#include <cstdint>   /* uint_fast32_t, ... */

// #include <cassert>

#include "Basic.hpp"
#include "ZDD.hpp"

using namespace std;



/******************************************************************************
 settings
 ******************************************************************************/

constexpr bool LCMmax = true;
constexpr int  SEQ_LENGTH = 20;


/******************************************************************************
 alias
 ******************************************************************************/
// class IntZDD_Root;

using IntZDD_Node   = ZDD_Node<int>;
// using IntZDD_Root   = ZDD_Root<int>;
using IntZDD_UTable = ZDD_UTable<IntZDD_Node>;



/******************************************************************************
 constant definitions
 ******************************************************************************/

constexpr int END_SYMBOL = numeric_limits<int>::max();
constexpr int TITEM_MAX  = numeric_limits<int>::max();
constexpr int LCM_TOP_DEFAULT  = 20;

const string PROMPT_STRING = "LCM> ";

/* file name */
const string HELP_FILE_NAME    = "IntZDD_help.txt";
const string HISTORY_FILE_NAME = ".comparelcm_history";


/******************************************************************************
 prototypes
 ******************************************************************************/

// void PrintList( const vector<IntZDD_Root>& ZDDs );



/******************************************************************************
 class
 ******************************************************************************/

class IntZDD_Root : public ZDD_Root<int>
{
 private:
	std::vector<int> Itemset__;  /* items which occur as a label */


 public:
	IntZDD_Root() : ZDD_Root<int>() {}

	IntZDD_Root(
			const std::string   &  init_name          ,
			IntZDD_Node* const    init_rootnode      ,
			const vector<int>   &  init_Itemset       ,
			const int              init_superset = -1 )
		: ZDD_Root<int>( init_name, init_rootnode, init_superset )
		, Itemset__( init_Itemset )
	{}

	std::vector<int> Itemset() const { return Itemset__ ; }


	virtual void ConvertAndPrint(
			      ostream&       ost         ,
			const vector<int> &  Combination ) const
	{  /* used in PrintCombinations() */
		vector<int>  array( Combination.size(), 0 );
		for ( size_t i = 0; i < Combination.size(); ++i ) {
			array[i] = Itemset()[ Combination[i] ];
		}
		ost << array;
	}
};



class TFrequentPattern
{
 public:
	int          freq;
	vector<int>  pattern;

	TFrequentPattern() : freq(0) {}

	TFrequentPattern(
			const int            init_freq    ,
			const vector<int> &  init_pattern )
		: freq(init_freq)
		, pattern(init_pattern)
	{}

	~TFrequentPattern(){}

	friend bool operator==( const TFrequentPattern & lhs, const TFrequentPattern & rhs ) {
		return (lhs.pattern == rhs.pattern);
	}
	friend bool operator< ( const TFrequentPattern & lhs, const TFrequentPattern & rhs ) {
		if (lhs.freq < rhs.freq) return true;
		if (lhs.freq == rhs.freq) {
			return (lhs.pattern < rhs.pattern);
		}
		return false;
	}

	friend bool operator!=( const TFrequentPattern & lhs, const TFrequentPattern & rhs ) {
		return std::rel_ops::operator!=(lhs,rhs);
	}
	friend bool operator> ( const TFrequentPattern & lhs, const TFrequentPattern & rhs ) {
		return std::rel_ops::operator> (lhs,rhs);
	}
	friend bool operator<=( const TFrequentPattern & lhs, const TFrequentPattern & rhs ) {
		return std::rel_ops::operator<=(lhs,rhs);
	}
	friend bool operator>=( const TFrequentPattern & lhs, const TFrequentPattern & rhs ) {
		return std::rel_ops::operator>=(lhs,rhs);
	}
};





/******************************************************************************
 functions
 ******************************************************************************/

inline Uint16 Pair2Int( const Uint8 left, const Uint8 right )
{
	return ( (left << 8) | right );
}

inline Uint8 pair_first ( const Uint16 val ) { return (val >>  8); }
inline Uint8 pair_second( const Uint16 val ) { return (val & 255); }

inline std::pair<Uint8,Uint8> Int2Pair( const Uint16 val )
{
	return std::make_pair( pair_first(val), pair_second(val) );
}




void DotBracket2PairArray(
		vector<int> &  pair_array ,
		const string      &  DB         )
{
	pair_array.clear();
	pair_array.reserve( 1 + (DB.length() / 2) );
	stack<Uint> left_position;
	for ( size_t i = 0, n = DB.length(); i < n; ++i ) {
		if ( DB[i] == '(' ) {
			left_position.push(i);
		}
		if ( DB[i] == ')' ) {
			pair_array.push_back( Pair2Int(left_position.top(), i) );
			left_position.pop();
		}
	}
	sort( pair_array.begin(), pair_array.end() );
// cout << DB << pair_array << endl;
}



int ReadDotBracketFromFile(
		vector< vector<int> > &  TDB ,
		ifstream              &  fin )
{
	string buf;
	vector<int> pair_array_buf;
	stringstream ss;
	string DB;
	double energy = 0.0;
	while ( getline( fin, buf ) ) {
		if ( buf.length() == 0 )  continue;
		if ( (buf[0] != '.') and (buf[0] != '(') and (buf[0] != ')') ) continue; /* discard the line */
		ss << buf;
		ss >> DB;
		ss >> energy;

		// pair_array_buf.clear();
		DotBracket2PairArray( pair_array_buf, DB );
		TDB.push_back( pair_array_buf );

		// reset ss
		ss.str(""); // clear buffer
		ss.clear(stringstream::goodbit); // clear the state of stream
	}
	return 0;
}



void ReindexTDB(
		vector< vector<int> > &  TDB     ,
		vector<int>           &  Itemset )
{
	Itemset.clear();

	vector< int* > ptr2item; /* pointer to elements in TDB */
	for ( auto& row : TDB ) {
		for ( auto& elem : row ) {
			ptr2item.push_back( &elem );
		}
	}
	int item_value_max = TITEM_MAX;
	ptr2item.push_back( &item_value_max );  /* sentinel */

	sort( ptr2item.begin(), ptr2item.end(), []( int* x, int* y ) { return (*x < *y); } );

	int index = 0;
	for ( size_t i = 0, n = ptr2item.size() - 1; i < n; ++i ) {
		if ( *(ptr2item[i]) < *(ptr2item[i + 1]) ) {
			Itemset.push_back( *(ptr2item[i]) );
			*(ptr2item[i]) = index;
			++index;
		} else {
			*(ptr2item[i]) = index;
		}
	}
}




void PrintPattern(
		const vector<int> &  Pattern    ,
		const vector<int> &  Itemset    ,
		ostream           &  ost = cout )
{
	vector<int>  array( Pattern.size() );
	for ( size_t i = 0; i < Pattern.size(); ++i ) {
		array[i] = Itemset[ Pattern[i] ];
	}
	// ost << array;

	string DB( SEQ_LENGTH, ' ' );  /* dot-bracket string */

	for ( const auto& e : array ) {
		DB[ pair_first (e) ] = '(';
		DB[ pair_second(e) ] = ')';
	}

	ost << DB;
}



void PrintFrequentPatterns(
		const vector<TFrequentPattern> &  FP         ,
		const vector<int>              &  Itemset    ,
		ostream                        &  ost = cout )
{
	if ( FP.empty() ) return;
	const int width = DigitNumber( FP.front().freq );
	for ( auto it = FP.cbegin(); it != FP.cend(); ++it ) {
		ost << setw(width) << it->freq << '\t';
		PrintPattern( it->pattern, Itemset, ost );
		ost << endl;
	}
}



inline void ComputePatternFromItemFreq(
		      vector<int> &  Pattern  ,
		const vector<int> &  ItemFreq ,
		const int            setsize  )
{
	for ( size_t i = 0; i < ItemFreq.size(); ++i ) {
		if ( ItemFreq[i] == setsize )  Pattern.push_back(i);
	}
}



/******************************************************************************
 Table version
 ******************************************************************************/

inline void ComputeItemFreq(
		      vector<int>            &  ItemFreq ,
		const vector< vector<int>* > &  T        )
{
	fill( ItemFreq.begin(), ItemFreq.end(), 0 );
	for ( const auto& row : T ) {
		for ( const auto& e : *row ) {
			++ItemFreq[e];
		}
	}
}



inline void BuildSubsetTDB(
		      vector< vector<int>* > &  T_subset ,
		const vector< vector<int>* > &  T        ,
		const int                       i        )
{
	for ( auto& e : T ) {
		if ( IsIn( i, *e, true ) ) {  /* true : binary search, false : linear search */
			T_subset.push_back(e);
		}
	}
}



void ComputeClosure(
		      vector<int>            &  Closure     ,
		const vector< vector<int>* > &  T_subset    ,
		      vector<int>            &  ItemFreqNew )
{
	Closure.clear();
	ComputeItemFreq( ItemFreqNew, T_subset );
	ComputePatternFromItemFreq( Closure, ItemFreqNew, T_subset.size() );
}



bool IsPrefixPreservingClosure(
		const vector<int> & Closure    ,  /* not empty. Pattern \subseteq Closure */
		const vector<int> & PatternNew ,
		const int           index      )
{
	auto itp = PatternNew.cbegin();
	auto itc = Closure.cbegin();
	while ( itp != PatternNew.cend() ) {
		if ( *itc >= index ) return true;
		if ( *itp++ != *itc++ )  return false;
	}
	return true;
}



inline void MakeNewPattern(
		      vector<int> &  PatternNew ,  /* size = Pattern.size() + 1 */
		const vector<int> &  Pattern    ,
		const int            i          )
{
	auto itr = Pattern.begin();
	auto itw = PatternNew.begin();
	while ( itr != Pattern.end() ) {
		if ( *itr >= i ) break;
		*itw++ = *itr++;
	}
	*itw++ = i;
	while ( itr != Pattern.end() ) {
		*itw++ = *itr++;
	}
}



void LCM_on_Table(
	      vector<TFrequentPattern> &  FP               ,  /* frequent patterns */
	const vector< vector<int>* >   &  T                ,  /* subset of TDB (pointer) */
	const vector<int>              &  Pattern          ,
	const vector<int>              &  ItemFreq         ,
	const int                         index_begin      ,
	const int                         ItemsetSize      ,
	const int                         MinSup           ,  /* minimal support */
	const bool                        LCMmax           ,  /* true : only maximal pattern */
	      vector<TProgress>        &  TimeOfComponents )
{
	// const auto int ItemsetSize = Itemset.size();
	const int setsize = T.size();

 /* reused container */
	vector<int>  Closure;
	vector<int>  ItemFreqNew( ItemsetSize, 0 );
	vector<int>  PatternNew( Pattern.size() + 1 );


	if ( not LCMmax ) {
		FP.emplace_back( setsize, Pattern );  /* add Pattern to FP */
	} else {
		bool is_maximal = true;
		for ( const auto& e : ItemFreq ) {  /* maximality check */
			if ( MinSup <= e and e < setsize ) { is_maximal = false; break; }
		}
		if ( is_maximal ) {
			FP.emplace_back( setsize, Pattern );
			return;
		}
	}



	for ( int i = index_begin; i < ItemsetSize; ++i )
	{
	 /* skip item */
		if ( ItemFreq[i] < MinSup )    continue;
		if ( ItemFreq[i] == setsize )  continue;  /* means item \in Pattern */

	 /* build subset */
		vector< vector<int>* > T_subset;
		TimeOfComponents[0].Start();
		BuildSubsetTDB( T_subset, T, i );
		TimeOfComponents[0].Stop();
		TimeOfComponents[0].AccumulateTime();

	 /* compute closure */
		TimeOfComponents[1].Start();
		ComputeClosure( Closure, T_subset, ItemFreqNew );
		TimeOfComponents[1].Stop();
		TimeOfComponents[1].AccumulateTime();

	 /* add item to Pattern */
		MakeNewPattern( PatternNew, Pattern, i );

	 /* avoid adding the same pattern twice to FP */
		if ( ! IsPrefixPreservingClosure( Closure, PatternNew, i ) ) continue;

	 /* recursive call */
		LCM_on_Table( FP, T_subset, Closure, ItemFreqNew, i + 1, ItemsetSize, MinSup, LCMmax, TimeOfComponents );
	}
}



/******************************************************************************
 ZDD version
 ******************************************************************************/

IntZDD_Node* BuildFromTable(
		      IntZDD_UTable        &  UTable    ,
		const vector< vector<int> > &  TDB       ,
		const Uint                     row_begin ,
		const Uint                     row_end   ,
		const Uint                     column    )
{  /* range = [row_begin,row_end ) */
	if ( row_begin >= row_end )  return nullptr;
	// prgrss.Print( row_begin );

	const auto Item = TDB[row_begin][column];

 /* separate [row_begin, row_end) to [row_begin, row_sep) and [row_sep, row_end) */
	auto row_sep = row_begin;
	while ( row_sep < row_end && TDB[row_sep][column] == Item )  ++row_sep;

	IntZDD_Node Nnew;
	if ( TDB[row_begin][column] == END_SYMBOL ) { /* line end */
		Nnew.hi = nullptr;
		Nnew.lo = nullptr;
	} else {
		Nnew.hi = BuildFromTable( UTable, TDB, row_begin, row_sep, column + 1 );
		Nnew.lo = BuildFromTable( UTable, TDB, row_sep,   row_end, column     );
	}
	Nnew.label = Item;
	return GetNode( UTable, Nnew );
}



void ComputeItemFreq(
		vector<int>         &  ItemFreq ,
		IntZDD_Node* const    Z        )
{
	std::function< int( IntZDD_Node* const  ) >
	subroutine = [&] ( IntZDD_Node* const  N ) -> int
		{
			if ( N == nullptr )  return 0;
			if ( N->IsNode1() )  return 1;

		 /* N is internal node */
			const Uint elemcount_hi = subroutine( N->hi );
			const Uint elemcount_lo = subroutine( N->lo );

			ItemFreq[N->label] += elemcount_hi ;

			return elemcount_hi + elemcount_lo;
		};


	fill( ItemFreq.begin(), ItemFreq.end(), 0 );
	subroutine(Z);
}



void LCM_on_ZDD(
	      vector<TFrequentPattern> &  FP               , /* frequent patterns */
	      IntZDD_UTable           &  UTable           ,
	IntZDD_Node* const               T                ,
	const int                         setsize          ,
	const int                         index_begin      ,
	const int                         ItemsetSize      ,
	const int                         MinSup           ,  /* minimal support */
	const bool                        LCMmax           ,  /* true : only maximal pattern */
	      vector<TProgress>        &  TimeOfComponents )
{
 /* add Pattern to FP */
	vector<int>  ItemFreq( ItemsetSize, 0 );
	vector<int>  Pattern;
	TimeOfComponents[3].Start();
	ComputeItemFreq( ItemFreq, T );
	TimeOfComponents[3].Stop();
	TimeOfComponents[3].AccumulateTime();

	TimeOfComponents[4].Start();
	ComputePatternFromItemFreq( Pattern, ItemFreq, setsize );
	TimeOfComponents[4].Stop();
	TimeOfComponents[4].AccumulateTime();

	if ( not LCMmax ) {
		FP.emplace_back( setsize, Pattern );  /* add Pattern to FP */
	} else {
		bool is_maximal = true;
		for ( const auto& e : ItemFreq ) {  /* maximality check */
			if ( MinSup <= e and e < setsize ) { is_maximal = false; break; }
		}
		if ( is_maximal ) {
			FP.emplace_back( setsize, Pattern );
			return;
		}
	}


	for ( int i = index_begin; i < ItemsetSize; ++i )
	{
	 /* skip item */
		if ( ItemFreq[i] < MinSup )    continue;
		if ( ItemFreq[i] == setsize )  continue;  /* means item \in Pattern */

	 /* build subset */
		TimeOfComponents[5].Start();
		const auto T_subset = BuildContainItem( UTable, T, i );
		TimeOfComponents[5].Stop();
		TimeOfComponents[5].AccumulateTime();

	 /* avoid adding the same pattern twice to FP */
		if ( T_subset->checked == true ) continue;  /* already added pattern */
		T_subset->checked = true;

	 /* recursive call */
		LCM_on_ZDD( FP, UTable, T_subset, ItemFreq[i], i + 1, ItemsetSize, MinSup, LCMmax, TimeOfComponents );
	}
}




/******************************************************************************
 ZDD - build subset by flag version
 ******************************************************************************/

/* return value :  true --> disable under the node */
bool BuildContainItem_flag(
		IntZDD_Node* const    N    ,
		const int           &  item )
{
	if ( N == nullptr ) return true;
	if ( N->IsNode1() or N->label > item ) return true;

 /* N is an internal node */
	if ( N->label == item ) {
		N->disable_lo = true;
		return false;
	}

 /* go to hi and lo */
	if ( not N->disable_hi ) {
		N->disable_hi = BuildContainItem_flag( N->hi, item );
	}
	if ( not N->disable_lo ) {
		N->disable_lo = BuildContainItem_flag( N->lo, item );
	}

	return ( N->disable_hi and N->disable_lo );
}


int ComputeItemFreq_on_ZDD_flag(  /* return setsize */
		vector<int>         &  ItemFreq ,
		IntZDD_Node* const    Z        )
{
	std::function< int( IntZDD_Node* const  ) >
	subroutine = [&] ( IntZDD_Node* const  N ) -> int
		{
			if ( N == nullptr )  return 0;
			if ( N->IsNode1() )  return 1;

		 /* N is internal node */
			Uint elemcount_hi = 0;
			Uint elemcount_lo = 0;
			if ( not N->disable_hi )  elemcount_hi = subroutine( N->hi );
			if ( not N->disable_lo )  elemcount_lo = subroutine( N->lo );

			ItemFreq[N->label] += elemcount_hi ;

			return elemcount_hi + elemcount_lo;
		};


	fill( ItemFreq.begin(), ItemFreq.end(), 0 );
	return subroutine(Z);
}



void ComputeClosure_on_ZDD_flag(
		      vector<int>            &  Closure     ,
		      IntZDD_Node* const       T           ,
		      vector<int>            &  ItemFreqNew )
{
	Closure.clear();
	const int setsize = ComputeItemFreq_on_ZDD_flag( ItemFreqNew, T );
	ComputePatternFromItemFreq( Closure, ItemFreqNew, setsize );
}



void LCM_on_ZDD_flag(
	      vector<TFrequentPattern> &  FP               ,  /* frequent patterns */
	const vector<IntZDD_Node*>    &  Nodes            ,
	      IntZDD_Node* const         T                ,
	const vector<int>              &  Pattern          ,
	const vector<int>              &  ItemFreq         ,
	const int                         setsize          ,
	const int                         index_begin      ,
	const int                         ItemsetSize      ,
	const int                         MinSup           ,  /* minimal support */
	      vector<TProgress>        &  TimeOfComponents )
{
	FP.emplace_back( setsize, Pattern );  /* add Pattern to FP */

	TimeOfComponents[6].Start();
	vector<bool> disable_hi_array( Nodes.size(), false );
	vector<bool> disable_lo_array( Nodes.size(), false );
	{  /* save disable flags */
		auto it_hi = disable_hi_array.begin();
		auto it_lo = disable_lo_array.begin();
		for ( auto it = Nodes.cbegin(); it != Nodes.cend(); ++it ) {
			*it_hi++ = (*it)->disable_hi;
			*it_lo++ = (*it)->disable_lo;
		}
	}
	TimeOfComponents[6].Stop();
	TimeOfComponents[6].AccumulateTime();


 /* reused container */
	vector<int>  Closure;
	vector<int>  ItemFreqNew( ItemsetSize, 0 );
	vector<int>  PatternNew( Pattern.size() + 1 );


	for ( int i = index_begin; i < ItemsetSize; ++i )
	{
	 /* skip item */
		if ( ItemFreq[i] < MinSup )    continue;
		if ( ItemFreq[i] == setsize )  continue;  /* means item \in Pattern */

		TimeOfComponents[7].Start();
		{  /* write back disable flags to nodes */
			auto it_hi = disable_hi_array.cbegin();
			auto it_lo = disable_lo_array.cbegin();
			for ( auto it = Nodes.begin(); it != Nodes.end(); ++it ) {
				(*it)->disable_hi = *it_hi++;
				(*it)->disable_lo = *it_lo++;
			}
		}
		TimeOfComponents[7].Stop();
		TimeOfComponents[7].AccumulateTime();

	 /* build subset */
		TimeOfComponents[8].Start();
		BuildContainItem_flag( T, i );
		// for ( auto& e : Nodes ) {  if ( e->label == i )  e->disable_lo = true;  }
		TimeOfComponents[8].Stop();
		TimeOfComponents[8].AccumulateTime();

	 /* compute closure */
		TimeOfComponents[9].Start();
		ComputeClosure_on_ZDD_flag( Closure, T, ItemFreqNew );
		TimeOfComponents[9].Stop();
		TimeOfComponents[9].AccumulateTime();

	 /* add item to Pattern */
		MakeNewPattern( PatternNew, Pattern, i );

	 /* avoid adding the same pattern twice to FP */
		if ( not IsPrefixPreservingClosure( Closure, PatternNew, i ) ) continue;

	 /* recursive call */
		LCM_on_ZDD_flag( FP, Nodes, T, Closure, ItemFreqNew, ItemFreq[i],
				i + 1, ItemsetSize, MinSup, TimeOfComponents );
	}
}









int main()
{
	IntZDD_UTable  UTable;
	vector< vector<int> >  TDB;
	vector<int>  Itemset;
	int  MinSup = 0;

	// string IFileName = "inputdata/tdbtest.dat";
	string IFileName;
	{ /* read transaction database file */
		string  CmdLine;
		ReadlineString( CmdLine, "input filename : " );
		stringstream  ss(CmdLine);
		ss >> IFileName;
	}
	ifstream  fin( IFileName );
	if ( ! FileCheck( fin, IFileName ) ) return 1;

	string OFileName = "output.txt";
	// string OFileName;
	// { /* output file */
	// 	string  CmdLine;
	// 	ReadlineString( CmdLine, "output filename : " );
	// 	stringstream  ss(CmdLine);
	// 	ss >> OFileName;
	// }
	ofstream  fout( OFileName );
	if ( ! FileCheck( fout, OFileName ) ) return 1;

	ReadDotBracketFromFile( TDB, fin );

	if ( TDB.empty() ) return 1;

	// cout << TDB << endl;
	ReindexTDB( TDB, Itemset );

	vector< vector<int>* >  T;
	for ( auto it = TDB.begin(); it != TDB.end(); ++it ) {
		T.push_back( &*it );
	}

	size_t len_max = 0;  /* use in resizing TDBtable */


	{
		fout << "LCMmax : " << boolalpha << LCMmax << endl << endl;
	}

	{ /* print table size */
		size_t sum_of_len = 0;

		const auto element_size = sizeof( TDB[0][0] );
		const auto pointer_size = sizeof( &TDB.front() );
		for ( const auto& e : TDB ) {
			sum_of_len += e.size();
			len_max = max( len_max, e.size() );
		}
		auto size_all = sum_of_len * element_size;  /* space of element */
		size_all += TDB.size() * pointer_size;  /* space of pointer */

		fout
		<< '\n'
		<< "[TDB size]\n"
		<< "  element size      : " << element_size        << "[bytes]\n"
		<< "  array length sum  : " << sum_of_len          << '\n'
		<< "  memory            = " << size_all            << "[bytes]\n"
		<< '\n'
		<< "  #row              : " << TDB.size()          << '\n'
		<< "  array length max  : " << len_max             << '\n'
		<< endl;
	}

	{ /* set MinimalSupport */
		cout << "MinimalSupport = ";
		cin >> MinSup;
	}


	TProgress Timer;
	vector<TProgress> TimeOfComponents(10);



	fout << "LCM - Table version     started ... " << flush;
	vector<TFrequentPattern>  FP_table;
	{
		Timer.Start();
		vector<int>  ItemFreq( Itemset.size(), 0 );
		ComputeItemFreq( ItemFreq, T );
		vector<int>  Closure;
		ComputeClosure( Closure, T, ItemFreq );
		LCM_on_Table( FP_table, T, vector<int>(), ItemFreq, 0, Itemset.size(),
				MinSup, LCMmax, TimeOfComponents );
		Timer.Stop();
	}
	fout << " done.   ";   Timer.PrintTime( fout );   fout << endl;


	fout << "building ZDD" << endl;
	for ( auto& e : TDB ) { e.resize( len_max + 1, END_SYMBOL ); }
	sort( TDB.begin(), TDB.end() );
	const auto rootnode = BuildFromTable( UTable, TDB, 0, TDB.size(), 0 );
	// IntZDD_Root Z( IFileName, rootnode, Itemset );


	fout << "LCM - ZDD version      started ... " << flush;
	vector<TFrequentPattern>  FP_ZDD;
	{
		Timer.Start();
		LCM_on_ZDD( FP_ZDD, UTable, rootnode, TDB.size(), 0, Itemset.size(),
				MinSup, LCMmax, TimeOfComponents );
		Timer.Stop();
	}
	fout << " done.   ";   Timer.PrintTime( fout );   fout << endl;


	// fout << "LCM - ZDD flag version started ... " << flush;
	// vector<TFrequentPattern>  FP_ZDD_flag;
	// {
	// 	vector<IntZDD_Node*> Nodes;
	// 	for ( auto& e : UTable ) {
	// 		Nodes.push_back( const_cast<IntZDD_Node*>( &e ) );
	// 	}
	// 	vector<int>  ItemFreq( Itemset.size(), 0 );
	// 	ComputeItemFreq_on_ZDD_flag( ItemFreq, rootnode );
	// 	vector<int>  Closure;
	// 	ComputeClosure_on_ZDD_flag( Closure, rootnode, ItemFreq );

	// 	Timer.Start();
	// 	LCM_on_ZDD_flag( FP_ZDD_flag, Nodes, rootnode, Closure, ItemFreq,
	// 			TDB.size(), 0, Itemset.size(), MinSup, TimeOfComponents );
	// 	Timer.Stop();
	// }
	// fout << " done.   ";   Timer.PrintTime( fout );   fout << endl;
	// fout << endl;


	{
		fout << " time of components " << endl;
		fout << "  [0] (Table) BuildSubsetTDB             : ";
		TimeOfComponents[0].PrintAccumulatedTime( fout );
		fout << endl;
		fout << "  [1] (Table) ComputeClosure             : ";
		TimeOfComponents[1].PrintAccumulatedTime( fout );
		fout << endl;
		fout << "  [3] (ZDD) ComputeItemFreq             : ";
		TimeOfComponents[3].PrintAccumulatedTime( fout );
		fout << endl;
		fout << "  [4] (ZDD) ComputePatternFromItemFreq  : ";
		TimeOfComponents[4].PrintAccumulatedTime( fout );
		fout << endl;
		fout << "  [5] (ZDD) BuildContainItem            : ";
		TimeOfComponents[5].PrintAccumulatedTime( fout );
		fout << endl;
		// fout << "  [6] (ZDD) save disable flags          : ";
		// TimeOfComponents[6].PrintAccumulatedTime( fout );
		// fout << endl;
		// fout << "  [7] (ZDD) write back                  : ";
		// TimeOfComponents[7].PrintAccumulatedTime( fout );
		// fout << endl;
		// fout << "  [8] (ZDD) BuildContainItem_flag       : ";
		// TimeOfComponents[8].PrintAccumulatedTime( fout );
		// fout << endl;
		// fout << "  [9] (ZDD) ComputeClosure_on_ZDD_flag : ";
		// TimeOfComponents[9].PrintAccumulatedTime( fout );
		// fout << endl;
		fout << endl;
	}

	{
		fout << "(verify the result) LCMonTable = LCMonZDD?" << endl;
		fout << " -> " << boolalpha << ( FP_table == FP_ZDD ) << endl;
		fout << endl;
		// fout << "(verify the result) LCMonTable = LCMonZDD = LCMonZDDflag?" << endl;
		// fout << " -> " << boolalpha << ( FP_table == FP_ZDD and FP_table == FP_ZDD_flag ) << endl;
		// fout << endl;
	}

	{ /* result */
		fout << "result" << endl;
		PrintFrequentPatterns( FP_table, Itemset, fout );
		fout << endl;
	}


	return 0;
}
