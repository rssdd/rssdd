
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <sys/param.h>
#include <malloc.h>


#include <iostream>
#include <fstream>

#include <bitset>
#include <string>
#include <vector>
#include <stack>             /* used at DB to pair array */
#include <utility>           /* pair, make_pair */
#include <unordered_set>

#include <initializer_list>
#include <iomanip>           /* print */
#include <sstream>           /* StoN, ToString */
#include <algorithm>         /* sort, min_element, unique */


#include "mylib.hpp"
#include "WeightedZDD.hpp"
#include "RSSDD.hpp"

using namespace std;



string VIENNARNA_DIR = "./ViennaRNA-2.2.4";  /* set at the beginning of this program */



/**********************************************************
 functions
 **********************************************************/

double GetPartFunc(
	/*R*/ std::string const &  RNAsequence ,
	/*R*/ double      const    Temperature )
{
	const string CmdLine
		= "echo " + RNAsequence
		  + " | " + VIENNARNA_DIR+"/src/bin/RNAfold --partfunc"
		  + " | " + "sed -ne '/^PartFunc-EnsembleFreeEnergy=/p'"
		  + " | " + "awk  '{ print $2; }'";

	const size_t BUFSIZE = 1024;
	char buf[ BUFSIZE ];
	{  /* pipe to ViennaRNA-RNAfold */
		FILE* fp;
		if ( (fp = popen( CmdLine.c_str(), "r" )) == nullptr ) {
			cout << "ERROR: popen()" << endl;
		}
		while ( fgets(buf, BUFSIZE, fp) != nullptr ) { ; }
		pclose(fp);
	}

	double EnsembleFreeEnergy;
	stringstream ss(buf);
	ss >> EnsembleFreeEnergy;
	return FreeEnergy2Prob( EnsembleFreeEnergy, 1.0, Temperature );
}



bool IsDBAstring(
	/*R*/ std::string const &  DBA )
{
 /* empty */
	if ( DBA.empty() ) return false;

 /* check invalid character */
	for ( const auto & ch : DBA ) {
		if ( (ch != '(') and (ch != ')') and (ch != '.') and (ch != '*') )
			return false;
	}

 /* brace correspondence check */
	Uint brace_left_minus_right = 0;
	for ( const auto & ch : DBA ) {
		if ( ch == '(' )  ++brace_left_minus_right;
		if ( ch == ')' )  --brace_left_minus_right;
	}
	if ( brace_left_minus_right != 0 ) return false;

	return true;
}



void DB2PairArray(
	/*W*/ std::vector<BasePair>       &  PairArray ,
	/*R*/ std::string           const &  DB        )
{
	PairArray.clear();
	PairArray.reserve( 1 + (DB.length() / 2) );
	stack<size_t> left_position;
	for ( size_t i = 0, n = DB.length(); i < n; ++i ) {
		if ( DB[i] == '(' ) {
			left_position.push(i);
		}
		if ( DB[i] == ')' ) {
			PairArray.emplace_back( BasePair(left_position.top(), i) );
			left_position.pop();
		}
	}
	sort( PairArray.begin(), PairArray.end() );
}



bool GetDerivedPattern(
	/*W*/ std::vector<EPairFlag> &  Pattern )
{
	for ( size_t i = 0; i < PATTERN_ROWSIZE; ++i ) {
		for ( size_t j = 0; j < PATTERN_COLSIZE; ++j ) {
			if ( Pattern[ BasePair(i,j) ] == EPairFlag::Pair ) {
				/* conflicting pair --> return -1
				   (check if Pair occur twice in the same row or column) */
				for ( size_t row = 0; row <= j; ++row ) {
					if ( row == i ) continue;
					if ( Pattern[ BasePair(row,j) ] == EPairFlag::Pair ) return false;
					Pattern[ BasePair(row,j) ] = EPairFlag::NotPair;
				}
				for ( size_t col = i; col < PATTERN_COLSIZE; ++col ) {
					if ( col == j ) continue;
					if ( Pattern[ BasePair(i,col) ] == EPairFlag::Pair ) return false;
					Pattern[ BasePair(i,col) ] = EPairFlag::NotPair;
				}
			}
		}
	}
	return true;
}



bool PairArray2Pattern(
	/*W*/ std::vector<EPairFlag>       &  Pattern  ,
	/*R*/ size_t                 const    seqleng  ,
	/*R*/ std::vector<BasePair>  const &  Pairs    ,
	/*R*/ std::vector<BasePair>  const &  NotPairs )
{
	if ( not Pairs.empty() ) {
		if ( Pairs.front() < 0 or Pairs.back() >= PATTERN_SIZE ) {
			cout << "ERROR: invalid value in Pairs" << endl;
			return false;
		}
	}
	if ( not NotPairs.empty() ) {
		if ( NotPairs.front() < 0 or NotPairs.back() >= PATTERN_SIZE ) {
			cout << "ERROR: invalid value in NotPairs" << endl;
			return false;
		}
	}
	InitializePattern( Pattern, seqleng );
	for ( const auto & p : Pairs ) {
		Pattern[p] = EPairFlag::Pair;
	}
	for ( const auto & p : NotPairs ) {
		if ( Pattern[p] == EPairFlag::Pair ) {
			cout << "ERROR: conflict among Pairs and NotPairs" << endl;
			return false;
		}
		Pattern[p] = EPairFlag::NotPair;
	}
	if ( GetDerivedPattern( Pattern ) == false ) {
		cout << "ERROR: conflict among Pairs and NotPairs" << endl;
		return false;
	}
	return true;
}



void DBA2Pattern(
	/*W*/ std::vector<EPairFlag>       &  Pattern ,
	/*R*/ std::string            const &  DBA     )
{
	if ( not IsDBAstring( DBA ) ) return;
	InitializePattern( Pattern, DBA.length() );

	stack<size_t> left_position;
	for ( size_t i = 0, n = DBA.length(); i < n; ++i ) {
		switch ( DBA[i] ) {
		 case '(' :
			left_position.push(i);
			break;
		 case ')' :
			Pattern[ BasePair(left_position.top(), i) ] = EPairFlag::Pair;
			left_position.pop();
			break;
		 case '.' :
			for ( size_t k = 0; k <= i; ++k ) {
				Pattern[ BasePair(k,i) ] = EPairFlag::NotPair;
			}
			for ( size_t k = i; k < PATTERN_COLSIZE; ++k ) {
				Pattern[ BasePair(i,k) ] = EPairFlag::NotPair;
			}
			break;
		 default :
			break;
		}
	}
	GetDerivedPattern( Pattern );
	return;
}



void Pattern2DBA(
	/*W*/ std::string                  &  DBA     ,
	/*R*/ std::vector<EPairFlag> const &  Pattern )
{
	size_t seqleng = 0;
	for ( size_t i = 1; i < PATTERN_COLSIZE; ++i ) {
		if ( Pattern[i] == EPairFlag::Dummy ) {
			seqleng = i;
			break;
		}
	}

	DBA.resize( seqleng );
	fill( DBA.begin(), DBA.end(), DBA_CHAR_NOTPAIR );

	for ( BasePair i = 0; i < PATTERN_SIZE; ++i ) {
		if ( Pattern[i] == EPairFlag::ProbablyNotPair ) {
			DBA[ i.first()  ] = DBA_CHAR_PROBABLY_NOTPAIR;
			DBA[ i.second() ] = DBA_CHAR_PROBABLY_NOTPAIR;
		}
	}
	for ( BasePair i = 0; i < PATTERN_SIZE; ++i ) {
		if ( Pattern[i] == EPairFlag::Undecided ) {
			DBA[ i.first()  ] = DBA_CHAR_UNDECIDED;
			DBA[ i.second() ] = DBA_CHAR_UNDECIDED;
		}
	}
	for ( BasePair i = 0; i < PATTERN_SIZE; ++i ) {
		if ( Pattern[i] == EPairFlag::ProbablyPair ) {
			DBA[ i.first()  ] = DBA_CHAR_PROBABLY_PAIR_FIRST;
			DBA[ i.second() ] = DBA_CHAR_PROBABLY_PAIR_SECOND;
		}
	}
	for ( BasePair i = 0; i < PATTERN_SIZE; ++i ) {
		if ( Pattern[i] == EPairFlag::Pair ) {
			DBA[ i.first()  ] = DBA_CHAR_PAIR_FIRST;
			DBA[ i.second() ] = DBA_CHAR_PAIR_SECOND;
		}
	}
}



void PairFreq2Pattern(
	/*W*/ std::vector<EPairFlag>       &  Pattern  ,
	/*R*/ size_t                 const    seqleng  ,
	/*R*/ std::vector<TFreq>     const &  PairFreq ,
	/*R*/ TFreq                  const    setsize  )
{
	InitializePattern( Pattern, seqleng );

	for ( size_t i = 0; i < PATTERN_SIZE; ++i )
	{
		if ( Pattern[i] == EPairFlag::Dummy )  continue;  /* skip */

		if ( PairFreq[i] == 0 ) {
			Pattern[i] = EPairFlag::NotPair;
		} else if ( PairFreq[i] == setsize ) {
			Pattern[i] = EPairFlag::Pair;
		} else if ( PairFreq[i] <= (1.0 - CURLY_BRACKET_PROBABILITY) * setsize ) {
			Pattern[i] = EPairFlag::ProbablyNotPair;
		} else if ( PairFreq[i] >= CURLY_BRACKET_PROBABILITY * setsize ) {
			Pattern[i] = EPairFlag::ProbablyPair;
		} else {
			Pattern[i] = EPairFlag::Undecided;
		}
	}
}




void CountPairFreq(
	/*W*/ std::vector<TFreq>       &  PairFreq ,
	/*R*/ RSSDD_Node*   const    rootnode )
{
	/***** local functions ****************************/
	function< void   ( RSSDD_Node* const ) >
	subroutine = [&] ( RSSDD_Node* const  N )
	{
		if ( N == nullptr )  return;
		if ( N->IsNode1() )  return;

	 /* N is internal node */
		subroutine( N->hi );
		subroutine( N->lo );
		PairFreq[N->label] += N->hi->weight;
		return;
	};
	/**************************************************/


	fill( PairFreq.begin(), PairFreq.end(), 0 );
	subroutine( rootnode );
}



/**********************************************************
 build RSSDD from data
 **********************************************************/

void BuildFromFile(
	/*W*/ RSSDD_UTable                  &  UTable    ,
	/*W*/ std::vector<RSSDD_Root>       &  Roots     ,
	/*R*/ std::string                  const &  IFileName )
{
	ifstream fin( IFileName );
	if ( !FileCheck( fin, IFileName ) ) return;


	string    RNAsequence;
	double    Temperature = TEMPERATURE_DEFAULT;
	TFreq     sample_number = 0;
	double    sample_prob_sum = 0.0;
	double    PartFunc = 0.0;
	Progress  time_sum;
	Progress  progress;
	vector< pair< vector<BasePair>, TFreq > >  SecstrTable;


	/***** local functions ****************************/
	auto ReadSeqAndTempFromFile = [&] ()
	{
		ifstream fin( IFileName );
		if ( !FileCheck( fin, IFileName ) )  return;

		string buf;
		while ( getline( fin, buf ) ) {
			if ( buf.length() == 0 )  continue;
			if ( buf[0] == '>' )  continue;
			break;
		}
		stringstream  ss(buf);
		ss >> RNAsequence >> Temperature;
		// ss << buf;
		// getline( fin, buf );
		// ss << buf;
		// ss >> Temperature >> RNAsequence;
		Temperature = Celsius2Kelvin( Temperature );
	};

	auto ReadDBFromFile = [&] ()
	{
		string            buf;
		vector<BasePair>  pair_array_buf;
		stringstream      ss;
		string            DB;
		TFreq             freq;
		double            free_energy = 0.0;

		while ( getline( fin, buf ) ) {
			if ( buf.length() == 0 )  continue;  /* discard this line */

			/* reset ss */
			ss.str("");  /* clear buffer */
			ss.clear(stringstream::goodbit);  /* clear the state */

			ss << buf;
			ss >> freq >> DB >> free_energy;
			if ( IGNORE_WEIGHT ) freq = 1;

			if ( not IsDBstring(DB) ) continue;  /* discard this line */

			sample_number   += freq;
			sample_prob_sum += FreeEnergy2Prob( free_energy, PartFunc, Temperature );
			if ( DB.length() > RNA_LENGTH_MAX ) {
				cout
				 << "RNA sequence with >"
				 << RNA_LENGTH_MAX
				 << "bp is not supported" << endl;
				return;
			}

			DB2PairArray( pair_array_buf, DB );
			SecstrTable.emplace_back( pair_array_buf, freq );
		}
		return;
	};


	auto ResizeSecstrTable = [&] ()
	{
		size_t  maxlen = 0;
		size_t  sum_of_len = 0;

		for ( const auto & e : SecstrTable ) {
			maxlen = max( maxlen, e.first.size() );
			sum_of_len += e.first.size();
		}
		++maxlen;

		for ( auto & e : SecstrTable ) {
			e.first.resize( maxlen, END_SYMBOL );
		}

	 /* print table size */
		// size_t each_size = sizeof( SecstrTable.front().first.front() );
		// size_t prob_size = sizeof( SecstrTable.front().second );
		// size_t freq_size = sizeof( SecstrTable.front() );
		// size_t size_all = sum_of_len * each_size + SecstrTable.size() * prob_size + SecstrTable.size() * freq_size;

		// cout
		//  << endl
		//  << "[SecstrTable size]"                                       << endl
		//  << "  A : pair     : " << freq_size              << "[bytes]" << endl
		//  << "  B : arrays   : " << sum_of_len * each_size << "[bytes]" << endl
		//  << "  C : freq     : " << prob_size              << "[bytes]" << endl
		//  << "  D : #sec.str : " << SecstrTable.size()                  << endl
		//  << "  A * D + B + C * D = " << size_all          << "[bytes]" << endl
		//  << endl;
	};

	function< RSSDD_Node*(
		/*R*/ size_t const ,
		/*R*/ size_t const ,
		/*R*/ size_t const ) >
	BuildFromTable = [&] (
		/*R*/ size_t const  row_begin ,
		/*R*/ size_t const  row_end   ,
		/*R*/ size_t const  column    )
		-> RSSDD_Node*
	{ /* range = [row_begin,row_end) */
		if ( row_begin >= row_end )  return nullptr;
		progress.Print( row_begin );

		RSSDD_Node Nnew;
		Nnew.label = (SecstrTable[row_begin].first)[column];

		Nnew.weight = SecstrTable[row_begin].second;
		for ( size_t i = row_begin + 1; i < row_end; ++i ) {
			Nnew.weight += SecstrTable[i].second;
		}

		/* separate [row_begin, row_end)
		 * to [row_begin, row_sep) and [row_sep, row_end) */
		size_t  row_sep = row_begin;
		while ( row_sep < row_end
				and (SecstrTable[row_sep].first)[column] == Nnew.label )
		{ ++row_sep; }

		if ( (SecstrTable[row_begin]).first[column] == END_SYMBOL ) {
			/* line end */
			Nnew.hi = nullptr;
			Nnew.lo = nullptr;
		} else {
			Nnew.hi = BuildFromTable( row_begin, row_sep, column + 1 );
			Nnew.lo = BuildFromTable( row_sep,   row_end, column     );
		}
		return GetNode( UTable, Nnew );
	};
	/**************************************************/


	ReadSeqAndTempFromFile();
	PartFunc = GetPartFunc( RNAsequence, Temperature );


	{ /* read from the file */
		time_sum.Start("reading   ");
		ReadDBFromFile();
		time_sum.Stop(" done. ");
		time_sum.PrintTime();
		time_sum.AccumulateTime();
		cout << endl;
	}

	ResizeSecstrTable();

	{
	 /* sort the table */
		time_sum.Start("sorting   ");
		sort( SecstrTable.begin(), SecstrTable.end() );
		time_sum.Stop(" done. ");
		time_sum.PrintTime();
		time_sum.AccumulateTime();
		cout << endl;
	}

	{
		progress.init( SecstrTable.size() );
		progress.Start("building  ");
		time_sum.Start();
		const auto rootnode
			= BuildFromTable( 0 , SecstrTable.size() , 0 );
		time_sum.Stop(" done. ");
		progress.Stop();
		time_sum.PrintTime();
		time_sum.AccumulateTime();
		cout << endl;

		time_sum.Start("counting nodes   ");
		Roots.emplace_back(
				  IFileName
				, rootnode
				, Roots.size()
				, Roots.size()
				, RNAsequence
				, Temperature
				, sample_number
				, sample_prob_sum
				, PartFunc
		);
		progress.Stop(" done.");
		time_sum.AccumulateTime();
	}

	cout << endl;
	cout << " read + sort + build + countnode = ";
	time_sum.PrintAccumulatedTime();
	cout << endl;


	return;
}



/**********************************************************
 build RSSDD from RSSDD
 **********************************************************/

void MakeRoot(
	/*W*/ std::vector<RSSDD_Root>       &  Roots           ,
	/*W*/ std::string                        &  newname         ,
	/*R*/ RSSDD_Node*             const    rootnode        ,
	/*R*/ Uint                         const    superset_number )
{
	if ( ! Vector::CheckRange( Roots, superset_number ) )  return;
	const RSSDD_Root&  SupersetRoot = Roots[ superset_number ];

	if ( newname == "" ) {
		newname = SupersetRoot.name()
		           + ":rtaddr(" + ToString( rootnode ) + ')';
	}

	Roots.emplace_back(
			  newname
			, rootnode
			, superset_number
			, SupersetRoot.top_level_set()
			, SupersetRoot.RNAsequence()
			, SupersetRoot.Temperature()
			, SupersetRoot.sample_number()
			, SupersetRoot.sample_prob_sum()
			, SupersetRoot.PartFunc()
	);

	return;
}



void SetOperation_main(
	/*W*/ RSSDD_UTable                  &  UTable        ,
	/*W*/ std::vector<RSSDD_Root>       &  Roots         ,
	/*R*/ Uint                         const    root_number_1 ,
	/*R*/ Uint                         const    root_number_2 ,
	/*R*/ char                         const    op            )
{
	if ( ! Vector::CheckRange( Roots, root_number_1 ) )  return;
	if ( ! Vector::CheckRange( Roots, root_number_2 ) )  return;

	const RSSDD_Root Z1 = Roots[root_number_1];
	const RSSDD_Root Z2 = Roots[root_number_2];

	// if ( Z1.top_level_set() != Z2.top_level_set() ) {
		// cout << "ERROR: Union on subsets of different superset" << endl;
		// return;
	// }

	const auto&  SupersetRoot = Roots[ Z1.top_level_set() ];

	{
		Progress progress;
		progress.Start("building     ");
		RSSDD_Node* rootnode;
		switch (op) {
		 case '+' :
			rootnode = BuildUnion( UTable, Z1.rootnode(), Z2.rootnode() );
			break;
		 case '*' :
			rootnode = BuildIntersection( UTable, Z1.rootnode(), Z2.rootnode() );
			break;
		 default :
			break;
		}
		progress.Stop(" done. ");
		progress.PrintTime();
		cout << endl;


		Roots.emplace_back(
				  '(' + Z1.name() + op + Z2.name() + ')'
				, rootnode
				, root_number_1
				, Z1.top_level_set()
				, SupersetRoot.RNAsequence()
				, SupersetRoot.Temperature()
				, SupersetRoot.sample_number()
				, SupersetRoot.sample_prob_sum()
				, SupersetRoot.PartFunc()
		);
	}


	return;
}



void BuildTopK_main(
	/*W*/ RSSDD_UTable                  &  UTable      ,
	/*W*/ std::vector<RSSDD_Root>       &  Roots       ,
	/*R*/ Uint                         const    root_number ,
	/*R*/ Uint                         const    OutputNum   )
{
	if ( ! Vector::CheckRange( Roots, root_number ) ) return;

	const RSSDD_Root&  Z = Roots[root_number];

	if ( OutputNum < 1 ) {
		cout << "ERROR: wrong value for OutputNum" << endl;
		return;
	}

	vector<TFreq>  topKvec;
	TFreq  threshold = TFREQ_MIN;

	GetThreshold( threshold, topKvec, OutputNum, Z.rootnode() );

	{
		Progress progress;
		progress.Start("building top " + ToString(OutputNum) );
		const auto rootnode
			= BuildGreaterThanThreshold( UTable, Z.rootnode(), threshold );
		progress.Stop(" done. ");
		progress.PrintTime();
		cout << endl;

		Roots.emplace_back(
				  Z.name() + ":top" + ToString( OutputNum )
				, rootnode
				, root_number
				, Z.top_level_set()
				, Z.RNAsequence()
				, Z.Temperature()
				, Z.sample_number()
				, Z.sample_prob_sum()
				, Z.PartFunc()
				, Z.MFE()
		);
	}


	return;
}



void BuildSubsetForPattern(
	/*W*/ RSSDD_UTable                  &  UTable      ,
	/*W*/ std::vector<RSSDD_Root>       &  Roots       ,
	/*R*/ Uint                         const    root_number ,
	/*R*/ std::vector<EPairFlag>       const &  Pattern     )
{
	if ( ! Vector::CheckRange( Roots, root_number ) ) return;
	const RSSDD_Root&  Z = Roots[root_number];


	/***** local functions ****************************/
	/* NumOfRemainingPairs is necessary when reached Node1.
	 * even if there is no confilcts in the path to Node1,
	 * this path does not match the Pattern if some pair remained. */
	function< RSSDD_Node*(
		/*R*/ int              const ,
		/*R*/ RSSDD_Node* const ) >
	subroutine = [&] (
		/*R*/ int              const NumOfRemainingPairs ,
		/*R*/ RSSDD_Node* const N                   )
		-> RSSDD_Node*
	{
		if ( N == nullptr ) return nullptr;

		if ( N->IsNode1() ) {
			if ( NumOfRemainingPairs <= 0 ) return N;
			return nullptr;  /* some of pairs is not contained */
		}

		/* N is an internal node */
		RSSDD_Node Nnew;
		switch ( Pattern[ N->label ] ) {
		 case EPairFlag::Pair :
		 {  /* recursive call to hi-edge only */
		 	Nnew.hi = subroutine( NumOfRemainingPairs - 1, N->hi );
		 	if ( Nnew.hi == nullptr ) return nullptr;
		 	Nnew.lo = nullptr;
		 	Nnew.label = N->label;
		 	Nnew.SetWeightSum();
		 	return GetNode( UTable, Nnew );
		 }
		 case EPairFlag::NotPair :
		 {  /* recursive call to lo-edge only */
		 	return subroutine( NumOfRemainingPairs, N->lo );
		 }
		 case EPairFlag::Undecided :
		 {  /* recursive call to hi-edge & lo-edge */
		 	Nnew.hi = subroutine( NumOfRemainingPairs, N->hi );
		 	Nnew.lo = subroutine( NumOfRemainingPairs, N->lo );
		 	if ( Nnew.hi == nullptr ) return Nnew.lo;
		 	Nnew.label = N->label;
		 	Nnew.SetWeightSum();
		 	return GetNode( UTable, Nnew );
		 }
		 default :
		 	break;
		}
		return nullptr;
	};
	/**************************************************/


	{
		Progress progress;
		progress.Start("building     ");
		const auto rootnode
		 = subroutine( CountPairInPattern( Pattern ), Z.rootnode() );
		progress.Stop(" done. ");
		progress.PrintTime();
		cout << endl;

		Roots.emplace_back(
				  Z.name()
				, rootnode
				, root_number
				, Z.top_level_set()
				, Z.RNAsequence()
				, Z.Temperature()
				, Z.sample_number()
				, Z.sample_prob_sum()
				, Z.PartFunc()
				, Z.MFE()
				, Pattern
		);
	}


	return;
}



/**********************************************************
 frequent pattern mining (LCM algorithm)
 **********************************************************/

void LCM(
	/*W*/ RSSDD_UTable                  &  UTable        ,
	/*R*/ std::vector<RSSDD_Root> const &  Roots         ,
	/*R*/ Uint                         const    root_number   ,
	/*R*/ Uint                         const    OutputNum     ,
	/*R*/ bool                         const    LCMmax        ,
	/*R*/ double                       const    MinProb       ,
	/*R*/ Uint                         const    MinNumOfPairs ,
	/*R*/ double                       const    SkipProb      ,
	/*R*/ bool                         const    ToFile        ,
	/*R*/ std::string                  const &  OFileName     )
{
	if ( ! Vector::CheckRange( Roots, root_number ) )  return;
	const RSSDD_Root&  Z = Roots[root_number];

 /* check option values */
	if ( MinProb < 0.0 or 1.0 < MinProb ) {
		cout << "ERROR: wrong value for MinProb" << endl;
		return;
	}
	if ( MinNumOfPairs < 0 ) {
		cout << "ERROR: wrong value for MinNumOfPairs" << endl;
		return;
	}
	if ( SkipProb <= 0.0 or 1.0 < SkipProb ) {
		cout << "ERROR: wrong value for SkipProb" << endl;
		return;
	}
	if ( OutputNum < 0 ) {
		cout << "ERROR: wrong value for OutputNum" << endl;
		return;
	}
	if ( Z.setsize() <= 0 ) {
		cout << "ERROR: set is empty" << endl;
	}

 /* check output file if ToFile is true */
	ofstream fout( OFileName.c_str(), ios::out );
	if ( ToFile and !FileCheck( fout, OFileName ) )  return;
	auto & ost = ( ToFile ? fout : cout );


 /* print settings */
	ost
	 << fixed
	 << endl
	 << "\tOutput top                 " << OutputNum           << endl
	 << "\tMinimum Probability     =  " << MinProb             << endl
	 << "\tMinimum Number of Pairs =  " << MinNumOfPairs       << endl
	 << "\tSkip if Probability     >= " << SkipProb            << endl
	 << "\tMaximal Pattern only       " << boolalpha << LCMmax << endl
	 << endl;


 /* stopwatch and progress meter */
	vector<Progress>  TimeOfComponents(10);
	Progress  progress( Z.setsize() );

 /* frequency & frequent patterns */
	vector<FrequentPattern>  FP;

 /* minimal support */
	TFreq  MinSup = max<TFreq>( 1, Z.Prob2Freq(MinProb) );


	/***** local functions ****************************/
	auto is_maximal = [&] (
		/*R*/ std::vector<TFreq> const &  PairFreq ,
		/*R*/ TFreq              const &  setsize  ,
		/*R*/ int                const    index    )
	{
		for ( int i = index + 1, n = Z.Itemset().size(); i < n; ++i ) {
			const auto item = Z.Itemset()[i];

			/* size of subset of ZDD_for_NewPattern */
			const auto subsetsize = PairFreq[item];

			/* skip item */
			 /* MinimalSupport check */
			if ( subsetsize < MinSup ) continue;
			 /* SkipProb check */
			// if ( SkipProb * setsize <= subsetsize ) continue;
			if ( setsize <= subsetsize ) continue;

			 /* this item can be added to pattern (= not maximal) */
			return false;
		}
		return true;
	};


	function< void(
		/*R*/ RSSDD_Node* const   ,
		/*R*/ TFreq            const & ,
		/*R*/ int              const   ) >
	LCM_Backtrack = [&] (
		/*R*/ RSSDD_Node* const    ZDD_for_Pattern ,
		/*R*/ TFreq            const &  setsize          ,
		/*R*/ int              const    index            )
	{
	 /* compute Item frequency and probability */
		vector<TFreq>  PairFreq( 1 + Z.Itemset().back(), 0 );
		CountPairFreq( PairFreq, ZDD_for_Pattern );

		/* add ZDD_for_Pattern to FP */
		const auto num_of_pairs
			= CountPairInPattern( PairFreq, setsize * SkipProb );

		if ( num_of_pairs >= MinNumOfPairs ) {
			if ( !LCMmax or is_maximal( PairFreq, setsize, index ) ) {
				/* add Pattern to FP */
				Vector::UpdateTopK(
						  FP
						, OutputNum
						, FrequentPattern( setsize, ZDD_for_Pattern )
						, true
						, true );

				/* update minimal support */
				if ( FP.size() == OutputNum ) MinSup = FP.back().freq;

				 /* true : print dotonly */
				progress.Print( MinSup, cout, true );
			}
		}

		for ( int i = index + 1, n = Z.Itemset().size(); i < n; ++i )
		{
			const auto item = Z.Itemset()[i];

			/* size of subset of ZDD_for_NewPattern */
			const auto subsetsize = PairFreq[item];

			/* skip item */
			 /* MinimalSupport check */
			if ( subsetsize < MinSup ) continue;
			 /* SkipProb check */
			if ( SkipProb * setsize <= subsetsize ) continue;

			/* add item to Pattern */
			RSSDD_Node* ZDD_for_NewPattern
				= BuildContainItem( UTable, ZDD_for_Pattern, item );

			/* avoid adding the same pattern twice to FP
			 * ZDD_for_Pattern corresponds to pattern
			 * (PPC check in LCM) */
		//	if ( ZDD_for_NewPattern == nullptr )  continue; /* emptyset? */
			if ( ZDD_for_NewPattern->checked == true ) continue;
			ZDD_for_NewPattern->checked = true;

			/* recursive call */
			LCM_Backtrack( ZDD_for_NewPattern, subsetsize, i );
		}
	};
	/**************************************************/


	{
		ResetNodeFlags( UTable );

		progress.Start();
		LCM_Backtrack( Z.rootnode(), Z.setsize(), -1 );
		progress.Stop(" done. ");
		cout << endl;

	 /* print time */
		ost << endl;
		ost << "\tTime of Components" << endl;
		for ( size_t i = 0; i < TimeOfComponents.size(); ++i ) {
			if ( TimeOfComponents[i].GetAccumulatedTime() == 0.0 ) continue;
			ost << '\t' << setw(5) << right << i << " : ";
			TimeOfComponents[i].PrintAccumulatedTime( ost );
			ost << endl;
		}

		ost << "\tTotal : ";
		progress.PrintTime( ost );
		ost << endl;
	}
	PrintFrequentPatterns( UTable, FP, Z, ost );

	ResetNodeFlags( UTable );

	return;
}



void PrintFrequentPatterns(
	/*W*/ RSSDD_UTable                  &  UTable ,
	/*R*/ std::vector<FrequentPattern> const &  FP     ,
	/*R*/ RSSDD_Root              const &  Z      ,
	/*S*/ std::ostream                       &  ost    )
{
	vector<bool> lr = { 1, 0, 1, 0, 0 };

	vector<size_t> width = {
			  DigitNumber( FP.size() )
			, 12
			, DigitNumber( Z.setsize() )
			, 13
			, Z.RNAsequence().length() + 2 };

	vector<string> header_name = {
			  "Rank"
			, "prob"
			, "freq"
			, "Root addr"
			, "dot-bracket-aster" };

	for ( size_t i = 0; i < width.size(); ++i ) {
		width[i] = max( width[i], header_name[i].length() );
	}


	ost << endl;
	ost << MakeMDTableHeaderString( lr, width, header_name ) << endl;

	ost << fixed;
	for ( size_t i = 0; i < FP.size(); ++i )
	{
		if ( FP[i].rootnode == nullptr ) continue;

		const auto &  fp = FP[i];

		vector<EPairFlag> Pattern(PATTERN_SIZE);
		CalcPattern(
				  Pattern
				, Z.RNAsequence().length()
				, fp.freq
				, fp.rootnode );
		string DBA = Pattern2DBA( Pattern );
		DBA.insert( DBA.begin(), '`' );
		DBA.insert( DBA.end  (), '`' );

		ost
		 << MakeMDTableRowString( lr, width,
			  i + 1
			, Z.Freq2Prob( fp.freq )
			, fp.freq
			, fp.rootnode
			, DBA )
		 << endl;
	}

	ost << endl;
	vector<RSSDD_Node*> rootnodes;
	for ( auto & e : FP ) {
		if ( e.rootnode != nullptr ) {
			rootnodes.push_back( e.rootnode );
		}
	}

	ost << "sum of probability of these sets = ";
	auto UnionAllFP = BuildUnionMulti( UTable, rootnodes );
	if ( UnionAllFP == nullptr ) {
		ost << "0.0" << endl;
	} else {
		ost << Z.Freq2Prob( UnionAllFP->weight ) << endl;
	}
	ost << endl;
}



/**********************************************************
 print
 **********************************************************/

void PrintList(
	/*R*/ std::vector<RSSDD_Root> const &  Roots )
{
	size_t name_lengmax = 20;  /* default */
	for ( const auto & e : Roots ) {
		name_lengmax = max( name_lengmax, e.name().length() );
	}

	vector<bool> lr = {  /* left : 0, right : 1 */
			1, 1, 1, 0, 1,
			1, 1, 1, 1, 1,
			1, 0, 1, 1, 1 };


	vector<size_t> width = {
			 3,  3,  3, name_lengmax, 3,
			10, 10, 10, 13, 6,
			 0, 13,  6, 10, 10 };

	vector<string> header_name = {
			"No.", "top", "parent", "name", "seq.len",
			"#set uniq", "#set", "prob sum", "MFE", "Temp",
			"", "address", "height", "#node", "#node(tree)"};

	for ( size_t i = 0; i < width.size(); ++i ) {
		width[i] = max( width[i], header_name[i].length() );
	}



	cout << endl;
	cout << MakeMDTableHeaderString( lr, width, header_name ) << endl;

	cout << fixed;
	for ( size_t i = 0; i < Roots.size(); ++i ) {
		const RSSDD_Root&  Z = Roots[i];

		cout
		 << MakeMDTableRowString( lr, width,
			  i
			, Z.top_level_set()
			, Z.superset()
			, Z.name()
			, Z.RNAsequence().length()
			, Z.setsize_uniq()
			, Z.setsize()
			, Z.set_prob()
			, Z.MFE()
			, Z.Temperature()
			, ""
			, Z.rootnode()
			, Z.height()
			, Z.nodenum()
			, Z.nodenum_as_tree() )
		 << endl;
	}

	cout << endl;
}

