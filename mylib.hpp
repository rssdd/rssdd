
#pragma once


/************************************************
 # comments
 - argument types
 	* W = Write.  rewrite the passed argument
 	* R = Read.   read-only argument

 * Tested on g++ (Ubuntu 4.8.1-2ubuntu1~12.04) 4.8.1
 * 2016/6/22
 * Hideaki Noshiro
 ************************************************/

#include <readline/readline.h>
#include <readline/history.h>

#include <iostream>
#include <sstream>     /* StoN, NtoS */

#include <iterator>    /* distance */
#include <vector>
#include <deque>
#include <unordered_set>
#include <string>
#include <type_traits> /* is_integral, */
#include <utility>     /* pair */
#include <algorithm>

#include <iomanip>     /* setw */
#include <sys/time.h>  /* Progress - stopwatch */
#include <cstdint>     /* uint_fast32_t, ... */
#include <cmath>       /* round */

// #include <boost/operators.hpp>






/**********************************************************
 alias
 **********************************************************/

/* integer (c++11) */
using Uint    = unsigned int;

using Uint8   = std::uint_least8_t;   /*    8bit unsigned integer */
using Uint16  = std::uint_least16_t;  /*   16bit unsigned integer */
using Uint32  = std::uint_least32_t;  /*   32bit unsigned integer */
using Uint64  = std::uint_least64_t;  /*   64bit unsigned integer */
using Uintmax = std::uintmax_t;      /* maximum unsigned integer */

using Int8   = std::int_least8_t;    /*    8bit signed integer */
using Int16  = std::int_least16_t;   /*   16bit signed integer */
using Int32  = std::int_least32_t;   /*   32bit signed integer */
using Int64  = std::int_least64_t;   /*   64bit signed integer */
using Intmax = std::intmax_t;        /* maximum signed integer */



/**********************************************************
 functions prototypes
 **********************************************************/

/* wrapper for gnu readline */
inline bool ReadlineString(
	/*W*/   std::string       &  command_line
	/*R*/ , std::string const &  prompt = ""  )
{
	char* cmdline_c_str = nullptr;
	if ( ( cmdline_c_str = readline( prompt.c_str() ) ) == nullptr )
		return false;
	command_line = std::string(cmdline_c_str);
	free(cmdline_c_str);
	return true;
}



/* wrapper for gnu readline */
inline void Add2History(
	/*R*/ std::string const &  str )
{
	add_history( str.c_str() );
}



/* overload "<<" of Uint8 */
inline std::ostream & operator<<(
	/*W*/   std::ostream       &  ost
	/*R*/ , Uint8        const    num )
{
	ost << std::setw(3) << static_cast<Uint>( num );
	return ost;
}



/* overload "<<" of Uint16 */
inline std::ostream & operator<<(
	/*W*/   std::ostream       &  ost
	/*R*/ , Uint16       const    num )
{
	ost << std::setw(5) << static_cast<Uint>( num );
	return ost;
}



/* x^y */
inline Intmax Power(
	/*R*/   Intmax  const  x
	/*R*/ , Uintmax const  y )
{
	if ( y == 0 )  return 1;
	const Intmax half = Power( x, y / 2 );
	return ( (y % 2 == 0) ? 1 : x ) * half * half;
}



/* DigitNumber(12691625) = 8 */
inline Uint DigitNumber(
	/*R*/ Uintmax const  x )
{
	return std::log10(x) + 1;
}



/* parse */
inline bool StringFound(
	/*R*/   std::string const &  target
	/*R*/ , std::string const &  search_string )
{
	return ( target.find( search_string ) != std::string::npos );
}



/* extract positive decimal number from {str},
 * overwrite the number in {str} by '_' */
int ExtractNumber(
	/*W*/ std::string &  str );


/* '+' -> 1;   '-' -> -1;   not found -> 0 */
int ExtractPlusMinus(
	/*W*/ std::string &  str );



bool FileCheck(
	        std::ifstream       &  fin
	/*R*/ , std::string   const &  Ifilename );


bool FileCheck(
	        std::ofstream       &  fout
	/*R*/ , std::string   const &  Ofilename );


/* convert variable-length arguments to vector of strings
 * VariadicToString( 1, 2.3, "4 5" )
 *  --> [ "1", "2.3", "4 5" ] */
inline void VariadicToString(
	/*W*/ std::vector< std::string > &  strings ) {} /* dummy */

template < class Head, class ...Rest >
void VariadicToString(
	/*W*/   std::vector< std::string > &       strings
	/*R*/ , Head                       &&      head
	/*R*/ , Rest                       && ...  rest )
{
	std::stringstream  ss;
	ss << head;
	strings.push_back( ss.str() );
	VariadicToString( strings, std::move(rest)... );
}



/* make a string of Markdown table header
 * lr = "01011" -> left, right, left, right, right */
std::string MakeMDTableHeaderString(
	/*R*/   std::vector<bool>        const &  lr
	/*R*/ , std::vector<size_t>      const &  width
	/*R*/ , std::vector<std::string> const &  header_name );


/* make a string of Markdown table row
 * lr = "01011" -> left, right, left, right, right */
template <class... Args>
std::string MakeMDTableRowString(
	/*R*/   std::vector<bool>   const &  lr
	/*R*/ , std::vector<size_t> const &  width
	/*R*/ , Args...                      args )
{
	std::vector< std::string > strings;
	VariadicToString( strings, std::move(args)... );

	if ( lr.size() != width.size() or lr.size() != strings.size() ) {
		std::cout << "ERROR: mismatch in size" << std::endl;
		return "";
	}

	std::stringstream  ss;

	for ( size_t i = 0; i < strings.size(); ++i ) {
		ss << "| " << (lr[i] ? std::right : std::left);
		ss << std::setw(width[i]);
		ss << strings[i] << ' ';
	}
	ss << '|';

	return ss.str();
}



/**********************************************************
 template functions
 **********************************************************/

/* convert string to number */
template <typename TN>
TN StoN(
	/*R*/ std::string const &  String )
{
	std::istringstream ss( String );
	TN  Number;
	ss >> Number;
	return Number;
}


/* convert to string */
template <typename TN>
std::string ToString(
	/*R*/ TN const  arg )
{
	std::ostringstream ss;
	ss << arg;
	return ss.str();
}


/* overload "<<" of std::pair */
template <typename TN1, typename TN2>
std::ostream & operator<<(
	        std::ostream              &  ost
	/*R*/ , std::pair<TN1, TN2> const &  p )
{
	ost << '(' << p.first << ',' << p.second << ')';
	return ost;
}



/* overload "<<" of deque */
template <typename TN>
std::ostream & operator<<(
	        std::ostream         &  ost
	/*R*/ , std::deque<TN> const &  dq )
{
	if ( dq.empty() ) {  ost << "[ ]";  return ost;  }

	ost << "[ ";
	for( size_t i = 0; i < dq.size() - 1; ++i ) {
		ost << std::dec << dq[i] << ", ";
	}
	ost << std::dec << dq.back() << " ]";
	return ost;
}



/* overloading of << of vector */
template <typename TN>
std::ostream & operator<<(
	        std::ostream          &  ost
	/*R*/ , std::vector<TN> const &  vec )
{
	if ( vec.empty() ) {  ost << "[ ]";  return ost;  }

	ost << "[ ";
	for( size_t i = 0; i < vec.size() - 1; ++i ) {
		ost << std::dec << vec[i] << ", ";
	}
	ost << std::dec << vec.back() << " ]";
	return ost;
}




/* vector operations */
namespace Vector {

	template <typename TN>
	inline bool CheckRange(
		/*R*/ std::vector<TN> const &  vec ,
		/*R*/ size_t          const    pos )
	{
		if ( pos < 0 or vec.size() <= pos ) {
			std::cout << "ERROR at Vector::CheckRange : out of range" << std::endl;
			return false;
		}
		return true;
	}


	/* if all elements in vec is equal each other --> true */
	template <typename TN>
	inline bool AllEqual(
		/*R*/ std::vector<TN> const &  vec )
	{
		for ( auto & e : vec ) {
			if ( e != vec.front() ) return false;
		}
		return true;
	}



	/* assume TN is number */
	template <typename TN>
	inline TN Sum(
		/*R*/ std::vector<TN> const &  vec )
	{
		TN sum = 0;
		for ( const auto & e : vec )  sum += e;
		return sum;
	}


	template <typename TN>
	inline TN Max(
		/*R*/ std::vector<TN> const &  vec )
	{
		return *std::max_element( vec.cbegin(), vec.cend() );
		// TN max = vec.front();
		// for ( const auto & e : vec )  max = std::max( max, e );
		// return max;
	}



	template <typename TN>
	inline void Swap2pos(
		/*W*/ std::vector<TN>       &  vec  ,
		/*R*/ size_t          const    pos1 ,
		/*R*/ size_t          const    pos2 )
	{
		std::iter_swap( vec.begin() + pos1, vec.begin() + pos2 );
		// TN tmp    = vec[pos1];
		// vec[pos1] = vec[pos2];
		// vec[pos2] = tmp;
	}



	/* if elem is in vec --> true */
	template <typename TN>
	inline bool IsIn(
		/*R*/ TN              const &  elem           ,
		/*R*/ std::vector<TN> const &  vec            ,
		/*R*/ bool            const    sorted = false )
	{
		if (sorted) {
			return std::binary_search( vec.cbegin(), vec.cend(), elem );
		}
		return ( std::find( vec.cbegin(), vec.cend(), elem ) != vec.end() );
	}



	template <typename TN>
	inline intptr_t FindPos(
		/*R*/ std::vector<TN> const &  vec  ,
		/*R*/ TN              const &  elem )
	{
		if ( vec.empty() )  return -1;
		for ( auto it = vec.cbegin(); it != vec.cend(); ++it ) {
			if ( *it == elem ) return std::distance( vec.cbegin(), it );
		}
		
		return -1;
	}



	/* find and returns the position of elem in vec 
	 * (std::binary_search only returns true or false  */
	template <typename TN>
	intptr_t FindPos_byBinarySearch(
		/*R*/ std::vector<TN> const &  vec                ,
		/*R*/ TN              const &  elem               ,
		/*R*/ bool            const    decsending = false )
	{
		if ( vec.empty() )  return -1;

		size_t left = 0;
		size_t right = vec.size();
		while ( left < right ) {
			const size_t c = (left + right) / 2; /* center */
			if ( elem == vec[c] ) {  /* the same element found */
				return c;
			} else if ( decsending  ?  elem < vec[c]  :  elem > vec[c] ) {
				left = c + 1;  /* update left most */
			} else {
				right = c;  /* update right most */
			}
		}
		return -1;
	}



	/* insert elem to vec */
	template <typename TN>
	inline void Insert(
		/*W*/ std::vector<TN>       &  vec  ,
		/*R*/ TN              const &  elem ,
		/*R*/ size_t          const    pos  )
	{
		auto it = std::next( vec.begin(), pos );  /* go forward to pos */
		vec.insert( it, elem );
	}


	/* insert elem to vec 
	 * set decsending as true if vec is sorted by decsending order
	 * unique : if true, do not insert elem if the same value exists in vec */
	template <typename TN>
	intptr_t Insert_byBinarySearch(  /* return inserted position */
		/*W*/ std::vector<TN>       &  vec                ,
		/*R*/ TN              const &  elem               ,
		/*R*/ bool            const    decsending = false ,
		/*R*/ bool            const    unique     = false )
	{
		if ( vec.empty() ) {
			vec.push_back( elem );
			return 0;
		}

		size_t left = 0;
		size_t right = vec.size();
		while ( left < right ) {
			const size_t c = (left + right) / 2; /* center */
			if ( elem == vec[c] ) {  /* the same element found */
				if ( not unique )  Vector::Insert( vec, elem, c );
				return c;
			} else if ( decsending  ?  elem < vec[c]  :  vec[c] < elem ) {
				left = c + 1;  /* update left most */
			} else {
				right = c;  /* update right most */
			}
		}

		/* case : left == right */
		if ( unique and vec[left] == elem ) return left;

		Vector::Insert( vec, elem, ( elem < vec[left] ? left : right ) );

		return left;
	}


	/* assume sorted vector, shrink the same elements to be unique */
	template <typename TN>
	inline void Unique(
		/*W*/ std::vector<TN> & vec )
	{
		vec.erase( unique( vec.begin(), vec.end() ), vec.end() );
	}


	/* update sorted vector of max length K by elem */
	template <typename TN>
	inline void UpdateTopK(
		/*W*/ std::vector<TN>       &  vec               ,
		/*R*/ Uint            const    K                 ,
		/*R*/ TN              const &  elem              ,
		/*R*/ bool            const    decsending = true ,
		/*R*/ bool            const    unique = false    )
	{
		Vector::Insert_byBinarySearch( vec, elem, decsending, unique );
		if ( vec.size() > K )  vec.resize(K);
	}
}





template <typename TN>
inline bool Uset_IsIn(
	/*R*/ TN                     const &  elem ,
	/*R*/ std::unordered_set<TN> const &  us   )
{
	return us.find(elem) != us.end();
}



/* get option value of command {option_name}
 * cmds is the vector of commands
 * assume that TN is numeric type  */
template <typename TN>
TN GetOptionValue(
	/*R*/ std::vector<std::string> const &  cmds        ,
	/*R*/ std::string              const &  option_name )
{
	const auto pos = Vector::FindPos( cmds, option_name );
	if ( pos == cmds.size() - 1 ) return -1;
	if ( cmds[pos + 1] == "" ) return -1;

	return StoN<TN>( cmds[pos + 1] );
}



/**********************************************************
 classes
 **********************************************************/

class Progress{

private:
	double counter_;
	Uint   meter10percent_;
	double datasize_10percent_;
	double datasize_10Xpercent_;
	double datasize_2percent_;
	double datasize_2Xpercent_;
	double start_time_;
	double stop_time_;
	double time_accumulated_;
public:
	Progress()
		: counter_            (1.0)
		, meter10percent_     (0)
		, datasize_10percent_ (0.0)
		, datasize_10Xpercent_(0.0)
		, datasize_2percent_  (0.0)
		, datasize_2Xpercent_ (0.0)
		, time_accumulated_   (0.0)
	{}

	Progress( const double  datasize )
		: counter_            (1.0)
		, meter10percent_     (0)
		, datasize_10percent_ (datasize / 10.0)
		, datasize_10Xpercent_(0.0)
		, datasize_2percent_  (datasize / 50.0)
		, datasize_2Xpercent_ (0.0)
		, time_accumulated_   (0.0)
	{}

	void init( const double  datasize )
	{
		counter_             = (1.0);
		meter10percent_      = (0);
		datasize_10percent_  = (datasize / 10.0);
		datasize_10Xpercent_ = (0.0);
		datasize_2percent_   = (datasize / 50.0);
		datasize_2Xpercent_  = (0.0);
		time_accumulated_    = (0.0);
	}

	~Progress() {}

	void Start(
		/*R*/ std::string  const &  str = ""        ,
		      std::ostream       &  ost = std::cout )
	{
		ost << str << std::flush;
		struct timeval t;
		gettimeofday( & t, nullptr );
		start_time_  = static_cast<double>( t.tv_sec );
		start_time_ += static_cast<double>( t.tv_usec * 1e-6 );
	}

	void Stop(
		/*R*/ std::string  const &  str = ""        ,
		      std::ostream       &  ost = std::cout )
	{
		ost << str << std::flush;
		struct timeval t;
		gettimeofday( & t, nullptr );
		stop_time_  = static_cast<double>( t.tv_sec );
		stop_time_ += static_cast<double>( t.tv_usec * 1e-6 );
	}


	void IncrCounter() { counter_ += 1.0; }

	void IncrCounter( const double  n ) {
		counter_ += static_cast< decltype(counter_) >(n);
	}

	void SetCounter ( const double  n ) {
		counter_  = static_cast< decltype(counter_) >(n);
	}

	void Print(
		        std::ostream       & ost = std::cout
		/*R*/ , bool         const   dotonly = false )
	{
		if ( counter_ >= datasize_10Xpercent_ ) {
			if ( dotonly ) {
				ost << '.' << std::flush;
			} else {
				ost << ' ' << meter10percent_ << "% " << std::flush;
				meter10percent_ += 10;
			}
			datasize_10Xpercent_ += datasize_10percent_;
			datasize_2Xpercent_ += datasize_2percent_;
		} else if ( counter_ >= datasize_2Xpercent_ ) {
			ost << '.' << std::flush;
			datasize_2Xpercent_ += datasize_2percent_;
		}
	}

	void Print(
		/*R*/ double       const    d               ,
		      std::ostream       &  ost = std::cout ,
		/*R*/ bool         const    dotonly = false )
	{
		SetCounter(d);
		Print( ost, dotonly );
	}

	double GetTime() const {
		return stop_time_ - start_time_;
	}

	void PrintTime( std::ostream & ost = std::cout ) const
	{
		ost << ' ' << std::fixed << GetTime() << "[s]";
	}

	void AccumulateTime() {
		time_accumulated_ += ( stop_time_ - start_time_ );
	}

	double GetAccumulatedTime() const {
		return time_accumulated_;
	}

	void PrintAccumulatedTime( std::ostream & ost = std::cout ) const {
		ost << ' ' << std::fixed << time_accumulated_ << "[s]";
	}
};




