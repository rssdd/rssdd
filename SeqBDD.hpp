
#pragma once

/******************************************************************************
 * representation of sets of sequences by ZDD

 * GNU readline
 $ sudo apt-get install libreadline6 libreadline6-dev

 * Use compile option "-std=c++11" and "-lreadline"
 "$  g++ -std=c++11 -o SeqBDD.out Basic.cpp SeqBDD.cpp -lreadline"

 * Tested on g++ (Ubuntu 4.8.1-2ubuntu1~12.04) 4.8.1

 2016/3/29
 Hideaki Noshiro
 ******************************************************************************/

#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>
#include <iomanip>   /* print */
#include <cfloat>
#include <utility>   /* rel_ops */
#include <limits>    /* numeric_limits<char> */

#include "Basic.hpp"
#include "WeightedZDD.hpp"



/******************************************************************************
 settings
 ******************************************************************************/

constexpr bool PRINT_SETTINGS = true;

/* XY-pic setting */
constexpr int GRAPH_NODE_DISTANCE_Y = 15;
constexpr int GRAPH_NODE_DISTANCE_X = 30;

constexpr int SEQUENCE_LENGTH_MAX = 1000;  /* = max depth of SeqBDD */



/******************************************************************************
 alias
 ******************************************************************************/

using TChar   = char;  /* 1 byte character */
using TWeight = double;

using SeqBDD_Node   = WeightedZDD_Node< TChar, TWeight >;
using SeqBDD_UTable = WeightedZDD_UTable< SeqBDD_Node >;



/******************************************************************************
 definition of constants
 ******************************************************************************/

constexpr TChar   TCHAR_MAX   = std::numeric_limits<TChar>::max();
constexpr TChar   TCHAR_MIN   = std::numeric_limits<TChar>::min();
constexpr TWeight TWEIGHT_MAX = std::numeric_limits<TWeight>::max();
constexpr TWeight TWEIGHT_MIN = std::numeric_limits<TWeight>::min();

constexpr TChar ENDCHAR = TCHAR_MAX;

/* strings */
extern const std::string PROMPT_STRING;
extern const std::string HELP_FILE_NAME;
extern const std::string HISTORY_FILE_NAME;



/******************************************************************************
 prototypes
 ******************************************************************************/

class SeqBDD_Root;


std::vector<TChar> Str2CharVect( const std::string& str );
std::string CharVect2Str( const std::vector<TChar>& char_vect );

void PrintList( const std::vector<SeqBDD_Root>& SeqBDDs );



/******************************************************************************
 class
 ******************************************************************************/

class SeqBDD_Root : public WeightedZDD_Root<TChar, TWeight>
{
 private:
	std::vector<TChar> alphabet__;

	void GetAlphabet__(
			std::vector<bool>&  exist_alphabet,
			SeqBDD_Node* const  N              )
	{
		if ( N == nullptr || N->IsNode1() ) return;
		/* N is an internal node */
		exist_alphabet[ N->label ] = true;
		GetAlphabet__( exist_alphabet, N->hi );
		GetAlphabet__( exist_alphabet, N->lo );
		return;
	}

 public:
	SeqBDD_Root(
			const std::string&  init_name          ,
			SeqBDD_Node* const  init_rootnode      ,
			const int           init_superset = -1 )
	 : WeightedZDD_Root( init_name, init_rootnode, init_superset )  /* call super class constructor */
	{
		std::vector<bool> exist_alphabet( TCHAR_MAX, false );
		GetAlphabet__( exist_alphabet, rootnode() );
		for ( int i = 0; i < TCHAR_MAX; ++i ) {
			if ( exist_alphabet[i] ) alphabet__.push_back(i);
		}
	}

	/* getter */
	std::vector<TChar> Alphabet() const { return alphabet__  ;  }


	virtual void PrintHeader( std::ostream& ost ) const
	{  /* used in PrintCombination() */
		ost << std::left << std::setw( height_hi() + 5 ) << "string" << "weight" << std::endl;
	}

	virtual void ConvertAndPrint(
			std::ostream&              ost         ,
			const std::vector<TChar>&  Combination ) const
	{  /* used in PrintCombination() */
		ost << std::left << std::setw( height_hi() + 5 ) << CharVect2Str( Combination );
	}

	bool ExistString( std::string& query_str ) const
	{
		std::vector<TChar> char_vect = Str2CharVect( query_str );
		return ExistCombination( char_vect );
	}

};
