
all: main_RSSDD  main_Table_querytime  main_RSSDD_querytime  main_Table_querytime2  main_RSSDD_querytime2


main_RSSDD:   mylib.o  RSSDD.o  main_RSSDD.o
	g++ -Wall -O3 -std=c++11 -o  main_RSSDD.out  mylib.o  RSSDD.o  main_RSSDD.o  -lreadline


mylib.o:       mylib.hpp  mylib.cpp
	g++ -Wall -O3 -std=c++11 -c  mylib.cpp

RSSDD.o:  mylib.o  WeightedZDD.hpp  RSSDD.hpp  RSSDD.cpp
	g++ -Wall -O3 -std=c++11 -c  RSSDD.cpp

main_RSSDD.o: mylib.o  RSSDD.hpp  main_RSSDD.cpp
	g++ -Wall -O3 -std=c++11 -c  main_RSSDD.cpp  -lreadline



main_Table_querytime:  mylib.o  RSSDD.o  main_Table_querytime.o
	g++ -Wall -O3 -std=c++11 -o  main_Table_querytime.out  mylib.o  RSSDD.o  main_Table_querytime.o  -lreadline

main_Table_querytime.o: mylib.o  RSSDD.hpp  main_Table_querytime.cpp
	g++ -Wall -O3 -std=c++11 -c  main_Table_querytime.cpp  -lreadline


main_RSSDD_querytime:  mylib.o  RSSDD.o  main_RSSDD_querytime.o
	g++ -Wall -O3 -std=c++11 -o  main_RSSDD_querytime.out  mylib.o  RSSDD.o  main_RSSDD_querytime.o  -lreadline

main_RSSDD_querytime.o: mylib.o  RSSDD.hpp  main_RSSDD_querytime.cpp
	g++ -Wall -O3 -std=c++11 -c  main_RSSDD_querytime.cpp  -lreadline



main_Table_querytime2:  mylib.o  RSSDD.o  main_Table_querytime2.o
	g++ -Wall -O3 -std=c++11 -o  main_Table_querytime2.out  mylib.o  RSSDD.o  main_Table_querytime2.o  -lreadline

main_Table_querytime2.o: mylib.o  RSSDD.hpp  main_Table_querytime2.cpp
	g++ -Wall -O3 -std=c++11 -c  main_Table_querytime2.cpp  -lreadline

main_RSSDD_querytime2:  mylib.o  RSSDD.o  main_RSSDD_querytime2.o
	g++ -Wall -O3 -std=c++11 -o  main_RSSDD_querytime2.out  mylib.o  RSSDD.o  main_RSSDD_querytime2.o  -lreadline

main_RSSDD_querytime2.o: mylib.o  RSSDD.hpp  main_RSSDD_querytime2.cpp
	g++ -Wall -O3 -std=c++11 -c  main_RSSDD_querytime2.cpp  -lreadline






clean:
	rm -f *.o
