#!/bin/bash


##### how to use this script #####
# (example)
#
# ./MakeInputFile.sh  riboswitch_alanine  10000000
#
#


##### modify below #####


# directory
sequence_dir='/home/hideaki/BDD/inputdata/fasta/S-151Rfam'
output_dir='/home/hideaki/BDD/outputdata'
sort_dir='/home/hideaki/BDD/outputdata'
# output_dir='/media/sf_T_DRIVE/RNAsubopt/temp'
# sort_dir='/media/sf_T_DRIVE/RNAsubopt/temp/sorttemp'


##### extension #####
sequence_extension='fasta'
output_extention='db'


#########################################################

function RNAsubopt_uniq_count_RNAeval () {
	sequence_name=$1
	sample_number=$2
	temperature=$3
	filename_sequence=${sequence_dir}/${sequence_name}.${sequence_extension}
	output_filename=${sequence_name}_p${sample_number}_${temperature}C
	filename_RNAsubopt=${output_dir}/${output_filename}_RNAsubopt.${output_extention}
	filename_sort=${output_dir}/${output_filename}_sort.${output_extention}
	filename_uniq_count=${output_dir}/${output_filename}_uniq_count.${output_extention}
	filename_RNAeval=${output_dir}/${output_filename}_RNAeval.${output_extention}
	filename_output=${output_dir}/${output_filename}.${output_extention}

	# while read line
	# do
	# 	sequence=${line}
	# done <${filename_sequence}
	sequence=`sed -ne '2p' ${filename_sequence}`

# echo ${sequence_name}
# echo ${sample_number}
# echo ${filename_sequence}
# echo ${filename_RNAsubopt}
# echo ${filename_sort}
# echo ${filename_uniq_count}
# echo ${filename_output}
# echo ${sequence}

 # RNAsubopt (stochastic sampling)
	if [ ! -e ${filename_RNAsubopt} ]
	 then
		echo -ne '\t'${output_filename} "RNAsubopt   "
		date +%m/%d_%H:%M:%S
		RNAsubopt -p ${sample_number} -T ${temperature} < ${filename_sequence} > ${filename_RNAsubopt}
	fi

 # sort
	if [ ! -e ${filename_sort} ]
	 then
		echo -ne '\t'${output_filename} "sort        "
		date +%m/%d_%H:%M:%S
		cat ${filename_RNAsubopt} | sed -e '1d' | LC_ALL=C sort -T${sort_dir} > ${filename_sort}
	fi

 # # uniq -c
	if [ ! -e ${filename_uniq_count} ]
	 then
		echo -ne '\t'${output_filename} "uniq&count  "
		date +%m/%d_%H:%M:%S
		cat ${filename_sort} | uniq -c > ${filename_uniq_count}
	fi

 # # RNAeval
	if [ ! -e ${filename_RNAeval} ]
	 then
		echo -ne '\t'${output_filename} "RNAeval     "
		date +%m/%d_%H:%M:%S

		# cat     --> "   23  CCGCACAGCGGGCAGUGCCC"
		# awk     --> "CCGCACAGCGGGCAGUGCCC\n((((...))))((...)).."
		# RNAeval --> "CCGCACAGCGGGCAGUGCCC\n((((...))))((...)).. (-4.00)\n"
		# sed     --> "((((...))))((...)).. (-4.00)\n"   ### (discard line 1)
		# sed     --> "((((...))))((...)).. -4.00\n"     ### (remove brace)
		# awk     --> "-4.00\n"                          ### (output column 2)

		cat ${filename_uniq_count}                                        \
		 | awk -v sequence="${sequence}" '{ print sequence; print $2 }'   \
		 | RNAeval                                                        \
		 | sed -ne 0~2p                                                   \
		 | sed -e 's/ (/ /' -e 's/)$//'                                   \
		 | awk '{print $2}' > ${filename_RNAeval}
	fi


 # # output
	if [ ! -e ${filename_output} ]
	 then
		echo -ne '\t'${output_filename} "output      "
		date +%m/%d_%H:%M:%S

		echo ${sequence} "    " ${temperature} > ${filename_output}

		# concatinate each line of 2 files
		paste ${filename_uniq_count} ${filename_RNAeval} >> ${filename_output}
	fi


	SIZE=`wc -c < ${filename_output}`
	if [ -e ${filename_output} -a ${SIZE} -gt 20 ]
	 then
		rm ${filename_RNAsubopt}  ; echo -e '\t'removed ${filename_RNAsubopt}
		rm ${filename_sort}       ; echo -e '\t'removed ${filename_sort}
		rm ${filename_uniq_count} ; echo -e '\t'removed ${filename_uniq_count}
		rm ${filename_RNAeval}    ; echo -e '\t'removed ${filename_RNAeval}
	fi
}


# Call
RNAsubopt_uniq_count_RNAeval  $1 $2 $3

# [how to use]
# 
# ./MakeInputFile.sh trna 10000 37.0
#                     |     |    |
#                     |     |    +--temperature
#                     |     +-------sample number
#                     +-------------sequence name (fasta filename without extension)

