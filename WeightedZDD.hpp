
#pragma once

/**********************************************************
 # required libraries
 - GNU readline
    $  sudo apt-get install libreadline6 libreadline6-dev


 # Use compile option "-std=c++11" and "-lreadline"
    $  g++ -std=c++11  -o ***.out  mylib.cpp  ***.cpp  -lreadline


 # comments
 - argument types
 	* W = Write.  rewrite the passed argument
 	* R = Read.   read-only argument
 	* S = Stream. input or output stream
   (example)
    *R* ZDD_Node* const  rootnode ,
   --> read-only, pointer to the node is constant 


 * Tested on g++ (Ubuntu 4.8.1-2ubuntu1~12.04) 4.8.1
 * 2016/6/22
 * Hideaki Noshiro
 **********************************************************/

#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <bitset>
#include <unordered_set>
#include <unordered_map>
#include <iomanip>       /* print */
#include <cfloat>
#include <utility>       /* rel_ops */
#include <tuple>

#include "mylib.hpp"
// #include "ZDD.hpp"



/**********************************************************
 prototypes
 **********************************************************/

template <typename TItem, typename TWeight> class WeightedZDD_Node;
template <typename TItem, typename TWeight> class WeightedZDD_Root;
template <typename TNode> class HashFuncN2I;



/**********************************************************
 definition of constants
 **********************************************************/

/* for hash function */
constexpr size_t PRIME_NUMBER1 =   9999991;
// constexpr size_t PRIME_NUMBER =  49999991;
// constexpr size_t PRIME_NUMBER =  99999989;
// constexpr size_t PRIME_NUMBER = 199999991;
// constexpr size_t PRIME_NUMBER = 299999977;
constexpr size_t PRIME_NUMBER2 = 999999937;


/**********************************************************
 alias
 **********************************************************/

template <typename TNode>
using WeightedZDD_UTable = std::unordered_set< TNode, HashFuncN2I<TNode> >;



/**********************************************************
 class
 **********************************************************/

template <typename TItem, typename TWeight>
class WeightedZDD_Node
{
	using TNode = WeightedZDD_Node<TItem,TWeight>;

 public:
	TNode*   hi;
	TNode*   lo;
	TItem    label;
	TWeight  weight;  /* weight sum of ZDD under this node */
//	int      memo;    /* used in LCM::CountItemFreq */
//	bool     visited;
	bool     checked;

	WeightedZDD_Node() {}

	WeightedZDD_Node( TNode* const  Nright )
		: hi      (Nright->hi)
		, lo      (Nright->lo)
		, label   (Nright->label)
		, weight  (Nright->weight)
		// , memo    (0)
		// , visited (false)
		, checked (false)
	{}

	WeightedZDD_Node( TNode const &  Nright )
		: hi      (Nright.hi)
		, lo      (Nright.lo)
		, label   (Nright.label)
		, weight  (Nright.weight)
		// , memo    (0)
		// , visited (false)
		, checked (false)
	{}

	WeightedZDD_Node(
			TNode*  const    init_hi     ,
			TNode*  const    init_lo     ,
			TItem   const &  init_label  ,
			TWeight const &  init_weight )
		: hi      (init_hi)
		, lo      (init_lo)
		, label   (init_label)
		, weight  (init_weight)
		// , memo    (0)
		// , visited (false)
		, checked (false)
	{}

	TNode& operator=( TNode const &  Nright )
	{
		hi     = Nright.hi;
		lo     = Nright.lo;
		label  = Nright.label;
		weight = Nright.weight;
		return *this;
	}

	~WeightedZDD_Node() {}

	bool operator==( TNode const &  Nright ) const
	{
		return (
			    hi     == Nright.hi
			and lo     == Nright.lo
			and label  == Nright.label
			and weight == Nright.weight );
	}

	bool IsNode1() const {  return ( hi == nullptr and lo == nullptr );  }

	void SetWeightSum()
	{
		weight =  ( hi == nullptr ? 0 : hi->weight );
		weight += ( lo == nullptr ? 0 : lo->weight );
	}

	friend std::ostream & operator<<(
		/*S*/ std::ostream       &  ost ,
		/*R*/ TNode        const &  N   )
	{
		ost << " [" << & N << "] ";  /* address of this node */
		ost << "( " << std::right << std::setw(10)  << N.hi     ;
		ost << ", " << std::right << std::setw(10)  << N.lo     ;
		ost << ", " << std::right << std::setw(10)  << N.label  ;
		ost << ", " << std::right << std::setw(10)  << N.weight ;
		// ost << ", " << std::right << std::setw(10)  << N.memo   ;
		// ost << ", " << std::right << std::boolalpha << N.visited;
		ost << ", " << std::right << std::boolalpha << N.checked;
		ost << " )";
		return ost;
	}
};



/* manage all ZDDs */
/*
  - name             name of this set
  - rootnode         pointer to the root node
  - height           height of ZDD
  - height_hi        height of ZDD (count hi-edge only)
  - nodenum          number of Nodes
  - nodenum_as_tree  number of Nodes as a tree
  - superset         parent ZDD index of this ZDD
  - top_level_set    parent ZDD index of this ZDD
  - setsize_uniq     set size (count unique)
*/
template <typename TItem, typename TWeight>
class WeightedZDD_Root
{
	using TNode = WeightedZDD_Node<TItem,TWeight>;

 protected:
	std::string  name__ ;
	TNode*       rootnode__;

 /* information of ZDD */
	Uint         height__   ;
	Uint         height_hi__;
	Uint         nodenum__ ;
	Uint         nodenum_as_tree__ ;

 /* information of set */
	Uint         superset__ ;
	Uint         top_level_set__;
	Uint         setsize_uniq__;


	void GetInfo__(
		/*R*/ TNode* const  N        ,
		/*R*/ Uint   const  depth    ,
		/*R*/ Uint   const  depth_hi )
	{
		if ( N == nullptr ) return;
		++nodenum_as_tree__;

		if ( not N->checked ) {
			++nodenum__;
			N->checked = true;
		}
		if ( N->IsNode1() ) {
			height__    = std::max( height__,    depth    );
			height_hi__ = std::max( height_hi__, depth_hi );
			++setsize_uniq__;
			return;
		}
		/* N is an internal node */
		GetInfo__( N->hi, depth + 1, depth_hi + 1 );
		GetInfo__( N->lo, depth + 1, depth_hi     );
		return;
	}

 public:
	WeightedZDD_Root(
		/*R*/ std::string const &  init_name          ,
		/*R*/ TNode*      const    init_rootnode      ,
		/*R*/ Uint        const    init_superset      ,
		/*R*/ Uint        const    init_top_level_set )
		: name__            (init_name)
		, rootnode__        (init_rootnode)
		, height__          (0)
		, height_hi__       (0)
		, nodenum__         (0)
		, nodenum_as_tree__ (0)
		, superset__        (init_superset)
		, top_level_set__   (init_top_level_set)
		, setsize_uniq__    (0)
	{
	 /* height, height_hi, nodenum, nodenum_as_tree, MFE, setsize_uniq */
		ResetNodeFlags( rootnode__ );
		GetInfo__( rootnode__, 0, 0 );
	}

	~WeightedZDD_Root() {}

 /* getter */
	auto rootnode        () const  -> decltype(rootnode__        )
	{ return rootnode__         ; }

	auto height          () const  -> decltype(height__          )
	{ return height__           ; }

	auto height_hi       () const  -> decltype(height_hi__       )
	{ return height_hi__        ; }

	auto nodenum         () const  -> decltype(nodenum__         )
	{ return nodenum__          ; }

	auto nodenum_as_tree () const  -> decltype(nodenum_as_tree__ )
	{ return nodenum_as_tree__  ; }

	auto name            () const  -> decltype(name__            )
	{ return name__             ; }

	auto superset        () const  -> decltype(superset__        )
	{ return superset__         ; }

	auto top_level_set   () const  -> decltype(top_level_set__   )
	{ return top_level_set__    ; }

	auto weight_sum      () const  -> TWeight
	{ return (rootnode__ == nullptr ? 0 : rootnode__->weight) ; }

	auto setsize_uniq      () const  -> Uint
	{ return setsize_uniq__; }


 /* methods */
	bool ExistCombination( std::vector<TItem> &  Combination ) const;

	void PrintNodes(
		/*R*/ bool         const    omit_zero = false ,
		/*S*/ std::ostream       &  ost = std::cout   ) const;

	void PrintCombinations( std::ostream &  ost = std::cout ) const;

	/* used in PrintCombinations() */
	virtual void ConvertAndPrint(
		/*S*/ std::ostream             &  ost         ,
		/*R*/ std::vector<TItem> const &  Combination ,
		/*R*/ TWeight            const &  Weight      ) const
	{
		/* default */
		ost << std::left  << std::setw( height_hi() + 5 ) << Combination;
		ost << std::right << std::setw( weight_sum()    ) << Weight;
	}

	/* used in PrintCombinations() */
	virtual void PrintHeader( std::ostream &  ost ) const
	{
		ost
		 << std::left << std::setw( height_hi() + 5 ) << ""
		 << "weight" << std::endl;
	}

};



template <typename TItem, typename TWeight>
bool WeightedZDD_Root<TItem,TWeight>::ExistCombination(
	/*W*/ std::vector<TItem> & Combination ) const
{
	using TNode = WeightedZDD_Node<TItem,TWeight>;
	using namespace std;

	function< bool(
		/*R*/ TNode*             const    N           ,
		/*W*/ std::vector<TItem>       &  Combination ) >
	search = [&] (
		/*R*/ TNode*             const    N           ,
		/*W*/ std::vector<TItem>       &  Combination )
	{
		if ( N == nullptr ) return false;

		if ( N->IsNode1() ) {
			return Combination.empty();
		}
		if ( Combination.empty() or N->label != Combination.back() ) {
			return search( N->lo, Combination );
		} else {
			Combination.pop_back();
			return search( N->hi, Combination );
		}
	};

	reverse( Combination.begin(), Combination.end() );
	  /* to pop front by pop_back() */

	return search( rootnode__, Combination );
}



/* omit_zero : do not print hi-edge-only node */
template <typename TItem, typename TWeight>
void WeightedZDD_Root<TItem,TWeight>::PrintNodes(
	/*R*/ bool         const    omit_zero ,
	/*S*/ std::ostream       &  ost       ) const
{
	using TNode = WeightedZDD_Node<TItem,TWeight>;
	using namespace std;

	vector<TNode*> ChangedNodeList;

	function< void(
		/*R*/ TNode*  const    N         ,
		/*R*/ Uint    const    depth     ,
		/*R*/ bool    const    omit_zero ,
		/*S*/ ostream       &  ost       ) >
	subroutine = [&] (
		/*R*/ TNode*  const    N         ,
		/*R*/ Uint    const    depth     ,
		/*R*/ bool    const    omit_zero ,
		/*S*/ ostream       &  ost       )
	{
		if ( N == nullptr ) return;

		if ( not N->checked ) {
			N->checked = true;
			ChangedNodeList.push_back(N);
			if ( not ( omit_zero and N->lo == nullptr ) ) {
				ost << *N;
				ost << "   " << setfill('-') << setw(depth + 1) << 'o' << endl;
				ost.fill(' ');
			}
		}
		if ( not N->IsNode1() ) {  /* internal node */
			subroutine( N->hi, depth + 1, omit_zero, ost );
			subroutine( N->lo, depth    , omit_zero, ost );
		}
		return;
	};

	ResetNodeFlags( rootnode__ );
	ost << endl;

	{  /* header */
		ost
		 << " [address] "  /* address of this node */
		 << "( hi-edge"
		 << ", lo-edge"
		 << ", label"
		 << ", weight"
		 << ", checked"
		 << " )" << endl;
	}
	subroutine( rootnode__, 0, omit_zero, ost );
	ResetNodeFlags( ChangedNodeList );
	ost << endl;
}



template <typename TItem, typename TWeight>
void WeightedZDD_Root<TItem,TWeight>::PrintCombinations(
	/*S*/ std::ostream & ost ) const
{
	using namespace std;
	using TNode = WeightedZDD_Node<TItem,TWeight>;

	function< void(
		/*R*/ vector<TItem> const &  Combination ,
		/*R*/ TNode*        const    N           ,
		/*S*/ ostream             &  ost         ) >
	subroutine = [&] (
		/*R*/ vector<TItem> const &  Combination ,
		/*R*/ TNode*        const    N           ,
		/*S*/ ostream             &  ost         )
	{
		if ( N == nullptr ) return;

		if ( N->IsNode1() ) {
			ConvertAndPrint( ost, Combination, N->weight );
			ost << endl;
			return;
		}

	 /* N is an internal node */
		/* hi */
		vector<TItem> CombinationNew = Combination;
		CombinationNew.push_back( N->label );
		subroutine( CombinationNew, N->hi, ost );

		/* lo */
		subroutine( Combination,    N->lo, ost );

		return;
	};

	const vector<TItem> Combination;

	if ( nodenum__ > 1000 ) {
		cout << "This ZDD might be large. Print all?(y/n)" << endl;

		string yn;
		while ( ReadlineString( yn, "> " ) ) {
			if ( yn == "" ) continue;
			if ( yn[0] == 'n' or yn[0] == 'N' )  return;
			if ( yn[0] == 'y' or yn[0] == 'Y' )  break;
		}
	}

	ost << endl;
	PrintHeader( ost );
	subroutine( Combination, rootnode__, ost );
	ost << endl;

	return;
}




/**********************************************************
 template functions
 **********************************************************/

/***** ZDD universal functions *****/

/* hash function for UniqueTable */
template <typename TNode>
class HashFuncN2I {  /* WeightedZDD_Node to size_t */
 public:
	size_t operator()( TNode const &  N ) const
	{
		size_t t = 1;
		t *= ( PRIME_NUMBER1 + N.label );  /* may be 0 */
		t *= ( PRIME_NUMBER1 + reinterpret_cast<size_t>(N.hi) );  /* may be nullptr */
		t *= ( PRIME_NUMBER1 + reinterpret_cast<size_t>(N.lo) );  /* may be nullptr */
		t %= PRIME_NUMBER2;
		return t;
	}
};



/* find ZDD node N in UniqueTable
 * if not found, make N in UTable */
template <typename TNode>
inline TNode* GetNode(
	/*W*/ WeightedZDD_UTable<TNode>       &  UTable ,
	/*R*/ TNode                      const &  N      )
{ /* iterator --> object --> const pointer --> pointer */
	return const_cast<TNode*>( &(*( UTable.insert(N).first )) );
}



/* reset node flags */
/* mode = 000 ~ 111 */

template <typename TNode>
inline void ResetNodeFlags(
	/*W*/ WeightedZDD_UTable<TNode> &  UTable )
	// /*R*/ std::bitset<3>             const &  mode = 0b111 )
{
	for ( auto & e : UTable ) ( const_cast<TNode*>( &e ) )->checked = false;
	// {
		// auto N = const_cast<TNode*>( &e );
		// if ( mode[2] ) {  N->memo       = 0;      }  /* if 1** */
		// if ( mode[1] ) {  N->visited    = false;  }  /* if *1* */
		// if ( mode[0] ) {  N->checked    = false;  }  /* if **1 */
	// }
}

template <typename TNode>
inline void ResetNodeFlags(
	/*R*/ std::vector<TNode*> const &  ChangedNodeList )
	// /*R*/ std::bitset<3>      const &  mode = 0b111    )
{
	for ( auto & e : ChangedNodeList )  e->checked = false;
	// {
		// if ( mode[2] ) {  e->memo       = 0;      }  /* if 1** */
		// if ( mode[1] ) {  e->visited    = false;  }  /* if *1* */
		// if ( mode[0] ) {  e->checked    = false;  }  /* if **1 */
	// }
}

template <typename TNode>
void ResetNodeFlags(
	/*R*/ TNode* const  N )
	// /*R*/ std::bitset<3> const &  mode = 0b111 )
{
	if ( N == nullptr ) return;
	N->checked = false;
	// if ( mode[2] ) {  N->memo       = 0;      }  /* if 1** */
	// if ( mode[1] ) {  N->visited    = false;  }  /* if *1* */
	// if ( mode[0] ) {  N->checked    = false;  }  /* if **1 */
	if ( N->IsNode1() ) return;
	ResetNodeFlags( N->hi );
	ResetNodeFlags( N->lo );
	// ResetNodeFlags( N->hi, mode );
	// ResetNodeFlags( N->lo, mode );
	return;
}




/***** system *****/

template <typename TNode>
void PrintUniqueTableStatus( WeightedZDD_UTable<TNode> const &  UTable )
{
	using namespace std;
	cout << endl;
	cout << endl;
	cout << "\tbucket_count     : " << UTable.bucket_count()     << endl;
	cout << "\tmax_bucket_count : " << UTable.max_bucket_count() << endl;
	cout << "\tload_factor      : " << UTable.load_factor()      << endl;
	cout << "\tmax_load_factor  : " << UTable.max_load_factor()  << endl;
	cout << endl;
	cout << endl;
}



template <typename TNode>
void PrintMemoryUsage( WeightedZDD_UTable<TNode> const &  UTable )
{
	const auto A = sizeof( *UTable.begin() )  +  sizeof( &*UTable.begin() );
	const auto B = UTable.size();
	const auto C = sizeof( &*UTable.begin() );
	const auto D = UTable.bucket_count();
	// const auto a = sizeof( TNode* );
	// const auto b = sizeof( Node0<TNode>()->weight    );
	// const auto c = sizeof( Node0<TNode>()->label   );
	// const auto d = sizeof( Node0<TNode>()->checked );

	std::cout
	 << std::endl
	 << "[UTable size]" << std::endl
	 << "  A : bytes/node   = " << A << "[bytes]" << std::endl
	// << "   (hi: " << a << ", lo: " << a << ", weight: " << b << ", label: " << c << ", checked: " << d << ")\n"
	 << "  B : node count   = " << B << std::endl
	 << "  C : bytes/node*  = " << C << "[bytes]" << std::endl
	 << "  D : bucket count = " << D << std::endl
	 << "  A * B + C * D    = " << A * B + C * D << "[bytes]" << std::endl
	 << std::endl;
}



/* BuildTopK */
template <typename TItem, typename TWeight>
void GetThreshold(
	/*W*/ TWeight                                  &  threshold ,
	/*W*/ std::vector<TWeight>                     &  topKvec   ,
	/*R*/ Uint                               const    K         ,
	/*R*/ WeightedZDD_Node<TItem, TWeight>* const    N         )
{
	if ( N == nullptr ) return;

	if ( N->IsNode1() ) {
		Vector::UpdateTopK( topKvec, K, N->weight, true, false );
		/* update threshold */
		if ( topKvec.size() >= K ) {  threshold = topKvec.back();  }
		return;
	}

 /* N is an internal node */
	if ( N->weight >= threshold ) {
		GetThreshold( threshold, topKvec, K, N->hi );
		GetThreshold( threshold, topKvec, K, N->lo );
	}
	return;
}



template <typename TItem, typename TWeight>
WeightedZDD_Node<TItem,TWeight>* BuildGreaterThanThreshold(
	/*W*/ WeightedZDD_UTable< WeightedZDD_Node<TItem,TWeight> > &  UTable ,
	/*R*/ WeightedZDD_Node<TItem,TWeight>* const  N         ,
	/*R*/ TWeight                           const  threshold )
{
	if ( N == nullptr ) return nullptr;
	if ( N->weight < threshold ) return nullptr;
	if ( N->IsNode1() ) return N;

 /* N is an internal node */
	WeightedZDD_Node<TItem,TWeight> Nnew;
	Nnew.hi = BuildGreaterThanThreshold( UTable, N->hi, threshold );
	Nnew.lo = BuildGreaterThanThreshold( UTable, N->lo, threshold );
	if ( Nnew.hi == nullptr ) {  return Nnew.lo;  }
	Nnew.label = N->label;
	Nnew.SetWeightSum();
	return GetNode( UTable, Nnew );
};




/* set operations */

/* 
 * (note)
 * if set1 and set2 have the same element x with weight n, m
 * the resulting set has x with weight max(n,m)
 */
template <typename TNode>
TNode* BuildUnion(
	/*W*/ WeightedZDD_UTable<TNode>       &  UTable ,
	/*R*/ TNode*                     const    N1     ,
	/*R*/ TNode*                     const    N2     )
{
	if ( N1 == nullptr ) return N2;
	if ( N2 == nullptr ) return N1;
	if ( N1 == N2 ) return N1;
	if ( N1->IsNode1() and N2->IsNode1() ) {
		if ( N1->weight >= N2->weight ) return N1;
		else                            return N2;
	}

 /* otherwize */

	TNode Nnew;

	if ( N1->label == N2->label ) {
		Nnew.hi    = BuildUnion( UTable, N1->hi, N2->hi );
		Nnew.lo    = BuildUnion( UTable, N1->lo, N2->lo );
		Nnew.label = N1->label;
	} else if ( N1->label < N2->label ) {
		Nnew.hi    = N1->hi;
		Nnew.lo    = BuildUnion( UTable, N1->lo, N2 );
		Nnew.label = N1->label;
	} else {
		Nnew.hi    = N2->hi;
		Nnew.lo    = BuildUnion( UTable, N1, N2->lo );
		Nnew.label = N2->label;
	}
	Nnew.SetWeightSum();

	return GetNode( UTable, Nnew );
}


/* 
 * (note)
 * if set1 and set2 have the same element x with weight n, m
 * the resulting set has x with weight min(n,m)
 */
template <typename TNode>
TNode* BuildIntersection(
	/*W*/ WeightedZDD_UTable<TNode>       &  UTable ,
	/*R*/ TNode*                     const    N1     ,
	/*R*/ TNode*                     const    N2     )
{
	if ( N1 == nullptr or N2 == nullptr ) return nullptr;
	if ( N1 == N2 ) return N1;
	if ( N1->IsNode1() and N2->IsNode1() ) {
		if ( N1->weight < N2->weight ) return N1;
		else                           return N2;
	}
	/* otherwize */
	TNode Nnew;

	if ( N1->label == N2->label ) {
		Nnew.hi    = BuildIntersection( UTable, N1->hi, N2->hi );
		Nnew.lo    = BuildIntersection( UTable, N1->lo, N2->lo );
		Nnew.label = N1->label;
		if ( Nnew.hi == nullptr ) {
			return Nnew.lo;
		}
		Nnew.SetWeightSum();
		return GetNode( UTable, Nnew );
	} else if ( N1->label < N2->label ) {
		return BuildIntersection( UTable, N1->lo, N2 );
	} else {
		return BuildIntersection( UTable, N1,     N2->lo );
	}
}



/* union of 3 or more sets
 * 
 * (note)
 * if sets have the same element x with weight n1,n2,...,
 * the resulting set has x with weight max{ n1,n2,... }
 */
template <typename TNode>
TNode* BuildUnionMulti(
	/*W*/ WeightedZDD_UTable<TNode> &  UTable    ,
	/*R*/ std::vector<TNode*>        &  rootnodes )
{
	if ( rootnodes.empty() ) return nullptr;
	if ( Vector::AllEqual( rootnodes ) ) return rootnodes.front();

	std::sort( rootnodes.begin(), rootnodes.end(), 
		[]( TNode* const x, TNode* const y ) {
			return x->label < y->label;
		} );

	/* all rootnodes are 1-node */
	if ( rootnodes.front()->IsNode1() ) {
		/* return max weight 1-node */
		return *std::max_element( rootnodes.cbegin(), rootnodes.cend(),
			[]( TNode* x, TNode* y ) {
				return x->weight < y->weight;
			} );
	}

 /* otherwize */
	auto sep = rootnodes.cbegin();
	while ( sep != rootnodes.cend()
		    and (*sep)->label == rootnodes.front()->label )
		++sep;

	std::vector<TNode*> rootnodes_hi;
	std::vector<TNode*> rootnodes_lo( sep, rootnodes.cend() );
	rootnodes_hi.reserve( std::distance( rootnodes.cbegin(), sep ) );
	rootnodes_lo.reserve( rootnodes.size() );

	for ( auto it = rootnodes.cbegin(); it != sep; ++it ) {
		if ( (*it)->hi != nullptr ) rootnodes_hi.push_back( (*it)->hi );
		if ( (*it)->lo != nullptr ) rootnodes_lo.push_back( (*it)->lo );
	}

	TNode Nnew;
	Nnew.label = rootnodes.front()->label;
	Nnew.hi = BuildUnionMulti( UTable, rootnodes_hi );
	Nnew.lo = BuildUnionMulti( UTable, rootnodes_lo );
	Nnew.SetWeightSum();

	return GetNode( UTable, Nnew );
}




template <typename TNode, typename TItem>
TNode* BuildContainItem(
	/*W*/ WeightedZDD_UTable< TNode >       &  UTable ,
	/*R*/ TNode*                       const    N      ,
	/*R*/ TItem                        const &  item   )
{
	if ( N == nullptr ) return nullptr;

	if ( N->label > item or N->IsNode1() ) return nullptr;

 /* N is an internal node */
	TNode Nnew(N);
	if ( N->label == item ) {
		Nnew.lo = nullptr;
		Nnew.weight = Nnew.hi->weight;
		return GetNode<TNode>( UTable, Nnew );
	}

 /* case : N->label < item  -->  item remains to be checked */

 /* go to hi and lo */
	Nnew.hi = BuildContainItem( UTable, N->hi, item );
	Nnew.lo = BuildContainItem( UTable, N->lo, item );
	if ( Nnew.hi == nullptr )  return Nnew.lo;
	Nnew.SetWeightSum();

	return GetNode<TNode>( UTable, Nnew );
}
