
# ZDDのc++での実装
* ノード
	* ZDDのノードはUniqueTable上に直接構築する方法をとったのでhi, loはポインタとなる．
	* 末端ノードの指す先をNULLにするため，hi, loは書き換えは起きないが参照ではなくポインタとした．
	* UniqueTableは `std::unordered_set<NodeType>` により実装した．ポインタはconstで与えられるが，新しいノードNnewを作るときに一部を書き換えるのを有効にしたほうが簡潔な場合があるため，`const_cast`でconstを外して用いている．
	* 0-nodeは重み無し／有りにかかわらず1つでよい．
	* 0-nodeにはnullptrを用いる場合（採用）
		* 定数の定義が不要になる
		* N == nullptrでは0-nodeのことであるとわかりにくい
		* N->lo->weightのように多段にメンバ変数を呼び出すときにチェックが必要
			* SetWeightSum()により隠蔽し回避．
		* 0-nodeはnullptrと定義する．
		* 1-nodeは hi = lo = nullptr であるノードと定義する．
	* 0-nodeにnullptrを用いずにNODE0という定数を使う場合（不採用）
		* N->lo->weightのように多段にメンバ変数を呼び出せる
		* 任意のノードに対してIsTerminal()などのメソッドを用いて分かりやすく書ける
		* nullptrはc++11の言語機能で，マクロ定義のNULLと違い型情報を持つという違いがある．
		* 0-nodeは hi = lo = nullptr であるノードと定義する．
		* 1-nodeは hi = lo = 0-node であるノードと定義する．
* Rootは手動で作るので合計100個は超えないと想定し，メモリ消費はあまり考えないものになっている．



# クラスの継承関係
* 基底クラスから重み付きにしている．

```
WeightedZDD_Node<ItemType, WeightType>
	|
	+ SeqBDD_Node     : WeightedZDD_Node<char, double>
	|
	+ RSSDD_Node : WeightedZDD_Node<TBasePair, double>

WeightedZDD_Root<ItemType, WeightType>
	|
	+ SeqBDD_Root     : WeightedZDD_Root<char, double>
	|
	+ RSSDD_Root : WeightedZDD_Root<TBasePair, double>
```

* 重み無しも必要ならばメンバ変数weightを含まない基底クラスを作って継承すればよい
	* Todo: Node, Rootについて，ZDD基底クラスを作り，WeightedZDD_Nodeでもcheckedはまだ持たせないようにしたい．

```
ZDD_Node<ItemType>
	|
	+ WeightedZDD_Node<ItemType, WeightType> : ZDD_Node<ItemType>
		|
		+ SeqBDD_Node     : WeightedZDD_Node<char, double>
		|
		+ RSSDD_Node : WeightedZDD_Node<TBasePair, double>

ZDD_Root<ItemType>
	|
	+ WeightedZDD_Root<ItemType, WeightType> : ZDD_Root<ItemType>
		|
		+ SeqBDD_Root     : WeightedZDD_Root<char, double>
		|
		+ RSSDD_Root : WeightedZDD_Root<TBasePair, double>
```

* メモ
	* WeightTypeは最小値(`limit_min()`)，最大値(`limit_max()`)が定義されている必要がある．
		* RSSDDではdoubleを用いている．有理数の使用も検討中．
	* ItemTypeにはEND_SYMBOLが定義されている必要がある．（BuildFromFileの各行の行末記号）
	* SeqBDDの重みは仮にdoubleとしている．



## ZDD
* 一般的な組合せ集合を表すZDDの基底クラス．
* WeightedZDD_Node
	* ZDDのノードを表すテンプレートクラス．ラベルの型を変えて用いる．
	* メンバ変数
		* hi, lo : ZDD_Node型ポインタ．
		* label : LabelType型．SeqBDDでは`char`，RSSDDでは`TBasePair`．
		* checked : 一度訪れたことを表すチェックフラグ．`PrintNodes`やRSSDDの`LCM`で用いている．各ノードはUniqueTable上に実現されるようにしたため，ノードの番地によりランダムアクセスできるchecked flag配列を作りにくくなったので，ノードに直接持たせるようにした．labelの型にもよるが，今回実装したSeqBDDやRSSDDではpaddingに収まるため1ノードあたりのサイズは増えていない．等価性判定(`operator=`)では無視する．
* ZDD_Root
	* ZDDの根ノードとZDDの様々な情報を保持する．
	* メンバ変数
		* name          : このZDDの名前．
		* rootnode      : 根ノードのポインタ．
		* height        : ZDDの高さ．
		* height_hi     : hi枝を通る回数のみカウントした場合のZDDの高さ．
		* size          : ZDDに含まれるノード数．
		* treesize      : ZDDを二分木にした場合のノード数（共有されているノードが複数回数えられる）
		* parent_root   : このZDDの親のZDDの番号．部分集合を構築した場合などに親の番号を覚えておくのに用いる．すべてのZDDの祖先の番号は-1とする．
		* elemcount     : ZDDが表す組合せ集合の濃度．
	* メンバ関数
		* GetInfo__     : height, height_hi, size, treesize, elemcount, Itemsetを計算．コンストラクタで呼び出す．
		* Search        : ユーザーが指定した組み合わせを検索し，含まれるかどうかを表示．重み付きならばその重みも表示．
		* PrintNodes    : このZDDのノードを表示．
		* GenerateXYpic : XY-picのLaTeXコードをファイル出力．
		* PrintCombination
			* ConvertCombinationをvirtualで定義して派生クラスで書き換え



## RSSDD
* RSSDD_Node
* RSSDD_Root
	* メンバ変数
		* Itemset             : このZDDが表す組み合わせ集合に現れるアイテムの集合．
		* sequence            : RNA配列
		* PartFunc            : RNA配列から計算される分配関数
		* MFE                 : この集合に含まれる二次構造の最小自由エネルギー
		* prob_sum            : 存在確率の和
		* Pattern_disignated  : ユーザーが指定したパターン（部分集合構築でどう指定したかを記憶）
		* Pattern_result      : この集合の二次構造パターン
		* PairFreq            : この集合でのペアの頻度（そのペアを含む二次構造の数）
		* PairProb            : この集合でのペアの存在確率（そのペアを含む二次構造の存在確率の和）
	* メンバ関数
		* CalcPairFreqAndProb__ 
		* CalcPattern__
		* PrintItemset        : Itemsetを表示．


## WeightedZDD
* WeightedZDD_Node
	* ZDD_NodeにWeightType型のメンバ変数weightを追加したテンプレートクラス．
* WeightedZDD_Root





# 関数
* ノード集合（つまりUniqueTable）に対する操作はクラスにまとめてしまうことも考えられたが，単なるテンプレート関数とした．



## ZDDの共通の機能
* ZDDノード全体に対する操作
	* PrintUniqueTableStatus
	* PrintMemoryUsage
	* GetNode
	* ResetCheckedFlags
* 複数のZDDに対する操作
	* BuildUnion
	* Intersection
	* Setminus
	* PrintList
* ?
	* BuildFromFile -> 読み込み部分のみ変えて使うか否か．各行の組合せcの末尾整数（文字）の定義が必要に
	* MakeRoot
	* BuildTopK
	* Interactive -> 共通部分を共有するのは面倒なのでコピーでよいか



## RSSDDの機能
* Prob2FreeEnergy
* FreeEnergy2Prob
* CalcPartFunc
* IsDBAstring
* IsDBstring
* DotBracket2PairArray
* InitializePattern
* GetDerivedPattern
* PairArray2Pattern
* DBA2Pattern
* Pattern2DBA
* CalcPattern
* PrintPattern
* CalcPairFreqAndProb
* CountPairInPattern
	* Patternから
	* PairFreqから
	* PairProbとprob_sumから
* BuildSubsetForPatternFromPairArray
* BuildSubsetForPatternFromDBA
* BuildContainPair
* LCM
* PrintFrequentPatterns
* 


