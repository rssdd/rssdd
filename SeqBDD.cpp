
#include <cstdlib>
#include <unistd.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <malloc.h>


#include <iostream>
#include <fstream>

#include <bitset>
#include <string>
#include <vector>
#include <unordered_map>       /* command */
#include <unordered_set>

#include <initializer_list>
#include <iomanip>   /* print */
#include <sstream>   /* StoN, NtoS */
#include <algorithm> /* sort, min_element, unique */
#include <cmath>     /* log */
#include <cstdint>   /* uint_fast32_t, ... */

// #include <cassert>

#include "Basic.hpp"
#include "WeightedZDD.hpp"
#include "SeqBDD.hpp"

using namespace std;



/******************************************************************************
 definition of strings
 ******************************************************************************/

const string PROMPT_STRING      = "SeqBDD> ";
const string HELP_FILE_NAME     = "SeqBDD_help.txt";
const string HISTORY_FILE_NAME  = ".seqbdd_history";



/******************************************************************************
 functions
 ******************************************************************************/

inline SeqBDD_Node* GetNode( SeqBDD_UTable& UTable, const SeqBDD_Node& N )
{
	return GetNode<SeqBDD_Node>( UTable, N );
}


vector<TChar> Str2CharVect( const string& str )
{
	std::vector<TChar> char_vect( str.size(), ' ' );
	auto itv = char_vect.begin();
	auto its = str.cbegin();
	while ( its != str.cend() ) { *itv++ = *its++; }
	return char_vect;
}

string CharVect2Str( const vector<TChar>& char_vect )
{
	std::string str( char_vect.size(), ' ' );
	auto its = str.begin();
	auto itv = char_vect.cbegin();
	while ( itv != char_vect.cend() ) { *its++ = *itv++; }
	return str;
}



/******************************************************************************
 build SeqBDD from data
 ******************************************************************************/

int BuildFromFile(
		SeqBDD_UTable&        UTable,
		vector<SeqBDD_Root>&  SeqBDDs,
		const string&         IFileName )
{
	ifstream fin( IFileName );
	if ( !FileCheck( fin, IFileName ) ) return 1;

 /* local functions */
	auto ReadSequenceFromFile
		= [] (
				vector< pair<string, TWeight> >& SeqTable,
				ifstream& fin )
			-> int
		{
			string buf;

			while ( getline( fin, buf ) ) {
				if ( buf.length() == 0 )  continue;
				// if ( buf[0] == '>' ) continue; /* discard the line */
				stringstream ss( buf );
				string seq;
				TWeight weight = 0;
				ss >> seq;
				ss >> weight;
				if ( seq.length() > SEQUENCE_LENGTH_MAX ) {
					cout << "Sequences length >" << SEQUENCE_LENGTH_MAX << " is not supported" << endl;
					return 1;
				}

				SeqTable.emplace_back( seq, weight );
			}
			return 0;
		};


	auto ResizeTable
		= [] ( vector< pair<string, TWeight> >& SeqTable )
		{
			Uint maxlen = 0;
			Uint sum_of_len = 0;
			// for ( auto it = SeqTable.cbegin(); it != SeqTable.cend(); ++it ) {
			for ( const auto& e : SeqTable ) {
				maxlen = max( maxlen, e.first.size() );
				sum_of_len += e.first.size();
			}
			maxlen++;
			// for ( auto it = SeqTable.begin(); it != SeqTable.end(); ++it ) {
			for ( auto& e : SeqTable ) {
				e.first.resize( maxlen, ENDCHAR );
			}

		 /* print table size */
			Uint each_size   = sizeof( SeqTable.front().first.front() );
			Uint weight_size = sizeof( SeqTable.front().second );
			Uint pair_size   = sizeof( SeqTable.front() );
			Uint size_all    = sum_of_len * each_size + SeqTable.size() * weight_size + SeqTable.size() * pair_size;

			cout
			<< '\n'
			<< "[SeqTable size]\n"
			<< "  A : pair          : " << pair_size << "[bytes]\n"
			<< "  B : arrays        : " << sum_of_len * each_size << "[bytes]\n"
			<< "  C : weight        : " << weight_size << "[bytes]\n"
			<< "  D : #sec.str      : " << SeqTable.size() << '\n'
			<< "  A * D + B + C * D = " << size_all  << "[bytes]\n"
			<< endl;
		};

	std::function< SeqBDD_Node*(
				SeqBDD_UTable&,
				const vector< pair<string, TWeight> >&,
				const Uint,
				const Uint,
				const Uint,
				TProgress& )>
	BuildFromTable
		= [ &BuildFromTable ] (
				SeqBDD_UTable&                         UTable,
				const vector< pair<string, TWeight> >& SeqTable,
				const Uint                             row_begin,
				const Uint                             row_end,
				const Uint                             column,
				TProgress&                             prgrss )
			-> SeqBDD_Node*
		{ /* range = [row_begin,row_end) */
			if ( row_begin >= row_end )  return nullptr;
			prgrss.Print(row_begin);

			SeqBDD_Node Nnew;
			Nnew.label  = (SeqTable[row_begin].first)[column];
			// Nnew.weight = VoP_SumOfSecond( SeqTable, row_begin, row_end );

			/* separate [row_begin, row_end) to [row_begin, row_sep) and [row_sep, row_end) */
			Uint row_sep = row_begin;
			while ( row_sep < row_end
					&& (SeqTable[row_sep].first)[column] == Nnew.label )
			{ ++row_sep; }

			if ( Nnew.label == ENDCHAR ) { /* line end */
				Nnew.hi = nullptr;
				Nnew.lo = nullptr;
				Nnew.weight = VoP_SumOfSecond( SeqTable, row_begin, row_end );
			} else {
				Nnew.hi = BuildFromTable( UTable, SeqTable, row_begin, row_sep, column + 1, prgrss );
				Nnew.lo = BuildFromTable( UTable, SeqTable, row_sep,   row_end, column    , prgrss );
				Nnew.SetWeightSum();
			}
			return GetNode( UTable, Nnew );
		};


	TProgress time_sum;
	vector< pair<string, TWeight> > SeqTable;

	{ /* read from the file */
		time_sum.Start("reading   ");
		ReadSequenceFromFile( SeqTable, fin );
		time_sum.Stop(" done. ");
		time_sum.PrintTime();
		time_sum.AccumulateTime();
		cout << endl;
	}

	ResizeTable( SeqTable );

	{
	 /* sort the table */
		time_sum.Start("sorting   ");
		sort( SeqTable.begin(), SeqTable.end() );
		time_sum.Stop(" done. ");
		time_sum.PrintTime();
		time_sum.AccumulateTime();
		cout << endl;
	}

	{
		TProgress prgrss( SeqTable.size() );
		prgrss.Start("building     ");
		time_sum.Start();
		const auto rootnode = BuildFromTable(
				UTable,
				SeqTable,
				0,
				SeqTable.size(),
				0,
				prgrss );
		time_sum.Stop(" done. ");
		prgrss.Stop();
		time_sum.PrintTime();
		time_sum.AccumulateTime();
		cout << endl;

		time_sum.Start("counting nodes    ");
		SeqBDDs.emplace_back( IFileName, rootnode );
		prgrss.Stop(" done.");
		time_sum.AccumulateTime();
	}

	cout << endl;
	cout << " read + sort + build + countnode = ";
	time_sum.PrintAccumulatedTime();
	cout << '\n' << endl;

	PrintList( SeqBDDs );

	return 0;
}



/******************************************************************************
 build SeqBDD from SeqBDD
 ******************************************************************************/

int MakeRoot(
		vector<SeqBDD_Root>& SeqBDDs,
		SeqBDD_Node* const   rootnode,
		const Uint           parent_root_number,
		string&              newname )
{
	if ( !CheckRange( SeqBDDs, parent_root_number ) )  return 1;
	const SeqBDD_Root ParentRoot = SeqBDDs[ parent_root_number ];

	if ( newname == "" ) {
		newname = ParentRoot.name() + ":rootaddr(" + NtoS( rootnode ) + ')';
	}

	SeqBDDs.emplace_back( newname, rootnode );

	PrintList( SeqBDDs );
	return 0;
}



int SetOperation_main(
		SeqBDD_UTable&       UTable,
		vector<SeqBDD_Root>& SeqBDDs,
		const Uint           root_number_1,
		const Uint           root_number_2,
		const char           op )
{
	if ( !CheckRange( SeqBDDs, root_number_1 ) )  return 1;
	if ( !CheckRange( SeqBDDs, root_number_2 ) )  return 1;

	const SeqBDD_Root Z1 = SeqBDDs[root_number_1];
	const SeqBDD_Root Z2 = SeqBDDs[root_number_2];

	TProgress prgrss;
	prgrss.Start("building     ");

	SeqBDD_Node* rootnode;
	switch (op) {
		case '+' :
			rootnode = BuildUnion       ( UTable, Z1.rootnode(), Z2.rootnode() );
			break;
		case '*' :
			rootnode = BuildIntersection( UTable, Z1.rootnode(), Z2.rootnode() );
			break;
		default  :
			break;
	}
	prgrss.Stop(" done. ");
	prgrss.PrintTime();
	cout << endl;

	SeqBDDs.emplace_back(
			'(' + Z1.name() + ' ' + op + ' ' + Z2.name() + ')',
			rootnode );

	PrintList( SeqBDDs );

	return 0;
}



int BuildTopK_main(
		SeqBDD_UTable&       UTable,
		vector<SeqBDD_Root>& SeqBDDs,
		const Uint           root_number,
		const Uint           K )
{
	if ( !CheckRange( SeqBDDs, root_number ) ) return 1;

	const SeqBDD_Root Z = SeqBDDs[root_number];

	if ( K < 1 ) return 0;

	vector<TWeight> topKvec;
	TWeight threshold = TWEIGHT_MIN;

	GetThreshold<TChar, TWeight>( threshold, topKvec, K, Z.rootnode() );

	{
		TProgress prgrss;
		prgrss.Start("building     ");
		const auto rootnode = BuildGreaterThanThreshold( UTable, Z.rootnode(), threshold );
		prgrss.Stop(" done. ");
		prgrss.PrintTime();
		cout << endl;

		SeqBDDs.emplace_back( Z.name() + ":top" + NtoS( K ), rootnode );
	}

	PrintList( SeqBDDs );

	return 0;
}



int BuildGreaterThanThreshold_main(
		SeqBDD_UTable&       UTable,
		vector<SeqBDD_Root>& SeqBDDs,
		const Uint           root_number,
		const TWeight        threshold )
{
	if ( !CheckRange( SeqBDDs, root_number ) ) return 1;

	const SeqBDD_Root Z = SeqBDDs[root_number];

	{
		TProgress prgrss;
		prgrss.Start("building     ");
		const auto rootnode = BuildGreaterThanThreshold( UTable, Z.rootnode(), threshold );
		prgrss.Stop(" done. ");
		prgrss.PrintTime();
		cout << endl;

		SeqBDDs.emplace_back( Z.name() + ":weight>=" + NtoS( threshold ), rootnode );
	}

	PrintList( SeqBDDs );

	return 0;
}



/******************************************************************************
 print
 ******************************************************************************/

void PrintList( const vector<SeqBDD_Root>& SeqBDDs )
{
	Uint lengmax = 20;
	// for ( auto it = SeqBDDs.cbegin(); it != SeqBDDs.cend(); ++it ) {
	for ( const auto& e : SeqBDDs ) {
		lengmax = max<Uint>( lengmax, e.name().length() );
	}

	vector<size_t> width = {
			3, 3, lengmax, 13,
			0, 13, 6, 6, 10, 10 };

	vector<string> header_name = {
			"No.", "parent", "name", "#sequence",
			"", "address", "height", "height(hi)", "size", "tree size" };

	vector<bool> lr = {
			1,1,0,1,
			1,1,1,1,1,1 };

	cout << endl;
	PrintMarkdownTableHeader( width, header_name, lr, cout );

	for ( size_t i = 0; i < SeqBDDs.size(); ++i ) {
		cout <<  "| " << right << setw(width[0]) << i << flush;
		cout << " | " << right << setw(width[1]) << SeqBDDs[i].superset();
		cout << " | " << left  << setw(width[2]) << SeqBDDs[i].name();
		cout << " | " << right << setw(width[3]) << SeqBDDs[i].setsize();
		cout << " | " << right << setw(width[4]) << "";
		cout << " | " << right << setw(width[5]) << SeqBDDs[i].rootnode();
		cout << " | " << right << setw(width[6]) << SeqBDDs[i].height();
		cout << " | " << right << setw(width[7]) << SeqBDDs[i].height_hi();
		cout << " | " << right << setw(width[8]) << SeqBDDs[i].size();
		cout << " | " << right << setw(width[9]) << SeqBDDs[i].treesize();
		cout << " |"  << endl;
	}

	cout << endl;
}



/******************************************************************************
 interface
 ******************************************************************************/

int Interactive( vector<SeqBDD_Root>& SeqBDDs, SeqBDD_UTable& UTable )
{
	string this_dir;  /* help file and history file directory */
	{
		constexpr int PATH_MAX_LEN = 10000;
		char path[PATH_MAX_LEN];
		auto p = getcwd( path, sizeof(path) );
		if ( !p ) return 1;
		this_dir = path;
		this_dir.push_back('/');
	}

	read_history( (this_dir + HISTORY_FILE_NAME).c_str() );


 /* map : commands -> integer */
	enum class ECommand : int {
		ls, pwd, less,
		chdir,
		quit,
		help,
		build,
		makeroot,
		search,
		list,
		print,
		generate_xypic_code,
		uniqtable_status,
		memory_usage,
		malloc_stats
	};
	unordered_map<string,ECommand> command_id;
	{
		command_id["ls"]                  = ECommand::ls;
		command_id["pwd"]                 = ECommand::pwd;
		command_id["less"]                = ECommand::less;
		command_id["cd"]                  = ECommand::chdir;
		command_id["chdir"]               = ECommand::chdir;
		command_id["q"]                   = ECommand::quit;
		command_id["quit"]                = ECommand::quit;
		command_id["exit"]                = ECommand::quit;
		command_id["h"]                   = ECommand::help;
		command_id["help"]                = ECommand::help;
		command_id["uniq"]                = ECommand::uniqtable_status;
		command_id["uniq-status"]         = ECommand::uniqtable_status;
		command_id["unique-status"]       = ECommand::uniqtable_status;
		command_id["unique-table-status"] = ECommand::uniqtable_status;
		command_id["mem"]                 = ECommand::memory_usage;
		command_id["memory"]              = ECommand::memory_usage;
		command_id["memory-usage"]        = ECommand::memory_usage;
		command_id["malloc-stats"]        = ECommand::malloc_stats;
		command_id["b"]                   = ECommand::build;
		command_id["build"]               = ECommand::build;
		command_id["mkrt"]                = ECommand::makeroot;
		command_id["makeroot"]            = ECommand::makeroot;
		command_id["search"]              = ECommand::search;
		command_id["search-string"]       = ECommand::search;
		command_id["list"]                = ECommand::list;
		command_id["p"]                   = ECommand::print;
		command_id["print"]               = ECommand::print;
		command_id["xypic"]               = ECommand::generate_xypic_code;
		command_id["genxypic"]            = ECommand::generate_xypic_code;
		command_id["generatexypic"]       = ECommand::generate_xypic_code;
		command_id["generate-xypic"]      = ECommand::generate_xypic_code;
	}

	enum class ECommand_build : int {
		set_union,
		set_intersection,
		// set_difference,    /* unimplemented */
		subset
	};
	unordered_map<string,ECommand_build> command_build_id;
	{
		command_build_id["union"]        = ECommand_build::set_union;
		command_build_id["intersection"] = ECommand_build::set_intersection;
		// command_build_id["setminus"]     = ECommand_build::set_difference;
		// command_build_id["difference"]   = ECommand_build::set_difference;
		command_build_id["subset"]       = ECommand_build::subset;
	}

	enum class ECommand_buildsubset : int {
		top,
		weight_threshold,
	};
	unordered_map<string,ECommand_buildsubset> command_buildsubset_id;
	{
		command_buildsubset_id["top"]       = ECommand_buildsubset::top;
		command_buildsubset_id["w"]         = ECommand_buildsubset::weight_threshold;
		command_buildsubset_id["weight"]    = ECommand_buildsubset::weight_threshold;
		command_buildsubset_id["threshold"] = ECommand_buildsubset::weight_threshold;
	}

	enum class ECommand_print : int {
		list,
		node,
		allnodes,
		sequence,
		alphabet,
	};
	unordered_map<string,ECommand_print> command_print_id;
	{
		command_print_id["list"]               = ECommand_print::list;
		command_print_id["node"]               = ECommand_print::node;
		command_print_id["nodes"]              = ECommand_print::node;
		command_print_id["allnodes"]           = ECommand_print::allnodes;
		command_print_id["seq"]                = ECommand_print::sequence;
		command_print_id["seqs"]               = ECommand_print::sequence;
		command_print_id["sequence"]           = ECommand_print::sequence;
		command_print_id["sequences"]          = ECommand_print::sequence;
		command_print_id["str"]                = ECommand_print::sequence;
		command_print_id["strs"]               = ECommand_print::sequence;
		command_print_id["string"]             = ECommand_print::sequence;
		command_print_id["strings"]            = ECommand_print::sequence;
		command_print_id["alph"]               = ECommand_print::alphabet;
		command_print_id["alphabet"]           = ECommand_print::alphabet;
	}


/* command line loop */
 string CmdLine;
 while ( ReadlineString( CmdLine, PROMPT_STRING ) ) {

  if ( CmdLine == "" ) continue;

  bool CommandNotFound = true;
  bool add2history = true;
  const string CmdLine_history = CmdLine;  /* save to history */
  if ( CmdLine[0] == ' ' ) {  add2history = false;  }


  vector<string> cmd( 1000, "" );
  { /* split CmdLine to cmd[] */
      stringstream ss( CmdLine );
      string buf;
      int i = 0;
      while ( ss >> buf ) { cmd[i++] = buf; }
  }


  const bool FromFile = StringFound( CmdLine, " < " );
  const bool ToFile   = StringFound( CmdLine, " > " );
  string FileName;
  {
      if ( FromFile || ToFile ) {
         /* extract file name */
          size_t i = 0;
          while ( i < cmd.size() - 1 ) {
              if ( cmd[i] == "<" || cmd[i] == ">" ) break;
              ++i;
          }
          if ( i == cmd.size() || cmd[i + 1] == "" ) {
              cout << "ERROR: missing filename" << endl;
              CommandNotFound = false;  break;
          }
          FileName = cmd[i + 1];
          cmd.erase( cmd.begin() + i, cmd.end() );
      }
      if ( FromFile ) {
         /* erase after " < " */
          CmdLine.erase( CmdLine.begin() + CmdLine.find(" < "), CmdLine.end() );
      }
      if ( ToFile ) {
         /* erase after " > " */
          CmdLine.erase( CmdLine.begin() + CmdLine.find(" > "), CmdLine.end() );
      }
  }

  switch ( command_id[cmd[0]] ) {
	case ECommand::quit :
	{
		write_history( (this_dir + HISTORY_FILE_NAME).c_str() );
		return 0;
	}

	case ECommand::ls :
	case ECommand::pwd :
	case ECommand::less :
	{
		if ( system( CmdLine.c_str() ) == -1 ) {
			cout << "ERROR: system()" << endl;
		}
		add2history = false;
		CommandNotFound = false;  break;
	}

	case ECommand::chdir :
	{
		if ( cmd[1] == "" )  break;
		if ( chdir( cmd[1].c_str() ) == -1 ) {
			cout << "ERROR: system()" << endl;
		}
		add2history = false;
		CommandNotFound = false;  break;
	}


	case ECommand::help :
	{
		if ( system( ("less " + this_dir + HELP_FILE_NAME).c_str() ) == -1 ) {
			cout << "ERROR: system()" << endl;
		}
		add2history = false;
		CommandNotFound = false;  break;
	}

	case ECommand::uniqtable_status :
	{
		PrintUniqueTableStatus( UTable );
		CommandNotFound = false;  break;
	}
	case ECommand::memory_usage :
	{
		PrintMemoryUsage( UTable );
		CommandNotFound = false;  break;
	}
	case ECommand::malloc_stats :
	{
		malloc_stats();
		CommandNotFound = false;  break;
	}

	case ECommand::build :
	{
		if (FromFile) {
			BuildFromFile( UTable, SeqBDDs, FileName );
			CommandNotFound = false;  break;
		}

		if ( cmd[1] == "" )  break;
		switch ( command_build_id[cmd[1]] ) {
			case ECommand_build::set_union :
			{
				SetOperation_main( UTable, SeqBDDs, StoN<Uint>(cmd[2]), StoN<Uint>(cmd[3]), '+' );
				CommandNotFound = false;  break;
			}
			case ECommand_build::set_intersection :
			{
				SetOperation_main( UTable, SeqBDDs, StoN<Uint>(cmd[2]), StoN<Uint>(cmd[3]), '*' );
				CommandNotFound = false;  break;
			}
			case ECommand_build::subset :
			{
				if (cmd[2] == "") break;
				const Uint root_number = StoN<Uint>( cmd[2] );

				switch ( command_buildsubset_id[cmd[3]] ) {
					case ECommand_buildsubset::top :
					{
						const Uint K = GetOptionValue<Uint>( cmd, cmd[3] );
						if ( K < 0 ) {
							cout << "ERROR: wrong value for K" << endl;
							CommandNotFound = false;  break;
						}
						BuildTopK_main( UTable, SeqBDDs, root_number, K );
						CommandNotFound = false;  break;
					}
					case ECommand_buildsubset::weight_threshold :
					{
						const TWeight threshold = GetOptionValue<Uint>( cmd, cmd[3] );
						if ( threshold < 0 ) {
							cout << "ERROR: wrong value for threshold" << endl;
							CommandNotFound = false;  break;
						}
						BuildGreaterThanThreshold_main( UTable, SeqBDDs, root_number, threshold );
						CommandNotFound = false;  break;
					}
					default:
						break;
				}
				break;  /* ECommand_build::subset */
			}
			default:
				break;
		}
		break;  /* case ECommand::build */
	}

	case ECommand::makeroot:
	{  /* makeroot   NODE_ADDRESS   SUPERSET_ROOT_NUMBER   --name NEWNAME */
		if ( cmd[1] == "" || cmd[2] == "" )  break;

		size_t addr;
		{
			stringstream ss;
			ss << hex << cmd[1];
			ss >> addr;
		}
		const auto rootnode = reinterpret_cast<SeqBDD_Node*>(addr);

		const Uint parent_root_number = StoN<Uint>( cmd[2] );

		string newname = "";
		if ( StringFound( CmdLine, "--name" ) ) {
			newname = cmd[ 1 + SearchPos( cmd, string("--name") ) ];
		}
		MakeRoot( SeqBDDs, rootnode, parent_root_number, newname );
		CommandNotFound = false;  break;
	}

	case ECommand::search :
	{
		if (cmd[1] == "") break;
		const Uint root_number = StoN<Uint>( cmd[1] );
		if ( ! CheckRange( SeqBDDs, root_number ) )  break;
		const auto Z = SeqBDDs[root_number];

		string query_str;
		{
			Z.PrintHeader( cout );
			ReadlineString( query_str, "input string > " );
			Add2History( query_str );
		}

		const bool found = Z.ExistString( query_str );
		cout << endl;
		cout << ( found ? "found" : "not found" ) << endl;
		cout << endl;
		CommandNotFound = false;  break;
	}

	case ECommand::list :
	{
		PrintList( SeqBDDs );
		CommandNotFound = false;  break;
	}
	case ECommand::print :
	{
		ofstream fout( FileName.c_str(), ios::out );
		if ( ToFile && ! FileCheck( fout, FileName ) )  break;
		auto& ost = ( ToFile ? fout : cout );

		if ( cmd[1] == "" )  break;

		switch ( command_print_id[cmd[1]] ) {
			case ECommand_print::allnodes :
			{
				ost << '\n';
				for ( auto it = UTable.cbegin(); it != UTable.cend(); ++it ) {
					ost << *it << endl;
				}
				ost << endl;
				CommandNotFound = false;  break;
			}

			case ECommand_print::node :
			{
				if (cmd[2] == "") break;
				const Uint root_number = StoN<Uint>( cmd[2] );
				if ( ! CheckRange( SeqBDDs, root_number ) )  break;
				const bool omit_zero = StringFound( CmdLine, "--omit-zero" );
				SeqBDDs[root_number].PrintNodes( omit_zero, ost );
				CommandNotFound = false;  break;
			}

			case ECommand_print::sequence :
			{
				if (cmd[2] == "") break;
				const Uint root_number = StoN<Uint>( cmd[2] );
				if ( !CheckRange( SeqBDDs, root_number ) )  break;
				SeqBDDs[root_number].PrintCombination( ost );
				CommandNotFound = false;  break;
			}

			case ECommand_print::alphabet :
			{
				if (cmd[2] == "") break;
				const Uint root_number = StoN<Uint>( cmd[2] );
				if ( !CheckRange( SeqBDDs, root_number ) )  break;
				ost << SeqBDDs[root_number].Alphabet() << endl;
				CommandNotFound = false;  break;
			}

			default:
				break;
		}
		break;  /* case ECommand::print */
	}
	// case ECommand::generate_xypic_code :
	// {
	// 	if (cmd[1] == "") break;
	// 	const Uint root_number = StoN<Uint>( cmd[1] );
	// 	GenerateXYpic( SeqBDDs, root_number, FileName );
	// 	CommandNotFound = false;  break;
	// }
	default:
		break;
	}

	if ( CommandNotFound ) {
		cerr << "command \"" << CmdLine << "\" not found" << endl;
	}

 /* save to history file */
	if ( add2history ) Add2History( CmdLine_history );

 }
	return 0;
}





int main() {

 /* initialize SeqBDDs (pointer to each SeqBDD root) */
	vector<SeqBDD_Root> Roots;

 /* UTable-table */
	SeqBDD_UTable UTable;

	Interactive( Roots, UTable );

	return 0;
}
