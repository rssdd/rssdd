
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <sys/param.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <malloc.h>


#include <iostream>
#include <fstream>

#include <string>
#include <vector>
#include <utility>           /* pair, make_pair */
#include <unordered_map>     /* command */

#include <initializer_list>
#include <iomanip>           /* print */
#include <sstream>           /* StoN, ToString */
#include <algorithm>         /* sort, min_element, unique */
#include <cmath>             /* log */
#include <cstdint>           /* uint_fast32_t, ... */

#include "mylib.hpp"
// #include "WeightedZDD.hpp"
#include "RSSDD.hpp"

using namespace std;





int main( int argc, char const *argv[] )
{
	/* args : IFileName  LOOPNUM  */
	if ( argc < 2 ) return 1;
	if ( !argv[1] or !argv[2] ) return 1;
	const string  IFileName( argv[1] );
	const size_t  LOOPNUM = StoN<size_t>( argv[2] );


	string RNAsequence;
	{
		ifstream fin( IFileName );
		if ( !FileCheck( fin, IFileName ) ) return 1;
		string buf;
		getline( fin, buf );
		stringstream ss( buf );
		ss >> RNAsequence;
	}
	const size_t RNAlength = RNAsequence.length();

	const string  OFileName = "outputdata/result_query2"
	                        + IFileName.substr(10)
	                        + "_" + ToString(LOOPNUM)
	                        + "_RSSDD.txt";
	/* IFileName.substr(10) : remove "inputdata/" */
	ofstream ost( OFileName );


	ost
	 << "input  filename : " << IFileName << endl
	 << "output filename : " << OFileName << endl
	 << LOOPNUM << " loops" << endl
	 << "RNA sequence : " << RNAsequence << "\n\n" << endl;


	vector<RSSDD_Root>  Roots;
	RSSDD_UTable  UTable;
	BuildFromFile( UTable, Roots, IFileName );
	const auto& SecstrSet = Roots.front();


	vector<EPairFlag>  Pattern;
	Uint    match_num;
	double  match_weight_sum;


	Progress  query_time_sum;
	query_time_sum.Start( "", ost );

	for ( size_t l = 0; l < RNAlength - 1; ++l ) {
	for ( size_t r = l + 1; r < RNAlength; ++r ) {
		/* "*************************..........**************************************" */
		/* "************************(..........)*************************************" */
		string  Pattern_dba = string( RNAlength, '*' );
		Pattern_dba[l] = '(';
		Pattern_dba[r] = ')';

		ost << "pattern : `" << Pattern_dba << '`' << endl;

		DBA2Pattern( Pattern, Pattern_dba );

		Progress  query_time( LOOPNUM );
		query_time.Start();
		for ( Uint i = 0; i < LOOPNUM; ++i ) {
			query_time.Print( i, ost );
			auto p = SecstrSet.MatchPatternNum( Pattern );
			match_num        = p.first;
			match_weight_sum = p.second;
		}
		query_time.Stop( "finished.",ost );
		query_time.PrintTime( ost );
		ost << endl;

		ost
		 << match_num << " structures matched.  "
		 << "sum of probability = " << match_weight_sum << endl
		 << endl;
	}}

	ost << "query time sum  = ";
	query_time_sum.Stop( "finished", ost );
	query_time_sum.PrintTime( ost );
	ost << endl;


	return 0;
}
