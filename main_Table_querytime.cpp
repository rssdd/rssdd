
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <sys/param.h>
#include <malloc.h>


#include <iostream>
#include <fstream>

#include <bitset>
#include <string>
#include <vector>
#include <stack>             /* used at DB to pair array */
#include <utility>           /* pair, make_pair */
#include <unordered_set>

#include <initializer_list>
#include <iomanip>           /* print */
#include <sstream>           /* StoN, ToString */
#include <algorithm>         /* sort, min_element, unique */


#include "mylib.hpp"
#include "WeightedZDD.hpp"
#include "RSSDD.hpp"

using namespace std;



using SecstrTableType = vector< pair< vector<BasePair>, TFreq > >;




class CSecstrSet
{
 public:

	SecstrTableType  Table;
	std::string      RNAsequence;
	double           Temperature;
	TFreq            sample_number;
	double           sample_prob_sum;
	double           PartFunc;
	double           MFE;

	CSecstrSet() {}
	~CSecstrSet() {}


	TFreq GetFreqMax() const
	{
		TFreq MaxTemp = 0;
		for ( const auto& e : Table ) {
			MaxTemp = std::max( MaxTemp, e.second );
		}
		return MaxTemp;
	}


	void set_values(
		/*R*/ std::string const &  init_RNAsequence     ,
		/*R*/ double      const    init_Temperature     ,
		/*R*/ TFreq       const    init_sample_number   ,
		/*R*/ double      const    init_sample_prob_sum ,
		/*R*/ double      const    init_PartFunc        )
	{
		RNAsequence     = init_RNAsequence;
		Temperature     = init_Temperature;
		sample_number   = init_sample_number;
		sample_prob_sum = init_sample_prob_sum;
		PartFunc        = init_PartFunc;

		/* set MFE */
		if ( MFE == 0.0 ) {
			MFE = Prob2FreeEnergy(
				  Freq2Prob( GetFreqMax() )
				, PartFunc
				, Temperature );
		}
	}

 /* alias, shortcut */
	double  Freq2Prob( TFreq  const  freq ) const
	{ return (sample_prob_sum * freq) / sample_number; }

	TFreq   Prob2Freq( double const  prob ) const
	{ return (sample_number * prob) / sample_prob_sum; }


 /* methods */

	virtual void PrintHeader( std::ostream &  ost ) const
	{  /* used in PrintCombinations() */
		/* position number */
		for ( size_t i = 0; i <= RNAsequence.length() / 5; ++i ) {
			ost << std::setw(5) << std::left << 5 * i;
		}
		ost << std::endl;

		/* ruler */
		for ( size_t i = 0; i < RNAsequence.length(); ++i ) {
			switch (i % 10) {
				case 0  : ost << '|'; break;
				case 5  : ost << ':'; break;
				default : ost << '-'; break;
			}
		}
		ost << std::endl;

		/* RNA sequence */
		ost << RNAsequence << std::endl;
	}

	bool ExistSecstr( std::string const &  query_DB ) const
	{
		std::vector<BasePair>  pair_array;
		DB2PairArray( pair_array, query_DB );
		for ( const auto& e : Table ) {
			if ( e.first == pair_array ) return true;
		}
		return false;
	}

	std::pair<Uint,double>
	 MatchPatternNum( std::vector<EPairFlag> const &  Pattern ) const
	{
		Uint  match_num = 0;
		TFreq match_weight_sum = 0;

		const TFreq PairNumInPattern = CountPairInPattern( Pattern );

		for ( const auto& e : Table ) {
			bool match = true;
			TFreq  count = PairNumInPattern;
			for ( const auto& p : e.first ) {
				if ( not match ) break;
				switch ( Pattern[p] ) {
					case EPairFlag::NotPair :
						match = false;
						break;
					case EPairFlag::Pair :
						--count;
						break;
					default :
						break;
				}
			}
			if ( count > 0 ) match = false;
			if ( match ) {
				++match_num;
				match_weight_sum += e.second;
			}
		}

		return std::make_pair( match_num, Freq2Prob( match_weight_sum ) );
	}
};







void BuildTableFromFile(
	/*W*/ CSecstrSet      &  SecstrSet ,
	/*R*/ string    const &  IFileName )
{
	ifstream fin( IFileName );
	if ( !FileCheck( fin, IFileName ) ) return;


	string    RNAsequence;
	double    Temperature = TEMPERATURE_DEFAULT;
	TFreq     sample_number = 0;
	double    sample_prob_sum = 0.0;
	double    PartFunc = 0.0;
	Progress  time_sum;
	Progress  progress;


	/***** local functions ****************************/
	auto ReadSeqAndTempFromFile = [&] ()
	{
		ifstream fin( IFileName );
		if ( !FileCheck( fin, IFileName ) )  return;

		string buf;
		while ( getline( fin, buf ) ) {
			if ( buf.length() == 0 )  continue;
			if ( buf[0] == '>' )  continue;
			break;
		}
		stringstream  ss(buf);
		ss >> RNAsequence >> Temperature;
		// stringstream  ss;
		// ss << buf;
		// getline( fin, buf );
		// ss << buf;
		// ss >> Temperature >> RNAsequence;
		Temperature = Celsius2Kelvin( Temperature );
	};

	auto ReadDBFromFile = [&] ()
	{
		string            buf;
		vector<BasePair>  pair_array_buf;
		stringstream      ss;
		string            DB;
		TFreq             freq;
		double            free_energy = 0.0;

		while ( getline( fin, buf ) ) {
			if ( buf.length() == 0 )  continue;  /* discard this line */

			/* reset ss */
			ss.str("");  /* clear buffer */
			ss.clear(stringstream::goodbit);  /* clear the state */

			ss << buf;
			ss >> freq >> DB >> free_energy;

			if ( not IsDBstring(DB) ) continue;  /* discard this line */

			sample_number   += freq;
			sample_prob_sum += FreeEnergy2Prob( free_energy, PartFunc, Temperature );
			if ( DB.length() > RNA_LENGTH_MAX ) {
				cout
				 << "RNA sequence with >"
				 << RNA_LENGTH_MAX
				 << "bp is not supported" << endl;
				return;
			}

			DB2PairArray( pair_array_buf, DB );
			pair_array_buf.push_back( END_SYMBOL );
			SecstrSet.Table.emplace_back( pair_array_buf, freq );
		}
		return;
	};



	 /* print table size */
		// size_t each_size = sizeof( SecstrTable.front().first.front() );
		// size_t prob_size = sizeof( SecstrTable.front().second );
		// size_t freq_size = sizeof( SecstrTable.front() );
		// size_t size_all = sum_of_len * each_size + SecstrTable.size() * prob_size + SecstrTable.size() * freq_size;

		// cout
		//  << endl
		//  << "[SecstrTable size]"                                       << endl
		//  << "  A : pair     : " << freq_size              << "[bytes]" << endl
		//  << "  B : arrays   : " << sum_of_len * each_size << "[bytes]" << endl
		//  << "  C : freq     : " << prob_size              << "[bytes]" << endl
		//  << "  D : #sec.str : " << SecstrTable.size()                  << endl
		//  << "  A * D + B + C * D = " << size_all          << "[bytes]" << endl
		//  << endl;



	ReadSeqAndTempFromFile();
	PartFunc = GetPartFunc( RNAsequence, Temperature );


	{ /* read from the file */
		time_sum.Start("reading   ");
		ReadDBFromFile();
		time_sum.Stop(" done. ");
		time_sum.PrintTime();
		time_sum.AccumulateTime();
		cout << endl;
	}

	{
	 /* sort the table */
		time_sum.Start("sorting   ");
		sort( SecstrSet.Table.begin(), SecstrSet.Table.end() );
		time_sum.Stop(" done. ");
		time_sum.PrintTime();
		time_sum.AccumulateTime();
		cout << endl;
	}

	SecstrSet.set_values(
			  RNAsequence
			, Temperature
			, sample_number
			, sample_prob_sum
			, PartFunc );

}



int main( int argc, char const *argv[] )
{
	/* args : IFileName  LOOPNUM  W  */
	if ( argc < 3 ) return 1;
	if ( !argv[1] or !argv[2] or !argv[3] or !argv[4] ) return 1;
	const string  IFileName( argv[1] );
	const size_t  LOOPNUM = StoN<size_t>( argv[2] );
	const size_t  W       = StoN<size_t>( argv[3] );
	const size_t  PAIRNUM = StoN<size_t>( argv[4] );
	 /* number of '.'; W = 4 ~ 10 */


	string RNAsequence;
	{
		ifstream fin( IFileName );
		if ( !FileCheck( fin, IFileName ) ) return 1;
		string buf;
		getline( fin, buf );
		stringstream ss( buf );
		ss >> RNAsequence;
	}
	const size_t RNAlength = RNAsequence.length();

	const string  OFileName = "outputdata/result_"
	                        + IFileName.substr(10)
	                        + "_" + ToString(LOOPNUM)
	                        + "_" + ToString(W)
	                        + "_" + ToString(PAIRNUM)
	                        + "_Table.txt";
	/* IFileName.substr(10) : remove "inputdata/" */
	ofstream ost( OFileName );


	ost
	 << "input  filename : " << IFileName << endl
	 << "output filename : " << OFileName << endl
	 << LOOPNUM << " loops" << endl
	 << "W = " << W << endl
	 << "number of pairs = " << PAIRNUM << endl
	 << "RNA sequence : " << RNAsequence << "\n\n" << endl;


	CSecstrSet    SecstrSet;
	BuildTableFromFile( SecstrSet, IFileName );




	vector<EPairFlag>  Pattern;
	Uint    match_num;
	double  match_weight_sum;


	Progress  query_time_sum;
	query_time_sum.Start( "", ost );

	for ( size_t l = 0; l <= RNAlength - 2 * PAIRNUM - W; ++l ) {
		/* "*************************..........**************************************" */
		/* "************************(..........)*************************************" */
		string  Pattern_dba = string( l, '*' )
		              + string( PAIRNUM, '(' )
		              + string( W, '.' )
		              + string( PAIRNUM, ')' )
		              + string( RNAlength - 2 * PAIRNUM - W - l, '*' );

		ost << "pattern : `" << Pattern_dba << '`' << endl;

		DBA2Pattern( Pattern, Pattern_dba );

		Progress  query_time( LOOPNUM );
		query_time.Start();
		for ( Uint i = 0; i < LOOPNUM; ++i ) {
			query_time.Print( i, ost );
			auto p = SecstrSet.MatchPatternNum( Pattern );
			match_num        = p.first;
			match_weight_sum = p.second;
		}
		query_time.Stop( "finished.",ost );
		query_time.PrintTime( ost );
		ost << endl;

		ost
		 << match_num << " structures matched.  "
		 << "sum of probability = " << match_weight_sum << endl
		 << endl;
	}

	ost << "query time sum  = ";
	query_time_sum.Stop( "finished", ost );
	query_time_sum.PrintTime( ost );
	ost << endl;


	return 0;
}
