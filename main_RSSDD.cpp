
/*
 # required libraries

  - GNU readline
     $  sudo apt-get install libreadline6 libreadline6-dev



 # Use compile option "-std=c++11" and "-lreadline"
    $  g++ -std=c++11  -o ***.out  mylib.cpp  ***.cpp  -lreadline


*/
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <sys/param.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <malloc.h>


#include <iostream>
#include <fstream>

#include <string>
#include <vector>
#include <utility>           /* pair, make_pair */
#include <unordered_map>     /* command */

#include <initializer_list>
#include <iomanip>           /* print */
#include <sstream>           /* StoN, ToString */
#include <algorithm>         /* sort, min_element, unique */
#include <cmath>             /* log */
#include <cstdint>           /* uint_fast32_t, ... */

#include "mylib.hpp"
// #include "WeightedZDD.hpp"
#include "RSSDD.hpp"

using namespace std;





/* file name */
const string HELP_FILE_NAME    = "RSSDD_help.txt";
const string HISTORY_FILE_NAME = ".RSSDD_history";

/* prompt string of interactive() */
const string PROMPT_STRING = "RSSDD> ";


/**********************************************************
 interface
 **********************************************************/

void Interactive(
	/*W*/ std::vector<RSSDD_Root> &  Roots  ,
	/*W*/ RSSDD_UTable            &  UTable )
{
	string this_dir;  /* help file and history file directory */
	{
		char path[1024];
		auto p = getcwd( path, sizeof(path) );
		if ( !p ) return;
		this_dir = path;
		this_dir.push_back('/');
	}

	VIENNARNA_DIR = this_dir + "ViennaRNA-2.2.4";

	read_history( (this_dir + HISTORY_FILE_NAME).c_str() );


 /* map : command string -> integer */
	enum class ECommand : int {
		  ls
		, pwd
		, less
		, chdir
		, quit
		, help
		, build
		, LCM_algorithm
		, makeroot
		, search
		, pattern_match
		, list
		, print
		, uniqtable_status
		, memory_usage
		, malloc_stats
		, countpath
	};
	unordered_map<string,ECommand> command_id;
	{
		command_id["ls"]                  = ECommand::ls;
		command_id["pwd"]                 = ECommand::pwd;
		command_id["less"]                = ECommand::less;
		command_id["cd"]                  = ECommand::chdir;
		command_id["chdir"]               = ECommand::chdir;
		command_id["q"]                   = ECommand::quit;
		command_id["quit"]                = ECommand::quit;
		command_id["exit"]                = ECommand::quit;
		command_id["h"]                   = ECommand::help;
		command_id["help"]                = ECommand::help;
		command_id["uniq"]                = ECommand::uniqtable_status;
		command_id["uniq-status"]         = ECommand::uniqtable_status;
		command_id["unique-status"]       = ECommand::uniqtable_status;
		command_id["unique-table-status"] = ECommand::uniqtable_status;
		command_id["mem"]                 = ECommand::memory_usage;
		command_id["memory"]              = ECommand::memory_usage;
		command_id["memory-usage"]        = ECommand::memory_usage;
		command_id["malloc-stats"]        = ECommand::malloc_stats;
		command_id["b"]                   = ECommand::build;
		command_id["build"]               = ECommand::build;
		command_id["lcm"]                 = ECommand::LCM_algorithm;
		command_id["LCM"]                 = ECommand::LCM_algorithm;
		command_id["mkrt"]                = ECommand::makeroot;
		command_id["makeroot"]            = ECommand::makeroot;
		command_id["search"]              = ECommand::search;
		command_id["search-secstr"]       = ECommand::search;
		command_id["search-pattern"]      = ECommand::pattern_match;
		command_id["pattern-match"]       = ECommand::pattern_match;
		command_id["list"]                = ECommand::list;
		command_id["p"]                   = ECommand::print;
		command_id["print"]               = ECommand::print;
		command_id["countpath"]           = ECommand::countpath;
	}

	enum class ECommand_build : int {
		  set_union
		, set_intersection
		, set_difference    /* unimplemented */
		, subset
	};
	unordered_map<string,ECommand_build> command_build_id;
	{
		command_build_id["union"]        = ECommand_build::set_union;
		command_build_id["intersection"] = ECommand_build::set_intersection;
		// command_build_id["setminus"]     = ECommand_build::set_difference;
		// command_build_id["difference"]   = ECommand_build::set_difference;
		command_build_id["subset"]       = ECommand_build::subset;
	}

	enum class ECommand_buildsubset : int {
		  pair
		, DBA
		, top
		, weight_threshold
	};
	unordered_map<string,ECommand_buildsubset> command_buildsubset_id;
	{
		command_buildsubset_id["pair"]              = ECommand_buildsubset::pair;
		command_buildsubset_id["pairs"]             = ECommand_buildsubset::pair;
		command_buildsubset_id["dba"]               = ECommand_buildsubset::DBA;
		command_buildsubset_id["dot-bracket-aster"] = ECommand_buildsubset::DBA;
		command_buildsubset_id["DBA"]               = ECommand_buildsubset::DBA;
		command_buildsubset_id["top"]               = ECommand_buildsubset::top;
		// command_buildsubset_id["w"]                 = ECommand_buildsubset::weight_threshold;
		// command_buildsubset_id["weight"]            = ECommand_buildsubset::weight_threshold;
		// command_buildsubset_id["threshold"]         = ECommand_buildsubset::weight_threshold;
	}

	enum class ECommand_print : int {
		  list
		, node
		, allnodes
		, dot_bracket
		, itemset
		, pattern
		, disignated_pattern
		, DBA
		, pair_frequency
	};
	unordered_map<string,ECommand_print> command_print_id;
	{
		command_print_id["list"]               = ECommand_print::list;
		command_print_id["node"]               = ECommand_print::node;
		command_print_id["nodes"]              = ECommand_print::node;
		command_print_id["allnodes"]           = ECommand_print::allnodes;
		command_print_id["secstr"]             = ECommand_print::dot_bracket;
		command_print_id["secstrs"]            = ECommand_print::dot_bracket;
		command_print_id["db"]                 = ECommand_print::dot_bracket;
		command_print_id["dotbracket"]         = ECommand_print::dot_bracket;
		command_print_id["itemset"]            = ECommand_print::itemset;
		command_print_id["pattern"]            = ECommand_print::pattern;
		command_print_id["disignatedpattern"]  = ECommand_print::disignated_pattern;
		command_print_id["dba"]                = ECommand_print::DBA;
		command_print_id["dotbracketaster"]    = ECommand_print::DBA;
		command_print_id["pairfreq"]           = ECommand_print::pair_frequency;
	}


/* command line loop */
 string CmdLine;
 while ( ReadlineString( CmdLine, PROMPT_STRING ) ) {

  if ( CmdLine == "" ) continue;

  bool CommandNotFound = true;
  bool add2history = true;
  const string CmdLine_history = CmdLine;  /* save to history */
  if ( CmdLine[0] == ' ' ) {  add2history = false;  }


  vector<string> cmd( 1024, "" );
  { /* split CmdLine to cmd[] */
      stringstream ss( CmdLine );
      string buf;
      size_t i = 0;
      while ( ss >> buf ) { cmd[i++] = buf; }
  }


  const bool FromFile = StringFound( CmdLine, " < " );
  const bool ToFile   = StringFound( CmdLine, " > " );
  string FileName;
  {
      if ( FromFile or ToFile ) {
         /* extract file name */
          size_t i = 0;
          while ( i < cmd.size() - 1 ) {
              if ( cmd[i] == "<" or cmd[i] == ">" ) break;
              ++i;
          }
          if ( i == cmd.size() or cmd[i + 1] == "" ) {
              cout << "ERROR: missing filename" << endl;
              CommandNotFound = false;  break;
          }
          FileName = cmd[i + 1];
          cmd.erase( cmd.begin() + i, cmd.end() );
      }
      if ( FromFile ) {
         /* erase after " < " */
          CmdLine.erase( CmdLine.begin() + CmdLine.find(" < "), CmdLine.end() );
      }
      if ( ToFile ) {
         /* erase after " > " */
          CmdLine.erase( CmdLine.begin() + CmdLine.find(" > "), CmdLine.end() );
      }
  }

  switch ( command_id[cmd[0]] ) {
	case ECommand::quit :
	{
		write_history( (this_dir + HISTORY_FILE_NAME).c_str() );
		return;
	}

	case ECommand::ls :
	case ECommand::pwd :
	case ECommand::less :
	{
		if ( system( CmdLine.c_str() ) == -1 ) {
			cout << "ERROR: system()" << endl;
		}
		add2history = false;
		CommandNotFound = false;  break;
	}

	case ECommand::chdir :
	{
		if ( cmd[1] == "" )  break;
		if ( chdir( cmd[1].c_str() ) == -1 ) {
			cout << "ERROR: system()" << endl;
		}
		add2history = false;
		CommandNotFound = false;  break;
	}


	case ECommand::help :
	{
		if ( system( ("less " + this_dir + HELP_FILE_NAME).c_str() ) == -1 ) {
			cout << "ERROR: system()" << endl;
		}
		add2history = false;
		CommandNotFound = false;  break;
	}

	case ECommand::uniqtable_status :
	{
		PrintUniqueTableStatus( UTable );
		CommandNotFound = false;  break;
	}
	case ECommand::memory_usage :
	{
		PrintMemoryUsage( UTable );
		CommandNotFound = false;  break;
	}
	case ECommand::malloc_stats :
	{
		malloc_stats();
		CommandNotFound = false;  break;
	}

	case ECommand::build :
	{
		if (FromFile) {
			BuildFromFile( UTable, Roots, FileName );
			CommandNotFound = false;  break;
		}

		if ( cmd[1] == "" )  break;

		switch ( command_build_id[cmd[1]] ) {
		case ECommand_build::set_union :
		{
			SetOperation_main( UTable, Roots, StoN<Uint>(cmd[2]), StoN<Uint>(cmd[3]), '+' );
			CommandNotFound = false;  break;
		}
		case ECommand_build::set_intersection :
		{
			SetOperation_main( UTable, Roots, StoN<Uint>(cmd[2]), StoN<Uint>(cmd[3]), '*' );
			CommandNotFound = false;  break;
		}
		case ECommand_build::subset :
		{
			if (cmd[2] == "") break;
			const Uint  root_number = StoN<Uint>( cmd[2] );
			if ( ! Vector::CheckRange( Roots, root_number ) ) break;
			const RSSDD_Root&  Z = Roots[root_number];

			switch ( command_buildsubset_id[cmd[3]] ) {
			case ECommand_buildsubset::pair :
			{  /* -(0,10) +(1,9) ... */

				string pairs_str;
				string notpairs_str;
				if ( !ReadlineString( pairs_str,    " include : " ) ) break;
				if ( !ReadlineString( notpairs_str, " exclude : " ) ) break;
				Add2History( pairs_str );
				Add2History( notpairs_str );

				vector<BasePair> Pairs;
				vector<BasePair> NotPairs;
				while (1) {
					const int first  = ExtractNumber( pairs_str );
					const int second = ExtractNumber( pairs_str );
					if ( first < 0 or second < 0 )  break;
					Pairs.emplace_back( first, second );
				}
				while (1) {
					const int first  = ExtractNumber( notpairs_str );
					const int second = ExtractNumber( notpairs_str );
					if ( first < 0 or second < 0 )  break;
					NotPairs.emplace_back( first, second );
				}

				if ( Pairs.empty() and NotPairs.empty() ) break;

				sort( Pairs.begin(),    Pairs.end()    );
				sort( NotPairs.begin(), NotPairs.end() );

				vector<EPairFlag> DisignatedPattern;
				if ( ! PairArray2Pattern(
						  DisignatedPattern
						, Z.RNAsequence().length()
						, Pairs
						, NotPairs ) )
					break;

				BuildSubsetForPattern(
						  UTable
						, Roots
						, root_number
						, DisignatedPattern );
				CommandNotFound = false;  break;
			}
			case ECommand_buildsubset::DBA :
			{  /* ..((((..***..))))*. */
				string DBA( Z.RNAsequence().length(), ' ' );
				Z.PrintHeader( cout );
				while ( ReadlineString( DBA, "" ) ) {
					Add2History( DBA );
					if ( IsDBAstring( DBA ) and DBA.length() == Z.RNAsequence().length() )
						break;
					cout << endl;
					Z.PrintHeader( cout );
				}
				vector<EPairFlag> DisignatedPattern;
				DBA2Pattern( DisignatedPattern, DBA );
				BuildSubsetForPattern(
						  UTable
						, Roots
						, root_number
						, DisignatedPattern );
				CommandNotFound = false;  break;
			}
			case ECommand_buildsubset::top :
			{
				const Uint OutputNum = GetOptionValue<Uint>( cmd, "top" );
				BuildTopK_main( UTable, Roots, root_number, OutputNum );
				CommandNotFound = false;  break;
			}
			default:
				break;
			}
			break;  /* ECommand_build::subset */
		}
		default:
			break;
		}
		break;  /* case ECommand::build */
	}

	case ECommand::makeroot:
	{  /* makeroot   NODE_ADDRESS   SUPERSET_ROOT_NUMBER   --name NEWNAME */
		if ( cmd[1] == "" or cmd[2] == "" )  break;

		size_t addr;
		{
			stringstream ss;
			ss << hex << cmd[1];
			ss >> addr;
		}
		const auto  rootnode = reinterpret_cast<RSSDD_Node*>(addr);

		const Uint  superset_number = StoN<Uint>( cmd[2] );

		string newname = "";
		if ( StringFound( CmdLine, "--name" ) ) {
			newname = cmd[ 1 + Vector::FindPos( cmd, string("--name") ) ];
		}
		MakeRoot( Roots, newname, rootnode, superset_number );
		CommandNotFound = false;  break;
	}

	case ECommand::LCM_algorithm :
	{
		if (cmd[1] == "") break;
		const Uint root_number = StoN<Uint>( cmd[1] );

		/* number to output */
		Uint OutputNum = LCM_OUTPUT_NUM_DEFAULT;
		if ( StringFound( CmdLine, "--top" ) ) {
			OutputNum = GetOptionValue<Uint>( cmd, "--top" );
		}

		/* set minimal support */
		double MinProb = 0.0;  /* default value */
		if ( StringFound( CmdLine, "--probmin" ) ) {
			MinProb = GetOptionValue<double>( cmd, "--probmin" );
		}

		/* minimum number of pairs that pattern must have */
		Uint MinNumOfPairs = 1;  /* default value == 1 */
		if ( StringFound( CmdLine, "--pairmin" ) ) {
			MinNumOfPairs = GetOptionValue<Uint>( cmd, "--pairmin" );
		}

		/* skip probability threshold (see the description in hpp) */
		double SkipProb = LCM_HIGH_PROBABILITY;
		if ( StringFound( CmdLine, "--skip-prob" ) ) {
			SkipProb = GetOptionValue<double>( cmd, "--skip-prob" );
		}

		/* true --> output maximal pattern only */
		const bool LCMmax = StringFound( CmdLine, "--maximal" );
		LCM( 
				  UTable
				, Roots
				, root_number
				, OutputNum
				, LCMmax
				, MinProb
				, MinNumOfPairs
				, SkipProb
				, ToFile
				, FileName );
		CommandNotFound = false;  break;  /* case ECommand::LCM_algorithm */
	}

	case ECommand::search :
	{
		if (cmd[1] == "") break;
		const Uint root_number = StoN<Uint>( cmd[1] );
		if ( ! Vector::CheckRange( Roots, root_number ) )  break;
		const auto Z = Roots[root_number];

		string query_DB;
		Z.PrintHeader( cout );
		while ( ReadlineString( query_DB, "" ) ) {
			Add2History( query_DB );
			if ( IsDBstring( query_DB ) and query_DB.length() == Z.RNAsequence().length() ) break;
			cout << endl;
			Z.PrintHeader( cout );
		}

		const bool found = Z.ExistSecstr( query_DB );
		cout
		 << endl
		 << ( found ? "found" : "not found" ) << endl
		 << endl;
		CommandNotFound = false;  break;
	}

	case ECommand::pattern_match :
	{  /* ..((((..***..))))*. */
		if (cmd[1] == "") break;
		const Uint root_number = StoN<Uint>( cmd[1] );
		if ( ! Vector::CheckRange( Roots, root_number ) )  break;
		const auto Z = Roots[root_number];

		string DBA( Z.RNAsequence().length(), ' ' );
		Z.PrintHeader( cout );
		while ( ReadlineString( DBA, "" ) ) {
			Add2History( DBA );
			if ( IsDBAstring( DBA ) and DBA.length() == Z.RNAsequence().length() )
				break;
			cout << endl;
			Z.PrintHeader( cout );
		}
		vector<EPairFlag> DisignatedPattern;
		DBA2Pattern( DisignatedPattern, DBA );
		const auto p = Z.MatchPatternNum( DisignatedPattern );
		const Uint    match_num        = p.first;
		const double  match_weight_sum = p.second;
		cout
		 << endl
		 << match_num << " structures matched." << endl
		 << "sum of probability = " << match_weight_sum << endl
		 << endl;
		CommandNotFound = false;  break;
	}

	case ECommand::list :
	{
		PrintList( Roots );
		CommandNotFound = false;  break;
	}
	case ECommand::print :
	{
		ofstream fout( FileName.c_str(), ios::out );
		if ( ToFile and !FileCheck( fout, FileName ) )  break;
		auto& ost = ( ToFile ? fout : cout );

		if ( cmd[1] == "" )  break;

		if ( command_print_id[cmd[1]] == ECommand_print::allnodes )
		{
			ost << endl;
			for ( const auto & e : UTable )  ost << e << endl;
			ost << endl;
			CommandNotFound = false;  break;
		}

		if ( cmd[2] == "" ) break;
		const Uint  root_number = StoN<Uint>( cmd[2] );
		if ( ! Vector::CheckRange( Roots, root_number ) )  break;

		switch ( command_print_id[cmd[1]] ) {
		case ECommand_print::node : {
			const bool omit_zero = StringFound( CmdLine, "--omit-zero" );
			Roots[root_number].PrintNodes( omit_zero, ost );
			CommandNotFound = false;  break;
		}
		case ECommand_print::dot_bracket : {
			Roots[root_number].PrintDB( ost );
			CommandNotFound = false;  break;
		}
		case ECommand_print::itemset : {
			Roots[root_number].PrintItemset( ost );
			CommandNotFound = false;  break;
		}
		case ECommand_print::pattern : {
			Roots[root_number].PrintPatternResult( ost );
			CommandNotFound = false;  break;
		}
		case ECommand_print::disignated_pattern : {
			Roots[root_number].PrintDisignatedPattern( ost );
			CommandNotFound = false;  break;
		}
		case ECommand_print::DBA : {
			Roots[root_number].PrintDBA( ost );
			CommandNotFound = false;  break;
		}
		case ECommand_print::pair_frequency : {
			Roots[root_number].PrintPairFrequency( ost );
			CommandNotFound = false;  break;
		}
		default:
			break;
		}
		break;  /* case ECommand::print */
	}

	default:
		break;
	}

	if ( CommandNotFound ) {
		cerr << "command \"" << CmdLine << "\" not found" << endl;
	}

 /* save to history file */
	if ( add2history ) Add2History( CmdLine_history );

 }
	return;
}



int main() {

 /* initialize Roots (pointer to each RSSDD root) */
	vector<RSSDD_Root>  Roots;

 /* UTable-table */
	RSSDD_UTable  UTable;

	Interactive( Roots, UTable );

	return 0;
}






