
#pragma once

/**********************************************************
 - representation of sets of RNA secondary structures by ZDD
 - supported maximum length of RNA is 255


 # required libraries
  - GNU readline
     $  sudo apt-get install libreadline6 libreadline6-dev
 
  - Vienna RNA package
     $  sudo apt-add-repository ppa:j-4/vienna-rna
     $  sudo apt-get update
     $  sudo apt-get install vienna-rna

  - edited version of "RNAfold.c" of ViennaRNA



 # Use compile option "-std=c++11" and "-lreadline"
   "-lreadline" must be put in tail.
    $  g++ -std=c++11 -o RSSDD.out mylib.cpp RSSDD.cpp -lreadline



 # making input file 
  - use "MakeInputfile.sh"



 # comments
  - argument types
 	* W = Write.  rewrite the passed argument
 	* R = Read.   read-only argument
   (example)
    *R* ZDD_Node* const  rootnode ,
   --> read-only, pointer to the node is constant 



 # how to use functions without Interactive()
  - make
      - vector<RSSDD_Root>  Roots
      - RSSDD_UTable  UTable
    Roots  manages all ZDDs (pointer to its rootnode, etc.),
    and UTable manages all nodes.



 * Tested on g++ (Ubuntu 4.8.1-2ubuntu1~12.04) 4.8.1
 * 2016/6/22
 * Hideaki Noshiro
 **********************************************************/



#include <iostream>
#include <string>
#include <vector>
#include <unordered_set>     /* UniqueTable */
#include <iomanip>           /* print */
#include <utility>           /* rel_ops */
#include <cstdint>

#include "mylib.hpp"
#include "WeightedZDD.hpp"



/**********************************************************
 settings
 **********************************************************/

/* ignore weight */
constexpr bool IGNORE_WEIGHT = false;


/* LCM settings */
constexpr Uint   LCM_OUTPUT_NUM_DEFAULT    = 20;
constexpr double LCM_HIGH_PROBABILITY      = 0.8;

/* pair to be printed as "{ }" */
constexpr double CURLY_BRACKET_PROBABILITY = 0.8;

/* pattern character */
constexpr char PATTERN_CHAR_PAIR             = 'O';
constexpr char PATTERN_CHAR_PROBABLY_PAIR    = 'o';
constexpr char PATTERN_CHAR_UNDECIDED        = '*';
constexpr char PATTERN_CHAR_PROBABLY_NOTPAIR = 'x';
constexpr char PATTERN_CHAR_NOTPAIR          = 'X';
constexpr char PATTERN_CHAR_DUMMY            = ' ';

/* dot-bracket-aster character */
constexpr char DBA_CHAR_PAIR_FIRST           = '(';
constexpr char DBA_CHAR_PAIR_SECOND          = ')';
constexpr char DBA_CHAR_PROBABLY_PAIR_FIRST  = '{';
constexpr char DBA_CHAR_PROBABLY_PAIR_SECOND = '}';
constexpr char DBA_CHAR_UNDECIDED            = '*'; // 
constexpr char DBA_CHAR_PROBABLY_NOTPAIR     = ','; // 
constexpr char DBA_CHAR_NOTPAIR              = '.'; // 



/**********************************************************
 alias, class prototypes
 **********************************************************/

class BasePair;
class FrequentPattern;
class RSSDD_Root;

using TFreq             = Uint32;
using RSSDD_Node   = WeightedZDD_Node< BasePair, TFreq >;
using RSSDD_UTable = WeightedZDD_UTable< RSSDD_Node >;



/**********************************************************
 definition of constants
 **********************************************************/

constexpr size_t  SEQUENCE_LENGTH_MAX = 255;

constexpr TFreq    TFREQ_MAX   =  std::numeric_limits<TFreq>::max();
constexpr TFreq    TFREQ_MIN   = -std::numeric_limits<TFreq>::max();

/* MAX of BasePair */
constexpr int  TBASEPAIR_MAX      = UINT16_MAX;
constexpr int  TBASEPAIR_HALF_MAX = UINT8_MAX;

constexpr int  END_SYMBOL = TBASEPAIR_MAX;


/* strings */
extern std::string VIENNARNA_DIR;

/* energy constant (use the same value of ViennaRNA) */
constexpr double  GASCONST = 1.98717;
  /* [cal/(K * mol)] ( 8.31[J/(K * mol)] / 4.184[J/cal] ) */

constexpr double Celsius2Kelvin( const double  Temperature ) {
	return Temperature + 273.15;
}
constexpr double  TEMPERATURE_DEFAULT = Celsius2Kelvin( 37.0 );


/* maximum supported length of an RNA sequence */
constexpr int  RNA_LENGTH_MAX = TBASEPAIR_HALF_MAX;

/* pattern size */
constexpr int  PATTERN_ROWSIZE = RNA_LENGTH_MAX;
constexpr int  PATTERN_COLSIZE = RNA_LENGTH_MAX + 1;
constexpr int  PATTERN_SIZE    = PATTERN_ROWSIZE * PATTERN_COLSIZE;


enum class EPairFlag : Uint8 {
	  Dummy
	, Pair
	, ProbablyPair
	, Undecided
	, ProbablyNotPair
	, NotPair
};


/* overload "<<" of EPairFlag */
inline std::ostream & operator<<(
	/*W*/ std::ostream       &  ost ,
	/*R*/ EPairFlag    const    num );



/**********************************************************
 function prototypes and descriptions
 **********************************************************/


/**********************************************************
 probability and free energy of RNA secondary structure

 - Z : partition function
 - P : existence probability of secondary structure
 - E : free energy of secondary structure [kcal/mol]
 - k : gas const [cal / (K * mol)]
 - T : temperature [K]

 P = (1/Z) * exp( -((E * 1000) / (k * T)) )

 E = -( (k * T) * log(P * Z) ) / 1000

 **********************************************************/
/* calculate free energy from probability and partition function */
inline double Prob2FreeEnergy(
	/*R*/ double const  Prob        ,
	/*R*/ double const  PartFunc    ,
	/*R*/ double const  Temperature );


/* calculate probability from free energy and partition function */
inline double FreeEnergy2Prob(
	/*R*/ double const  Energy      ,
	/*R*/ double const  PartFunc    ,
	/*R*/ double const  Temperature );


double GetPartFunc(
	/*R*/ std::string const &  RNAsequence ,
	/*R*/ double      const    Temperature );



/**********************************************************
 conversion among secondary structure representations

 - secondary structure
	* PairArray      : [ (0,10), (1,9), (2,8), (3,7), (11,17), (12,16) ]
	* DB(DB) : ((((...))))((...))..

 - secondary structure pattern
	* Pattern    : upper triangle matrix
	* PairArray  : pairs & notpairs [ -(0,10), +(1,9), ... ]
	* DBA        : dot-bracket-aster notation  "..((...***...)).."
 **********************************************************/

/* check if the input string is dot-bracket-aster */
bool IsDBAstring(
	/*R*/ std::string const &  DBA );


/* check if the input string is dot-bracket */
inline bool IsDBstring(
	/*R*/ std::string const &  DB );


/* convert DB to PairArray (output by rewrite) */
void DB2PairArray(
	/*W*/ std::vector<BasePair>       &  PairArray ,
	/*R*/ std::string           const &  DB        );


/* convert DB to PairArray (output by return) */
inline std::vector<BasePair> DB2PairArray(
	/*R*/ std::string const &  DB );


/* convert PairArray to DB (output by return) */
inline std::string PairArray2DB(
	/*R*/ std::vector<BasePair> const &  PairArray ,
	/*R*/ size_t                const    seqleng   );


/************************************************
 * initialize pattern matrix.
 * fill Pattern[] with EPairFlag::Dummy and EPairFlag::Undecided.
 * (example : seqleng == 4)

            *****       x???x
            *****       xx??x
            *****  ==>  xxx?x 
            *****       xxxxx
            *****       xxxxx

 *******************************************************/
inline void InitializePattern(
	/*W*/ std::vector<EPairFlag>       &  Pattern ,
	/*R*/ size_t                 const    seqleng );


/* '?' --> 'X' if 'O' is in the same row or column of '?'
 * if there are some conflicts (two 'O's in the same line),
 * return false; */
bool GetDerivedPattern(
	/*W*/ std::vector<EPairFlag> &  Pattern );


/* convert PairArray to Pattern matrix */
bool PairArray2Pattern(
	/*W*/ std::vector<EPairFlag>       &  Pattern  ,
	/*R*/ size_t                 const    seqleng  ,
	/*R*/ std::vector<BasePair>  const &  Pairs    ,
	/*R*/ std::vector<BasePair>  const &  NotPairs );


/* convert DBA-string to Pattern matrix */
void DBA2Pattern(
	/*W*/ std::vector<EPairFlag>       &  Pattern ,
	/*R*/ std::string            const &  DBA     );


/*************************************************
 	Dummy                       --> skip
	1. (i,j) is NotPair         -->  "."
	2. (i,j) is ProbablyNotPair -->  ","
	3. (i,j) is Undecided       -->  "*"
	4. (i,j) is ProbablyPair    -->  "{ }"
	5. (i,j) is Pair            -->  "( )"
 *************************************************/
/* convert Pattern to DBA-string. used in LCM (output by rewrite) */
void Pattern2DBA(
	/*W*/ std::string                  &  DBA     ,
	/*R*/ std::vector<EPairFlag> const &  Pattern );

/* convert Pattern to DBA-string. used in LCM (output by return) */
inline std::string Pattern2DBA(
	/*R*/ std::vector<EPairFlag> const &  Pattern );


/* PairFreq matrix to Pattern */
void PairFreq2Pattern(
	/*W*/ std::vector<EPairFlag>       &  Pattern  ,
	/*R*/ size_t                 const    seqleng  ,
	/*R*/ std::vector<TFreq>     const &  PairFreq ,
	/*R*/ TFreq                  const    setsize  );


/* gain pattern matrix from ZDD under {rootnode}.
 * count PairFreq in this function,
 * if PairFreq[i] == setsize, Pattern[i] = 'O' etc. */
inline void CalcPattern(
	/*W*/ std::vector<EPairFlag>       &  Pattern  ,
	/*R*/ size_t                 const    seqleng  ,
	/*R*/ TFreq                  const    setsize  ,
	/*R*/ RSSDD_Node*       const    rootnode );


/* count pair frequency in ZDD under {rootnode} */
void CountPairFreq(
	/*W*/ std::vector<TFreq>       &  PairFreq ,
	/*R*/ RSSDD_Node*   const    rootnode );



/* common processing used in PrintPattern PrintPairFreq */
template <typename TN>
void PrintUpperTriangleMatrix(
	/*R*/ std::vector<TN> const &  Table       ,
	/*R*/ std::string     const &  RNAsequence ,
	/*R*/ Uint            const &  width       ,
	      std::ostream          &  ost         );

inline void PrintPattern(
	/*R*/ std::vector<EPairFlag> const &  Pattern         ,
	/*R*/ std::string            const &  RNAsequence     ,
	      std::ostream                 &  ost = std::cout );


inline void PrintPairFreq(
	/*R*/ std::vector<TFreq> const &  PairFreq        ,
	/*R*/ std::string        const &  RNAsequence     ,
	      std::ostream             &  ost = std::cout );


/* count pair in Pattern */
inline TFreq CountPairInPattern(
	/*R*/ std::vector<EPairFlag> const &  Pattern );


/* count pair whose existence probability in this set is 
 * greater than or equal to threshold */
inline TFreq CountPairInPattern(
	/*R*/ std::vector<TFreq> const &  PairFreq  ,
	/*R*/ TFreq              const    threshold );


/* build RSSDD from text file */
void BuildFromFile(
	/*W*/ RSSDD_UTable                  &  UTable    ,
	/*W*/ std::vector<RSSDD_Root>       &  Roots     ,
	/*R*/ std::string                  const &  IFileName );


/* build RSSDD_Root from given rootnode address.
 * newname can be "" */
void MakeRoot(
	/*W*/ std::vector<RSSDD_Root>       &  Roots           ,
	/*W*/ std::string                        &  newname         ,
	/*R*/ RSSDD_Node*             const    rootnode        ,
	/*R*/ Uint                         const    superset_number );


/* set operation */
void SetOperation_main(
	/*W*/ RSSDD_UTable                  &  UTable        ,
	/*W*/ std::vector<RSSDD_Root>       &  Roots         ,
	/*R*/ Uint                         const    root_number_1 ,
	/*R*/ Uint                         const    root_number_2 ,
	/*R*/ char                         const    op            );


/* build subset ZDD of sec.strs which is frequent */
void BuildTopK_main(
	/*W*/ RSSDD_UTable                  &  UTable      ,
	/*W*/ std::vector<RSSDD_Root>       &  Roots       ,
	/*R*/ Uint                         const    root_number ,
	/*R*/ Uint                         const    OutputNum   );


/* build subset ZDD of sec.strs which match the pattern */
void BuildSubsetForPattern(
	/*W*/ RSSDD_UTable                  &  UTable      ,
	/*W*/ std::vector<RSSDD_Root>       &  Roots       ,
	/*R*/ Uint                         const    root_number ,
	/*R*/ std::vector<EPairFlag>       const &  Pattern     );



/************************************************
 root_number   : 
 K             : output top K frequent pattern
 MinProb       : frequency >= MinProb is defined as frequent
 MinNumOfPairs : omit patterns with pairs fewer than 
 SkipProb      : default = LCM_HIGH_PROBABILITY
                 1.0 --> search exactly
                 0.8 --> don't add to pattern an item
                         with frequency >= 0.8 * setsize.
                 low value 
                     --> inaccurate but faster
                         and prevent outputting similar patterns
 LCMmax        : only output maximal patterns
 ToFile        : true --> output to file OFilename
 ************************************************/
void LCM(
	/*W*/ RSSDD_UTable                  &  UTable        ,
	/*R*/ std::vector<RSSDD_Root> const &  Roots         ,
	/*R*/ Uint                         const    root_number   ,
	/*R*/ Uint                         const    OutputNum     ,
	/*R*/ bool                         const    LCMmax        ,
	/*R*/ double                       const    MinProb       ,
	/*R*/ Uint                         const    MinNumOfPairs ,
	/*R*/ double                       const    SkipProb      ,
	/*R*/ bool                         const    ToFile        ,
	/*R*/ std::string                  const &  OFileName     );



/* print LCM result table (rank, prob, DBA)
 * [output]

 | Rank   | prob | freq | Root addr  | dot-bracket-aster      |
 |-------:|-----:|-----:|-----------:|:-----------------------|
 |      1 |  0.1 |    4 | 0000000000 | `  ((...))    ...    ` | */
void PrintFrequentPatterns(
	/*W*/ RSSDD_UTable                  &  UTable          ,
	/*R*/ std::vector<FrequentPattern> const &  FP              ,
	/*R*/ RSSDD_Root              const &  Z               ,
	      std::ostream                       &  ost = std::cout );

/* print */
void PrintList(
	/*R*/ std::vector<RSSDD_Root> const &  Roots );



/* interface */
void Interactive(
	/*W*/ std::vector<RSSDD_Root> &  Roots  ,
	/*W*/ RSSDD_UTable            &  UTable );




/**********************************************************
 class
 **********************************************************/

class BasePair
{
 private:
	Uint16 val;
 public:
	constexpr BasePair() : val(0) {}
	constexpr BasePair( const BasePair &  init ) : val(init.val) {}
	constexpr BasePair( const Uint16  init ) : val(init) {}

	constexpr BasePair( const Uint8  init1, const Uint8  init2 )
		: val( (init1 << 8) | init2 )
	{}

	constexpr BasePair( const std::pair<Uint8,Uint8> &  P )
		: val( (P.first << 8) | P.second )
	{}

	~BasePair() {}

 /* substitution */
	BasePair & operator=( const BasePair &  init ) {
		val = init.val;
		return *this;
	}

	BasePair & operator=( const Uint16  init ) {
		val = init;
		return *this;
	}

	operator int() const { return static_cast<int>(val); }

	Uint8 first () const { return (val >>  8); }
	Uint8 second() const { return (val & 255); }

	std::pair<Uint8,Uint8> Convert2Pair() const {
		return std::make_pair( first(), second() );
	}

	BasePair & operator++() { ++val;  return *this; }
	BasePair & operator--() { --val;  return *this; }
	BasePair & operator+=( const BasePair &  rhs ) { val += rhs.val; return *this; }
	BasePair & operator-=( const BasePair &  rhs ) { val -= rhs.val; return *this; }
	BasePair & operator+=( const Uint rhs ) { val += rhs; return *this; }
	BasePair & operator-=( const Uint rhs ) { val -= rhs; return *this; }

	friend bool operator==( const BasePair &  lhs, const BasePair &  rhs ) {
		return lhs.val == rhs.val;
	}
	friend bool operator!=( const BasePair &  lhs, const BasePair &  rhs ) {
		return std::rel_ops::operator!=(lhs,rhs);
	}
	friend bool operator< ( const BasePair &  lhs, const BasePair &  rhs ) {
		return lhs.val < rhs.val;
	}
	friend bool operator> ( const BasePair &  lhs, const BasePair &  rhs ) {
		return std::rel_ops::operator> (lhs,rhs);
	}
	friend bool operator<=( const BasePair &  lhs, const BasePair &  rhs ) {
		return std::rel_ops::operator<=(lhs,rhs);
	}
	friend bool operator>=( const BasePair &  lhs, const BasePair &  rhs ) {
		return std::rel_ops::operator>=(lhs,rhs);
	}

 /* compare with int */
	friend bool operator==( const BasePair &  lhs, const int  rhs ) {
		return static_cast<int>(lhs.val) == rhs;
	}
	friend bool operator!=( const BasePair &  lhs, const int  rhs ) {
		return static_cast<int>(lhs.val) != rhs;
	}
	friend bool operator< ( const BasePair &  lhs, const int  rhs ) {
		return static_cast<int>(lhs.val) <  rhs;
	}
	friend bool operator> ( const BasePair &  lhs, const int  rhs ) {
		return static_cast<int>(lhs.val) >  rhs;
	}
	friend bool operator<=( const BasePair &  lhs, const int  rhs ) {
		return static_cast<int>(lhs.val) <= rhs;
	}
	friend bool operator>=( const BasePair &  lhs, const int  rhs ) {
		return static_cast<int>(lhs.val) >= rhs;
	}

	friend bool operator==( const int  lhs, const BasePair &  rhs ) {
		return lhs == static_cast<int>(rhs.val);
	}
	friend bool operator!=( const int  lhs, const BasePair &  rhs ) {
		return lhs != static_cast<int>(rhs.val);
	}
	friend bool operator< ( const int  lhs, const BasePair &  rhs ) {
		return lhs <  static_cast<int>(rhs.val);
	}
	friend bool operator> ( const int  lhs, const BasePair &  rhs ) {
		return lhs >  static_cast<int>(rhs.val);
	}
	friend bool operator<=( const int  lhs, const BasePair &  rhs ) {
		return lhs <= static_cast<int>(rhs.val);
	}
	friend bool operator>=( const int  lhs, const BasePair &  rhs ) {
		return lhs >= static_cast<int>(rhs.val);
	}


	friend std::ostream & operator<<(
		        std::ostream       &  ost
		/*R*/ , BasePair     const &  p )
	{
		ost << std::right << '(' << p.first() << ',' << p.second() << ')';
		return ost;
	}
};




class FrequentPattern
{
 public:
	TFreq             freq;
	RSSDD_Node*  rootnode;

	FrequentPattern()
		: freq    (0)
		, rootnode(nullptr)
	{}

	FrequentPattern(
		/*R*/ TFreq            const  init_freq     ,
		/*R*/ RSSDD_Node* const  init_rootnode )
		: freq    (init_freq)
		, rootnode(init_rootnode)
	{}

	~FrequentPattern(){}

	friend bool operator==(
		/*R*/ FrequentPattern const &  lhs ,
		/*R*/ FrequentPattern const &  rhs )
	{
		return (lhs.rootnode == rhs.rootnode);
	}
	friend bool operator< (
		/*R*/ FrequentPattern const &  lhs ,
		/*R*/ FrequentPattern const &  rhs )
	{
		return (lhs.freq <  rhs.freq);
		// if (lhs.freq <  rhs.freq) return true;
		// if (lhs.freq == rhs.freq) {
		// 	return (lhs.rootnode < rhs.rootnode);
		// }
		// return false;
	}

	friend bool operator!=(
		/*R*/ FrequentPattern const &  lhs ,
		/*R*/ FrequentPattern const &  rhs )
	{
		return std::rel_ops::operator!=(lhs,rhs);
	}
	friend bool operator> (
		/*R*/ FrequentPattern const &  lhs ,
		/*R*/ FrequentPattern const &  rhs )
	{
		return std::rel_ops::operator> (lhs,rhs);
	}
	friend bool operator<=(
		/*R*/ FrequentPattern const &  lhs ,
		/*R*/ FrequentPattern const &  rhs )
	{
		return std::rel_ops::operator<=(lhs,rhs);
	}
	friend bool operator>=(
		/*R*/ FrequentPattern const &  lhs ,
		/*R*/ FrequentPattern const &  rhs )
	{
		return std::rel_ops::operator>=(lhs,rhs);
	}
};




/************************************************
   - PartFunc              pratition function for sequence
   - sample_number         number of sample
   - sample_prob_sum       sum of existence probability of this set.
      approximate existence probability by
      sample_prob_sum * |structure X| / sample_number

 * pattern of secondary structure set
   - Pattern_disignated    Pattern disignated by user 
   - Pattern_result        Pattern derived from this set
   - PairFreq              pair frequency
                           (sum of sec.str frequency including the pair) 
   - Itemset               all pairs that appear in this RSSDD 

 * shortcut
   - setsize()             sum of the element count of
                           secondary structures in RSSDD
   - MFE()                 the minimum free energy of
                           secondary structure in RSSDD
 ************************************************/
class RSSDD_Root : public WeightedZDD_Root<BasePair, TFreq>
{
 private:
	std::string             RNAsequence__;
	double                  Temperature__;
	TFreq                   sample_number__;
	double                  sample_prob_sum__;
	double                  PartFunc__;
	double                  MFE__;

	std::vector<EPairFlag>  Pattern_disignated__;
	std::vector<EPairFlag>  Pattern_result__;
	std::vector<TFreq>      PairFreq__;
	std::vector<BasePair>   Itemset__;


	TFreq GetFreqMax__(
		/*R*/ RSSDD_Node* const  N ) const
	{
		if ( N == nullptr ) return 0;
		if ( N->IsNode1() ) {
			return N->weight;
		}
		if ( N->weight ) {
			return std::max( GetFreqMax__( N->hi ) , GetFreqMax__( N->lo ) );
		}
		return 0;
	}


 public:
	RSSDD_Root(
		/*R*/ std::string            const &  init_name              ,
		/*R*/ RSSDD_Node*       const    init_rootnode          ,
		/*R*/ Uint                   const    init_superset          ,
		/*R*/ Uint                   const    init_top_level_set     ,
		/*R*/ std::string            const &  init_RNAsequence       ,
		/*R*/ double                 const    init_Temperature       ,
		/*R*/ TFreq                  const    init_sample_number     ,
		/*R*/ double                 const    init_sample_prob_sum   ,
		/*R*/ double                 const    init_PartFunc = 0.0    ,
		/*R*/ double                 const    init_MFE      = 0.0    ,
		/*R*/ std::vector<EPairFlag> const    init_DisignatedPattern
		                       = std::vector<EPairFlag>() )
	: WeightedZDD_Root (
				  init_name
				, init_rootnode
				, init_superset
				, init_top_level_set )
		, RNAsequence__     (init_RNAsequence)
		, Temperature__     (init_Temperature)
		, sample_number__   (init_sample_number)
		, sample_prob_sum__ (init_sample_prob_sum)
		, PartFunc__        (init_PartFunc)
		, MFE__             (init_MFE)
	{
		/* PartFunc */
		if ( PartFunc__ == 0.0 ) {
			PartFunc__ = GetPartFunc( RNAsequence__, Temperature__ );
		}
		/* MFE */
		if ( MFE__ == 0.0 ) {
			MFE__ = Prob2FreeEnergy(
				  Freq2Prob( GetFreqMax__( rootnode() ) )
				, PartFunc__
				, Temperature__ );
		}

		/* disignated pattern */
		if ( not init_DisignatedPattern.empty() ) {
			Pattern_disignated__ = init_DisignatedPattern;
		} else {
			InitializePattern( Pattern_disignated__, RNAsequence().length() );
		}

		/* result pattern */
		InitializePattern( Pattern_result__, RNAsequence().length() );
		PairFreq__.resize( PATTERN_SIZE, 0 );
		CountPairFreq( PairFreq__, rootnode__ );

		PairFreq2Pattern(
				  Pattern_result__
				, RNAsequence().length()
				, PairFreq__
				, setsize() );

		/* get itemset */
		for ( BasePair i = 0, n = PairFreq__.size(); i < n; ++i ) {
			if ( PairFreq__[i] > 0 ) Itemset__.push_back(i);
		}
	}


 /* getter */
	auto RNAsequence       () const  -> decltype( RNAsequence__ )
	{ return RNAsequence__; }

	auto sample_number     () const  -> decltype( sample_number__ )
	{ return sample_number__; }

	auto sample_prob_sum   () const  -> decltype( sample_prob_sum__ )
	{ return sample_prob_sum__; }

	auto PartFunc          () const  -> decltype( PartFunc__ )
	{ return PartFunc__; }

	auto MFE               () const  -> decltype( MFE__ )
	{ return MFE__; }

	auto Temperature       () const  -> decltype( Temperature__ )
	{ return Temperature__; }

	auto Pattern_disignated() const  -> decltype( Pattern_disignated__ )
	{ return Pattern_disignated__; }

	auto Pattern_result    () const  -> decltype( Pattern_result__ )
	{ return Pattern_result__; }

	auto PairFreq          () const  -> decltype( PairFreq__ )
	{ return PairFreq__; }

	auto Itemset           () const  -> decltype( Itemset__ )
	{ return Itemset__; }


 /* alias, shortcut */
	TFreq   setsize() const
	{ return weight_sum(); }  /* alias */

	double  set_prob() const
	{ return Freq2Prob(setsize()); }

	double  Freq2Prob( TFreq  const  freq ) const
	{ return (sample_prob_sum() * freq) / sample_number(); }


	TFreq   Prob2Freq( double const  prob ) const
	{ return (sample_number() * prob) / sample_prob_sum(); }


 /* methods */

	virtual void PrintHeader( std::ostream &  ost ) const
	{  /* used in PrintCombinations() */
		/* position number */
		for ( size_t i = 0; i <= RNAsequence().length() / 5; ++i ) {
			ost << std::setw(5) << std::left << 5 * i;
		}
		ost << std::endl;

		/* ruler */
		for ( size_t i = 0; i < RNAsequence().length(); ++i ) {
			switch (i % 10) {
				case 0  : ost << '|'; break;
				case 5  : ost << ':'; break;
				default : ost << '-'; break;
			}
		}
		ost << std::endl;

		// ost << "   probability" << std::endl;

		/* RNA sequence */
		ost << RNAsequence() << std::endl;
	}

	virtual void ConvertAndPrint(
		      std::ostream                &  ost        ,
		/*R*/ std::vector<BasePair> const &  pair_array ,
		/*R*/ TFreq                 const &  weight     ) const
	{  /* used in PrintCombinations() */
		ost << PairArray2DB( pair_array, RNAsequence().length() ) << "   ";
		ost << std::right << std::setw(10) << std::fixed << Freq2Prob(weight);
	}

	bool ExistSecstr( std::string const &  query_DB ) const
	{
		std::vector<BasePair>  pair_array;
		DB2PairArray( pair_array, query_DB );
		return ExistCombination( pair_array );
	}

	std::pair<Uint,double>
	 MatchPatternNum( std::vector<EPairFlag> const &  Pattern ) const
	{
		Uint  match_num = 0;
		TFreq match_weight_sum = 0;

		/***** local functions ****************************/
		/* NumOfRemainingPairs is necessary when reached Node1.
		 * even if there is no confilcts in the path to Node1,
		 * this path does not match the Pattern if some pair remained. */
		std::function< void(
			/*R*/ int              const NumOfRemainingPairs ,
			/*R*/ RSSDD_Node* const N                   ) >
		subroutine = [&] (
			/*R*/ int              const NumOfRemainingPairs ,
			/*R*/ RSSDD_Node* const N                   )
		{
			// if ( found ) return;  /* found --> don't need to search anymore */
			if ( N == nullptr ) return;

			if ( N->IsNode1() ) {
				if ( NumOfRemainingPairs <= 0 ) {
					++match_num;
					match_weight_sum += N->weight;
				}
				/* else : some of pairs is not contained */
				return;
			}

			/* N is an internal node */
			switch ( Pattern[ N->label ] ) {
				case EPairFlag::Pair :
				 /* recursive call to hi-edge only */
					subroutine( NumOfRemainingPairs - 1, N->hi );
					break;
				case EPairFlag::NotPair :
				 /* recursive call to lo-edge only */
					subroutine( NumOfRemainingPairs, N->lo );
					break;
				case EPairFlag::Undecided :
				 /* recursive call to hi-edge & lo-edge */
					subroutine( NumOfRemainingPairs, N->hi );
					subroutine( NumOfRemainingPairs, N->lo );
					break;
				default :
					break;
			}
		};
		/**************************************************/

		subroutine( CountPairInPattern( Pattern ), rootnode() );
		return std::make_pair( match_num, Freq2Prob( match_weight_sum ) );
	}

	void PrintDB( std::ostream &  ost ) const
	{
		PrintCombinations( ost );
	}


	void PrintItemset( std::ostream &  ost ) const
	{
		ost << std::endl;
		ost << "#Itemset = " << Itemset().size() << std::endl;
		ost << "Itemset  = " << Itemset() << std::endl;
		ost << std::endl;
	}


	void PrintDisignatedPattern( std::ostream &  ost ) const
	{ /* print upper-triangle matrix of pair-flag character */
		PrintPattern( Pattern_disignated(), RNAsequence(), ost );
	}

	void PrintPatternResult( std::ostream &  ost ) const
	{ /* print upper-triangle matrix of pair-flag character */
		PrintPattern( Pattern_result(), RNAsequence(), ost );
	}

	void PrintPairFrequency( std::ostream &  ost ) const
	{ /* print upper-triangle matrix of pair frequency */
		PrintPairFreq( PairFreq(), RNAsequence(), ost );
	}

	void PrintDBA( std::ostream &  ost ) const
	{
		ost << std::endl;
		PrintHeader( ost );
		ost << Pattern2DBA( Pattern_result() ) << std::endl;
		ost << std::endl;
	}

};




/**********************************************************
 inline function & template function
 **********************************************************/

inline std::ostream & operator<<(
	/*W*/ std::ostream       &  ost ,
	/*R*/ EPairFlag    const    num )
{
	switch ( num ) {
		case EPairFlag::Pair :
			ost << PATTERN_CHAR_PAIR            ; break;
		case EPairFlag::ProbablyPair :
			ost << PATTERN_CHAR_PROBABLY_PAIR   ; break;
		case EPairFlag::Undecided :
			ost << PATTERN_CHAR_UNDECIDED       ; break;
		case EPairFlag::ProbablyNotPair :
			ost << PATTERN_CHAR_PROBABLY_NOTPAIR; break;
		case EPairFlag::NotPair         :
			ost << PATTERN_CHAR_NOTPAIR         ; break;
		case EPairFlag::Dummy           :
			ost << PATTERN_CHAR_DUMMY           ; break;
		default : break;
	}
	return ost;
}



inline double Prob2FreeEnergy(
	/*R*/ double const  Prob        ,
	/*R*/ double const  PartFunc    ,
	/*R*/ double const  Temperature )
{
	return -( (GASCONST * Temperature) * std::log(Prob * PartFunc) ) / 1000.0;
}



inline double FreeEnergy2Prob(
	/*R*/ double const  Energy      ,
	/*R*/ double const  PartFunc    ,
	/*R*/ double const  Temperature )
{
	return std::exp( (-Energy * 1000.0) / (GASCONST * Temperature) ) / PartFunc;
}



inline bool IsDBstring(
	/*R*/ std::string const &  DB )
{
	if ( not IsDBAstring( DB ) ) return false;
 /* if '*' exists -> DB is not DB-string */
	for ( const auto & ch : DB ) { if ( ch == '*' ) return false; }
	return true;
}


inline std::vector<BasePair> DB2PairArray(
	/*R*/ std::string const &  DB )
{
	std::vector<BasePair> PairArray;
	DB2PairArray( PairArray, DB );
	return PairArray;
}


inline std::string PairArray2DB(
	/*R*/ std::vector<BasePair> const &  PairArray ,
	/*R*/ size_t                const    seqleng   )
{
	std::string DB( seqleng, '.' );  /* dot-bracket string */

	for ( const auto & e : PairArray ) {
		DB[ e.first()  ] = '(';
		DB[ e.second() ] = ')';
	}
	return DB;
}


inline void InitializePattern(
	/*W*/ std::vector<EPairFlag>       &  Pattern ,
	/*R*/ size_t                 const    seqleng )
{
	Pattern.resize( PATTERN_SIZE );
	fill( Pattern.begin(), Pattern.end(), EPairFlag::Dummy );
	for ( size_t i = 0; i < seqleng; ++i ) {
		for ( size_t j = i + 1; j < seqleng; ++j ) {
			Pattern[ BasePair(i,j) ] = EPairFlag::Undecided;
		}
	}
}


inline std::string Pattern2DBA(
	/*R*/ std::vector<EPairFlag> const &  Pattern )
{
	std::string  DBA;
	Pattern2DBA( DBA, Pattern );
	return DBA;
}



inline void CalcPattern(
	/*W*/ std::vector<EPairFlag>       &  Pattern  ,
	/*R*/ size_t                 const    seqleng  ,
	/*R*/ TFreq                  const    setsize  ,
	/*R*/ RSSDD_Node*       const    rootnode )
{
	std::vector<TFreq>  PairFreq( PATTERN_SIZE );
	CountPairFreq( PairFreq, rootnode );
	PairFreq2Pattern( Pattern, seqleng, PairFreq, setsize );
}



template <typename TN>
void PrintUpperTriangleMatrix(
	/*R*/ std::vector<TN> const &  Table       ,
	/*R*/ std::string     const &  RNAsequence ,
	/*R*/ Uint            const &  width       ,
	      std::ostream          &  ost         )
{
	ost
	 << std::endl
	 << "   ";
	for ( auto ch = RNAsequence.cbegin(); ch != RNAsequence.cend(); ++ch ) {
		ost << std::setw(width) << *ch;
	}
	ost
	 << std::endl
	 << "  +" << std::string( width * RNAsequence.length() , '-')
	 << '+' << std::endl;

	for( size_t i = 0; i < RNAsequence.length(); ++i ) {
		ost << ' ' << RNAsequence[i] << '|';
		for( size_t j = 0; j < RNAsequence.length(); ++j ) {
			if ( i < j ) {
				ost << std::setw(width) << Table[ BasePair(i,j) ];
			} else {
				ost << std::setw(width) << '.';
			}
		}
		ost << '|' << RNAsequence[i]<< std::endl;
	}
	ost
	 << "  +"
	 << std::string( width * RNAsequence.length() , '-')
	 << '+' << std::endl
	 << "   ";
	for ( auto ch = RNAsequence.cbegin(); ch != RNAsequence.cend(); ++ch ) {
		ost << std::setw(width) << *ch;
	}
	ost
	 << std::endl
	 << std::endl;
}



inline void PrintPattern(
	/*R*/ std::vector<EPairFlag> const &  Pattern     ,
	/*R*/ std::string            const &  RNAsequence ,
	      std::ostream                 &  ost         )
{
	const Uint  width = 2;
	PrintUpperTriangleMatrix( Pattern, RNAsequence, width, ost );
}


inline void PrintPairFreq(
	/*R*/ std::vector<TFreq> const &  PairFreq    ,
	/*R*/ std::string        const &  RNAsequence ,
	      std::ostream             &  ost         )
{
	const Uint  width
	 = 1 + std::max( Uint(1), DigitNumber( Vector::Max( PairFreq ) ) );
	PrintUpperTriangleMatrix( PairFreq, RNAsequence, width, ost );
}



inline TFreq CountPairInPattern(
	/*R*/ std::vector<EPairFlag> const &  Pattern )
{
	return std::count_if(
		  Pattern.cbegin()
		, Pattern.cend()
		, [](EPairFlag x) { return x == EPairFlag::Pair; } );
}


inline TFreq CountPairInPattern(
	/*R*/ std::vector<TFreq> const &  PairFreq  ,
	/*R*/ TFreq              const    threshold )
{
	return std::count_if(
		  PairFreq.cbegin()
		, PairFreq.cend()
		, [&](TFreq x) { return x >= threshold; } );
}
