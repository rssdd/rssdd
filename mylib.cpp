
#include <readline/readline.h>
#include <readline/history.h>

#include <iostream>
#include <fstream>

#include <string>
#include <vector>

#include <iomanip>     /* setw */
#include <cstdint>     /* uint_fast32_t, ... */
#include <cmath>       /* log10 */
#include <algorithm>

// #include <boost/operators.hpp>

#include "mylib.hpp"

using namespace std;




int ExtractNumber( string &  str )
{
	size_t i = 0;
	for( ; i < str.size(); ++i ) {
		if ( '0' <= str[i] and str[i] <= '9' )  break;
	}
	if ( i >= str.size() )  return -1;

	size_t beg_pos = i;

	for( ; i < str.size(); ++i ) {
		if ( str[i] < '0' or '9' < str[i] )  break;
	}
	size_t end_pos = i;

	int number = StoN<int>( str.substr( beg_pos, end_pos - beg_pos ) );

	for ( size_t k = beg_pos; k < end_pos; ++k ) {
		str[k] = '_';  /* overwrite */
	}

	return number;
}



/* '+' -> 1;   '-' -> -1;   not found -> 0 */
int ExtractPlusMinus( string &  str )
{
	for( size_t i = 0; i < str.size(); ++i ) {
		if ( str[i] == '+' ) {
			str[i] = '_';  return  1;
		}
		if ( str[i] == '-' ) {
			str[i] = '_';
			return -1;
		}
	}
	return 0;
}



bool FileCheck(
	/*S*/ std::ifstream       &  fin       ,
	/*R*/ std::string   const &  Ifilename )
{
	if ( fin.fail() ) {
		cerr << "cannot open \"" << Ifilename << '\"' << endl;
		return false;
	}
	return true;
}



bool FileCheck(
	/*S*/ std::ofstream       &  fout      ,
	/*R*/ std::string   const &  Ofilename )
{
	if ( fout.fail() ) {
		cerr << "cannot open \"" << Ofilename << '\"' << endl;
		return false;
	}
	return true;
}



std::string MakeMDTableHeaderString(
	/*R*/ std::vector<bool>        const &  lr          ,
	/*R*/ std::vector<size_t>      const &  width       ,
	/*R*/ std::vector<std::string> const &  header_name )
{
	if ( lr.size() != width.size() or lr.size() != header_name.size() ) {
		std::cout << "ERROR: mismatch in size" << std::endl;
		return "";
	}

	std::stringstream  ss;

	for ( size_t i = 0; i < header_name.size(); ++i ) {
		ss << "| " << std::left << std::setw(width[i]);
		ss << header_name[i] << ' ';
	}
	ss << "|\n";

 /* lr == "01011" -> left right left right right */

	ss.fill('-');
	for ( size_t i = 0; i < header_name.size(); ++i ) {
		ss << '|';
		ss << (lr[i] ? std::right : std::left);
		ss << std::setw( width[i] + 2 ) << ':';
	}
	ss << '|';

	return ss.str();
}
