var constraints_8h =
[
    [ "VRNA_CONSTRAINT_DB_PIPE", "group__constraints.html#ga13053547a2de5532b64b64d35e097ae1", null ],
    [ "VRNA_CONSTRAINT_DB_DOT", "group__constraints.html#ga369bea82eae75fbe626f409fa425747e", null ],
    [ "VRNA_CONSTRAINT_DB_X", "group__constraints.html#ga7283bbe0f8954f7b030ecc3f2d1932b2", null ],
    [ "VRNA_CONSTRAINT_DB_ANG_BRACK", "group__constraints.html#gad54c1315a47d55653dcaa5de6e544b77", null ],
    [ "VRNA_CONSTRAINT_DB_RND_BRACK", "group__constraints.html#gac17b034852c914bc5879954c65d7e74b", null ],
    [ "VRNA_CONSTRAINT_DB_INTRAMOL", "group__constraints.html#ga5c17253f5a39d1d49b0fb11f5196982a", null ],
    [ "VRNA_CONSTRAINT_DB_INTERMOL", "group__constraints.html#ga31d0ebb9755ca8a4acafc14f00ca755d", null ],
    [ "VRNA_CONSTRAINT_DB_GQUAD", "group__constraints.html#ga75cfab03cdc97c95b3ce8bb29f52b08e", null ],
    [ "VRNA_CONSTRAINT_DB_ENFORCE_BP", "group__constraints.html#ga29ebe940110d60ab798fdacbcdbbfb7d", null ],
    [ "VRNA_CONSTRAINT_MULTILINE", "group__hard__constraints.html#ga7d725ef525b29891abef3f1ed42599a4", null ],
    [ "VRNA_CONSTRAINT_NO_HEADER", "group__hard__constraints.html#ga08d12a9a846ea593b7171d277c9f033f", null ],
    [ "VRNA_CONSTRAINT_ALL", "group__constraints.html#ga0a697f77a6fbb10f34e16fa68ed9e655", null ],
    [ "VRNA_CONSTRAINT_DB", "group__constraints.html#ga4bfc2f15c4f261c62a11af9d2aa80c90", null ],
    [ "VRNA_CONSTRAINT_FILE", "group__constraints.html#ga62e0ed0c33002c09423de4e646f85a2b", null ],
    [ "VRNA_CONSTRAINT_SOFT_MFE", "group__soft__constraints.html#ga62aa195893d02d1a79ca94952748df36", null ],
    [ "VRNA_CONSTRAINT_SOFT_PF", "group__soft__constraints.html#ga03fb5000c19b9a2082bf4ea30a543045", null ],
    [ "VRNA_CONSTRAINT_CONTEXT_EXT_LOOP", "group__hard__constraints.html#ga9418eda62a5dec070896702c279d2548", null ],
    [ "VRNA_CONSTRAINT_CONTEXT_HP_LOOP", "group__hard__constraints.html#ga79203702b197b6b9d3b78eed40663eb1", null ],
    [ "VRNA_CONSTRAINT_CONTEXT_INT_LOOP", "group__hard__constraints.html#ga21feeab3a9e5fa5a9e3d9ac0fcf5994f", null ],
    [ "VRNA_CONSTRAINT_CONTEXT_INT_LOOP_ENC", "group__hard__constraints.html#ga0536288e04ff6332ecdc23ca4705402b", null ],
    [ "VRNA_CONSTRAINT_CONTEXT_MB_LOOP", "group__hard__constraints.html#ga456ecd2ff00056bb64da8dd4f61bbfc5", null ],
    [ "VRNA_CONSTRAINT_CONTEXT_MB_LOOP_ENC", "group__hard__constraints.html#ga02a3d703ddbcfce393e4bbfcb9db7077", null ],
    [ "VRNA_CONSTRAINT_CONTEXT_ALL_LOOPS", "group__hard__constraints.html#ga886d9127c49bb982a4b67cd7581e8a5a", null ],
    [ "VRNA_DECOMP_PAIR_HP", "group__generalized__sc.html#ga8bd41ebc8039378d242e4e8c273716a5", null ],
    [ "VRNA_DECOMP_PAIR_IL", "group__generalized__sc.html#gaeab04f34d7730cff2d651d782f95d857", null ],
    [ "vrna_hc_t", "group__constraints.html#gac7e4c4f8abe3163a68110c5bff24e01d", null ],
    [ "vrna_sc_t", "group__constraints.html#ga75401ce219ef8dbcceb672db82d434c6", null ],
    [ "vrna_callback_hc_evaluate", "group__hard__constraints.html#ga16eb71ac9a7a35369be2eaa9d8f8dfa0", null ],
    [ "vrna_callback_sc_energy", "group__generalized__sc.html#gaf38062858ac25fd5e240c2c3b0b0b780", null ],
    [ "vrna_callback_sc_exp_energy", "group__generalized__sc.html#ga2eade8745c163a553763be4cfe2a679b", null ],
    [ "vrna_callback_sc_backtrack", "group__generalized__sc.html#gaa216f513c3b0bd6fe5807dd0c53a8e5a", null ],
    [ "vrna_message_constraint_options", "group__constraints.html#gaa1f20b53bf09ac2e6b0dbb13f7d89670", null ],
    [ "vrna_message_constraint_options_all", "group__constraints.html#gaec7e13fa0465c2acc7a621d1aecb709f", null ],
    [ "vrna_constraints_add", "group__constraints.html#ga35a401f680969a556858a8dd5f1d07cc", null ],
    [ "vrna_hc_init", "group__hard__constraints.html#ga36ff456c43bf920629cee5a236e4f0ff", null ],
    [ "vrna_hc_add_up", "group__hard__constraints.html#gaeb352e3e6ccd2b567bafa451365bb545", null ],
    [ "vrna_hc_add_bp", "group__hard__constraints.html#gac49305fc5c7d8653c5fbd2de1e1615e2", null ],
    [ "vrna_hc_add_bp_nonspecific", "group__hard__constraints.html#gadeb1083d0dc928e1e931065aae54ad82", null ],
    [ "vrna_hc_free", "group__hard__constraints.html#ga696dcf77887d856c6f21ea266d8b9ca2", null ],
    [ "vrna_sc_init", "group__soft__constraints.html#ga9d977a1681356778cc66dceafbe5b032", null ],
    [ "vrna_sc_add_bp", "group__soft__constraints.html#ga86049d4bb0ea8674cae9b6177156b184", null ],
    [ "vrna_sc_add_up", "group__soft__constraints.html#ga30f30c8eff9676775a3e831d972b5284", null ],
    [ "vrna_sc_remove", "group__soft__constraints.html#ga73cdc07b9a199c614367bebef0f2c41a", null ],
    [ "vrna_sc_free", "group__soft__constraints.html#ga6d55446448d69346fc313b993c4fb3e8", null ],
    [ "vrna_sc_add_SHAPE_deigan", "group__soft__constraints.html#ga57d612b58e1c61dd6cfcb5a843f8f1b3", null ],
    [ "vrna_sc_add_SHAPE_deigan_ali", "group__soft__constraints.html#ga04ba85da63d8c793bb8001d1e6f800ba", null ],
    [ "vrna_sc_add_SHAPE_zarringhalam", "group__soft__constraints.html#gaf3c65a045060aef5c4e41693d30af58c", null ],
    [ "vrna_sc_SHAPE_parse_method", "constraints_8h.html#a71b84eb0dd69e5c256f4ecf8f78cf314", null ],
    [ "vrna_sc_SHAPE_to_pr", "group__soft__constraints.html#ga67675b3ed48744489a3bcfa4174197cb", null ],
    [ "vrna_sc_add_f", "group__generalized__sc.html#ga8c7d907ec0125cd61c04e0908010a4e9", null ],
    [ "vrna_sc_add_bt", "group__generalized__sc.html#gabde7d07a79bb9a8f4721aee247b674ea", null ],
    [ "vrna_sc_add_exp_f", "group__generalized__sc.html#ga87e382b5d0c9b7d9ce1b79c0473ff700", null ],
    [ "print_tty_constraint", "constraints_8h.html#a4d167deb70bb51723e44374dc981deb2", null ],
    [ "print_tty_constraint_full", "constraints_8h.html#ae8ae8a34962b9959be3f6c40f0a80ac1", null ],
    [ "constrain_ptypes", "constraints_8h.html#a36c3a6c3218b041f992052767bc74549", null ]
];