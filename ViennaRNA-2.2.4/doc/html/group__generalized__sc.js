var group__generalized__sc =
[
    [ "VRNA_DECOMP_PAIR_HP", "group__generalized__sc.html#ga8bd41ebc8039378d242e4e8c273716a5", null ],
    [ "VRNA_DECOMP_PAIR_IL", "group__generalized__sc.html#gaeab04f34d7730cff2d651d782f95d857", null ],
    [ "vrna_callback_sc_energy", "group__generalized__sc.html#gaf38062858ac25fd5e240c2c3b0b0b780", null ],
    [ "vrna_callback_sc_exp_energy", "group__generalized__sc.html#ga2eade8745c163a553763be4cfe2a679b", null ],
    [ "vrna_callback_sc_backtrack", "group__generalized__sc.html#gaa216f513c3b0bd6fe5807dd0c53a8e5a", null ],
    [ "vrna_sc_add_f", "group__generalized__sc.html#ga8c7d907ec0125cd61c04e0908010a4e9", null ],
    [ "vrna_sc_add_bt", "group__generalized__sc.html#gabde7d07a79bb9a8f4721aee247b674ea", null ],
    [ "vrna_sc_add_exp_f", "group__generalized__sc.html#ga87e382b5d0c9b7d9ce1b79c0473ff700", null ],
    [ "vrna_sc_add_hi_motif", "group__generalized__sc.html#gaa6ff0113a3a76dc0b8d62961f4e1dfa0", null ]
];